<?php

session_start();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: Weekend Winterberg - Winterberg Lodge Hotel</title>
<meta name="description" content="Beleef een heerlijk wintersport weekend in Winterberg - Bus en hotel vanaf ...">
<link href="futureware/winterberg_style_hotels_winterberg.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<meta name="viewport" content="width=1002px">
</head>

<body>

<?php

require("css/connect.php");

?>
<div id="onder">
  <div id="wrapper">
    <div id="kopw">
      <div id="kopwl">
        <div id="language">
          <ul>
            <li><a href="h_bedrijfsuitje_personeelsuitje_winterberg.html"><img src="bus_reis_winterberg_images/nl_vakantie_winterberg_sauerland.png" width="18" height="12" /></a></li>
            <li><a href="h_company_day_out_wintersport_ski_snow_winterberg_germany.html"><img src="bus_reis_winterberg_images/en_ski_snowboard_snow_trip_company_day_out_winterberg_sauerland_germany_by_bus.png" width="18" height="12" /></a></li>
          </ul>
        </div>
        <div id="menu">
          <ul>
            <li><a href="http://www.winterbergbus.nl" target="_blank">seat only tickets</a></li> /
            <li><a href="h_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">sneeuw &amp; webcams</a></li> /
            <li><a href="h_wat_is_er_te_doen_in_winterberg_sauerland_activiteiten.html">things to do</a></li> /
            <li><a href="h_skikleding_wintersportkleding_outlet_skibroek_ski_jas_handschoenen_skibril_skihelm_wintersport_ski_snowboard_kleding_slee_webshop.php">webshop</a></li> /
            <li><a href="h_bedrijfsuitje_personeelsuitje_schoolreis_winterberg_aanvragen.php">offerte</a></li> /
          </ul>
        </div>
      </div>
      <div id="kopwr"><img src="bus_reis_winterberg_images/busreis_busreizen_winterberg_sauerland_duitsland.png" width="125" height="125" /></div>
    </div>
    <div id="acc-sub-bg">
      <div class="acc-sub-row1">
        <div class="inner">Accommodatie selecteren <span class="marg6lr">&gt;</span> <span class="blu1">Boek bus en kamer(s)</span> &gt; Afrekenen</div></div>
    <div id="acc-sub-row2">
      <div id="acc-sub-row2-wrap">
        
        <div id="acc-sub-row2-content">
        
          <div id="row2-box1">
		  
			<?php
			
			$volgorde = $_GET['volgorde'];
			
			if ($volgorde==NULL) {
			
				$volgorde = $_POST['volgorde'];
				
				if ($volgorde==NULL) {
			
					$volgorde = 1;
					
				}
				
			}
			
			$weekend = $_POST['weekend'];
			
			$query = "SELECT * FROM reizen WHERE volgorde=\"$volgorde\"";
			$query_result = mysql_query($query);
			if ($query_result) {
			while ($row = mysql_fetch_array($query_result)) {
			
				$naam = $row[2];
				$prijs = $row[3];
				$plaats = $row[4];
				$omschrijving = $row[5];
				$ligging = $row[6];
				$faciliteiten = $row[7];
				$kamers = $row[8];
				$verzorging = $row[9];
				$overig = $row[10];
				$foto1 = $row[11];
				
				$pic1 = 'images/' . $foto1;
				
				$foto2 = $row[12];
				
				$pic2 = 'images/' . $foto2;
				
				$foto3 = $row[13];
				
				$pic3 = 'images/' . $foto3;
				
				$zwembad = $row[14];
				$lounge = $row[15];
				$bos = $row[16];
				$receptie = $row[17];
				$fiets = $row[18];
				$laundry = $row[19];
				$wifi = $row[20];
				$berging = $row[21];
				$douchetoilet = $row[22];
				$wellness = $row[23];
				$centraal = $row[24];
				$douchetage = $row[25];
				$baddouche = $row[26];
				$kluis = $row[27];
				$restaurant = $row[28];
				$ontbijtzaal = $row[29];
			
			}
			
			}

			?>
			
            <div class="box1-title"><span class="box1-title-mar6"><?php echo($naam) ?></span></div>
            
          <div class="box1-row2"><img src="bus_reis_winterberg_images/00vlag.png" width="16" height="11" class="marg5r" /><?php echo($plaats) ?></div>  
          
        <div class="box1-row3">
        <div class="accomodatie">
        <div class="changer"><img src="<? echo($pic1) ?>" /></div> 
        <div class="thumb">
        <a href="<? echo($pic1) ?>">
		<img src="<? echo($pic1) ?>" class="thumb-size" />
		</a>
		<a href="<? echo($pic2) ?>">
	    <img src="<? echo($pic2) ?>" class="thumb-size" />
		</a>
		<a href="<? echo($pic3) ?>">
		<img src="<? echo($pic3) ?>" class="thumb-size close" />
		</a>
		</div>
        </div>
        </div>

        <div class="box1-row4"><div class="fb-like" data-href="https://www.facebook.com/pages/Winterbergbusnl/304489432908047" data-width="492" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div></div>
  
  <div class="box1-row5">
  <p class="box1-row5-title">Omschrijving</p>
  <p class="br10">&nbsp;</p>
  <p><?php echo($omschrijving) ?></p>
  <p>&nbsp;</p>
  <p class="box1-row5-title">Ligging</p>
  <p class="br10">&nbsp;</p>
  <?php echo($ligging) ?>
  <p>&nbsp;</p>
  <p class="box1-row5-title">Faciliteiten o.a.</p>
  <p class="br10">&nbsp;</p>
  <?php echo($faciliteiten) ?>
  <p>&nbsp;</p>
  <p class="box1-row5-title">Kamers</p>
  <p class="br10">&nbsp;</p>
  <?php echo($kamers) ?>
  <p>&nbsp;</p>
  <p class="box1-row5-title">Verzorging</p>
  <p class="br10">&nbsp;</p>
  <?php echo($verzorging) ?>
  <p>&nbsp;</p>
  <p class="box1-row5-title">Overige informatie</p>
  <p class="br10">&nbsp;</p>
  <?php echo($overig) ?>
  <p>&nbsp;</p>
</div>
          </div>
          <div id="row2-box2">
            <div class="box2-row1">
			
			<?php 
			
			print "$zwembad";
			
			if ($zwembad == "ja") {
			
			print "<img src=\"images/01.png\" width=\"26\" height=\"26\" />";
			
			}
			
			?>
			</div>
            <div class="box2-row2">
              <div class="box2-row2-left">
                <div class="book-title">leuk, ik ga boeken! <span class="blu1">&gt;&gt;</span></div>
                <div class="form">
                  <form id="form2" name="form2" method="post" action="<? $_SERVER['PHP_SELF'];?>">
					
					<div class="form-title">Selecteer accommodatie</div>
                    <div class="form-row">
                    <div class="form-option1">
                  				  
					<?php 
					
						print "<select name=\"volgorde\" class=\"text-input1\" data-validation=\"required\" onchange=\"this.form.submit()\">";
						
							$query1 = "SELECT * FROM reizen WHERE volgorde=\"$volgorde\" ORDER BY naam";
							$query_result1 = mysql_query($query1);
							if ($query_result1) {
							while ($row = mysql_fetch_array($query_result1)) {
							
								print "<option value=\"$row[1]\">$row[2]</option>";
							
							}
							
							}
							
							$query1 = "SELECT * FROM reizen WHERE volgorde!=\"$volgorde\" ORDER BY naam";
							$query_result1 = mysql_query($query1);
							if ($query_result1) {
							while ($row = mysql_fetch_array($query_result1)) {
							
								print "<option value=\"$row[1]\">$row[2]</option>";
							
							}
							
							}
						
						print "</select>";
				  
					?>
				  
                    </div>
                    </div>
					</form>
					
					<form id="form3" name="form3" method="post" action="<? $_SERVER['PHP_SELF'];?>">
					
                    <div class="form-title">Selecteer weekend</div>
                    <div class="form-row">
                    <div class="form-option1">
					
					<?php
					
					print "<input type=\"hidden\" name=\"volgorde\" value=\"$volgorde\">";
					
					print "<select name=\"weekend\" class=\"text-input1\" data-validation=\"required\" onchange=\"this.form.submit()\">";
					
					if ($weekend==NULL) {
					
						print "<option value=\"\"></option>";
					
					}
					
					$query3 = "SELECT * FROM weekend WHERE reis=\"$volgorde\" AND volgorde=\"$weekend\"";
					$query_result3 = mysql_query($query3);
					if ($query_result3) {
					while ($row = mysql_fetch_array($query_result3)) {
                  
						print "<option value=\"$row[2]\">$row[3]</option>";
						
					}
					
					}
					
					$query3 = "SELECT * FROM weekend WHERE reis=\"$volgorde\" AND volgorde!=\"$weekend\"";
					$query_result3 = mysql_query($query3);
					if ($query_result3) {
					while ($row = mysql_fetch_array($query_result3)) {
                  
						print "<option value=\"$row[2]\">$row[3]</option>";
						
					}
					
					}
					
					print "</select>";
					
					?>
					
                    </div>
                    </div>
					
					</form>
					
					<form id="form1" name="form1" method="post" action="hotel_boeken.php">
                    
                    <div class="form-title">Selecteer opstapplaats</div>
                    <div class="form-row">
                    <div class="form-option1">
					
					<?php 
					
					print "<input type=\"hidden\" name=\"volgorde\" value=\"$volgorde\">";
					
					print "<input type=\"hidden\" name=\"weekend\" value=\"$weekend\">";
                  
					print "<select name=\"opstapplaats\" class=\"text-input1\" data-validation=\"required\">";
					print "<option value=\"\"></option>";
					
					$query2 = "SELECT * FROM opstapplaats ORDER BY opstapplaats";
					$query_result2 = mysql_query($query2);
					if ($query_result2) {
					while ($row = mysql_fetch_array($query_result2)) {
                  
						print "<option value=\"$row[1]\">$row[2]</option>";
						
					}
					
					}
                  
					print "</select>";
				  
					?>
					
                    </div>
                    </div>
					
					<div class="form-title">Stoelen gereserveerd bij elkaar</div>
                    <div class="form-row">
                    <div class="form-option2">
					<input name="stoelen" type="checkbox" value="ja">
					<label name="stoelen" class="form-checkbox">ja (+ € 4,50 per boeking)</label></input>
                    </div>
                    </div>
					
					<div class="form-title">Aantal 1-pers kamers</div>
                    <div class="form-row">
                    <div class="form-option1">
					
					<?php
					
					$query4 = "SELECT * FROM weekend WHERE reis=\"$volgorde\" AND volgorde=\"$weekend\"";
					$query_result4 = mysql_query($query4);
					if ($query_result4) {
					while ($row = mysql_fetch_array($query_result4)) {
					
						$een = $row[4] - $row[5];
						$twee = $row[6] - $row[7];
												
					}
					
					}
					
					print "<select name=\"eenpers\" class=\"text-input1\" data-validation=\"required\">";
					
					print "<option value=\"0\">geen</option>";
					
					$start1 = 1;
					
					while ($start1 < ($een+1)) {
					 					  
						print "<option value=\"$start1\">$start1 x 1-pers.</option>";
					  
					$start1 = $start1 + 1;
					
					}
					
					print "</select>";
					
					?>
                    </div>
                    </div>
					
					<div class="form-title">Aantal 2-pers kamers</div>
                    <div class="form-row">
                    <div class="form-option1">
					
					<?php 
					
					print "<select name=\"tweepers\" class=\"text-input1\" data-validation=\"required\">";
					
					print "<option value=\"0\">geen</option>";
					
					$start2 = 1;
					
					while ($start2 < ($twee + 1)) {
					
						print "<option value=\"$start2\">$start2 x 2-pers.</option>";
						
					$start2 = $start2 + 1;
					
					}
					
					print "</select>";
					
					?>
                    </div>
                    </div>
					
					<div class="form-title">Bij voorkeur 3-pers. kamer(s)</div>
                    <div class="form-row">
                    <div class="form-option2">
					<input name="driepers" type="checkbox" value="ja" />
					<label class="form-checkbox">ja</label></input>
                    </div>
                    </div>
                    
                    <div class="form-title">Naam hoofdboeker</div>
                    <div class="form-row">
                    <div class="form-option1">
					<input type="text" name="boeker" class="text-input2" data-validation="required">
                    </div>
                    </div>
                    
                    <div class="form-title">E-mailadres</div>
                    <div class="form-row">
                    <div class="form-option1">
                  <input name="e_mail" class="text-input2" data-validation="email">
                    </div>
                    </div>
                    
                    <div class="form-title">Telefoonnummer tijdens reis</div>
                    <div class="form-row">
                    <div class="form-option1">
                  <input name="telefoon" class="text-input2" data-validation="required">
                    </div>
                    </div>
                    <?php
					
					//unset($_SESSION['bestelnummer']);
					
                    /*<div class="form-voorwaarden">
                  <input type="checkbox" onclick="javascript:toggle();" />
                  <label class="text-label">Voorwaarden akkoord</label>
                    </div>*/
					
					?>
                    
                    <div class="form-button">
						<input name="submit" type="submit" class="button" value="boek deze reis" />
                    </div>
                    
                  </form>
                </div>
              </div>
              <div class="box2-row2-right">
                <div class="overzicht-title">het weekend</div>
                <div class="overzicht-subtitle">prijs</div>
                <div class="overzicht-prijs"><?php echo('&euro;' . $prijs) ?> p.p.</div>
                <div class="overzicht-subtitle">reisdata</div>
                <div class="overzicht-info">
                  <p><strong><u>reis 1</u></strong></p>
                  <p>30/01 (aankomst 9:45 uur)</p>
                  <p>t/m 01/02 (vertrek 19:00 uur)</p>
                  <p class="br10">&nbsp;</p>
                  <p><strong><u>reis 2</u></strong></p>
                  <p>20/02 (aankomst 9:45 uur)</p>
                  <p>t/m 22/02 (vertrek 19:00 uur)</p>
                </div>
                <div class="overzicht-subtitle">opstapplaatsen</div>
                <div class="overzicht-info">Amsterdam, Leiderdorp, Delft, Rotterdam, De Meern (Utrecht), 's-Hertogenbosch, Nijmegen</div>
                <div class="overzicht-subtitle">wat is inclusief</div>
                <div class="overzicht-info">
                  <ul>
                    <li>Vervoer per luxe touringcar</li>
                    <li>Halte nabij accommodatie</li>
                    <li>Vervoer koffers, ski's etc.</li>
                    <li>2x overnachting</li>
                    <li>2x ontbijt</li>
                    <li>Skiberging accommodatie</li>
                    <li>Uitgebreide informatie</li>
                  </ul>
                </div>
                <div class="overzicht-subtitle">Exclusief</div>
                <div class="overzicht-info">
                  <ul>
                    <li>Diner, lunch en dranken</li>
                    <li>Skipas (va € 11,00)</li>
                    <li>Huurmateriaal</li>
                    <li>Reisverzekering</li>
                    <li>Boekingskosten € 4,50</li>
                  </ul>
                </div>
                <div class="overzicht-sluit"></div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    
    </div>
    
  </div>
</div>
<div id="foot-border"></div>
<div id="foot">
  <div id="foot-wrap">
    <div class="foot-boxs">
      <div class="foot-txtt">snel naar</div>
      <div class="foot-link"><a href="http://www.winterbergbus.nl" target="_blank">winterbergbus.nl</a><br />
        <a href="h_bedrijfsuitje_personeelsuitje_winterberg.html">bedrijfsuitje winterberg</a><br />
        <a href="h_studenten_schoolreis_winterberg_schoolreizen_snowworld_bottrop_school_reis_reizen_dag_willingen_winterbergen.html">schoolreis winterberg</a><br />
        <a href="l_uitleg_1_een_dag_dagje_dagtocht_winterberg_bus_skieen_snowboarden_langlaufen_wintersport_winterbergen.html" target="_blank">losse bustickets (dagje)</a><br />
        <a href="l_uitleg_lang_weekend_winterberg_midweek_week_sauerland_winterbergen.html" target="_blank">losse bustickets (weekend)</a><br />
      <a href="h_wat_is_er_te_doen_in_winterberg_sauerland_activiteiten.html">things to do</a></div>
    </div>
    <div class="foot-boxs">
      <div class="foot-txtt">winterberg actueel</div>
      <div class="foot-link"><a href="h_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">weer winterberg</a><br />
        <a href="h_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">sneeuw winterberg</a><br />
        <a href="h_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">pistes winterberg</a><br />
        <a href="h_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">webcams winterberg</a><br />
      <a href="http://www.nordicsport-arena.de/de/nordic-winter/" target="_blank">langlaufen winterberg</a></div>
    </div>
    <div class="foot-boxs">
      <div class="foot-txtt">winterbergbus</div>
      <div class="foot-link"><a href="h_personeelsuitje_winterberg_bedrijfsuitje_referenties_winterbergbus_ervaring_zoover_tripadvisor.html">onze klanten</a><br />
        <a href="h_winterbergbus_busreis_winterberg_busreizen_winterbergen_voorwaarden.html">voorwaarden busreizen</a><br />
        <a href="h_bedrijfsuitje_personeelsuitje_schoolreis_winterberg_aanvragen.php">offerte aanvragen</a><br />
        <a href="h_mailform.php">contact</a><br />
        <br />
        +31 (0)70 212 71 81
      </div>
    </div>
    <div class="foot-boxl">
      <div class="foot-txtt">volg ons</div>
      <div class="foot-social1">
        <ul>
          <li><a href="https://www.facebook.com/pages/Winterbergbusnl/304489432908047" target="_blank"><img src="bus_reis_winterberg_images/winterberg_facebook_winterbergbus.png" width="37" height="37" /></a></li>
          <li><a href="https://twitter.com/Winterbergbus" target="_blank"><img src="bus_reis_winterberg_images/winterberg_twitter_winterbergbus.png" width="37" height="37" /></a></li>
          <li><a href="http://www.pinterest.com/Winterbergbus/" target="_blank"><img src="bus_reis_winterberg_images/winterberg_pinterest_winterbergbus.png" width="37" height="37" /></a></li>
          <li><a href="http://www.instagram.com/Winterbergbus.nl" target="_blank"><img src="bus_reis_winterberg_images/winterberg_instagram_winterbergbus.png" width="37" height="37" /></a></li>
        </ul>
      </div>
      <div class="foot-social2">
        <ul>
          <li><a href="http://www.youtube.com/playlist?list=PLcMqWG9AdEF4FmaTATWPQ5dXtrWZ5Ovnv" target="_blank"><img src="bus_reis_winterberg_images/winterberg_film_youtube_winterbergbus.png" width="37" height="36" /></a></li>
          <li><a href="http://open.spotify.com/user/winterbergbus.nl/playlist/1Mg1TT63Wu3PR3qFi1Pn7m" target="_blank"><img src="bus_reis_winterberg_images/winterberg_wintersport_apres_ski_muziek_spotify_winterbergbus.png" width="37" height="37" /></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div id="foot-close">
  <div id="foot-close-wrap">
    <div class="foot-close-logo1">
      <ul>
        <li><img src="bus_reis_winterberg_images/logo_dutchweek_dutchweekend_winterberg_winterbergbus.png" width="119" height="21" /></li>
        <li><img src="bus_reis_winterberg_images/logo_apres_ski_apresski_winterberg_flying_deer_vliegend_hert_red_bull_jagermeister.png" width="37" height="45" /></li>
        <li><img src="bus_reis_winterberg_images/logo_hotel_der_brabander_winterberg_winterbergbus.png" width="84" height="45" /></li>
        <li><img src="bus_reis_winterberg_images/logo_wintersport_groupon_winterberg_sneeuw_winterbergbus_travelbird.png" width="88" height="25" /></li>
        <li><img src="bus_reis_winterberg_images/logo_vakantieveilingen_winterberg_wintersport_sneeuw_winterbergbus.png" width="105" height="31" /></li>
        <li><img src="bus_reis_winterberg_images/logo_rtl4_snowmagazine_rtl5_wintersport_sneeuw_winterberg_winterbergbus.png" width="44" height="45" /></li>
        <li><img src="bus_reis_winterberg_images/logo_bergfex_webcams_weerbericht_sneeuw_winterberg.png" width="95" height="31" /></li>
        <li><img src="bus_reis_winterberg_images/logo_snowworld_alpincenter_wintersport_winterberg.png" width="106" height="31" /></li>
      </ul>
    </div>
    <div class="foot-close-txt">© Winterbergbus Groepen™ is een merk van Winterbergbus Snow Included, KvK nummer 54143357<br />
      Winterbergbus Snow Included, Simon Carmiggelthof 158, 2492 JN Den Haag
    </div>
  </div>
</div>

<script type="text/javascript">
$('.thumb a').click( function(event){
    event.preventDefault();
    var grote = $(this).attr('href');
    $(this).parents('.accomodatie').find('.changer').html( "<img src=" + grote + " />" );
});
</script>

<script type="text/javascript">
function toggle() {
if(document.test["zend"].disabled==true) {
document.test["zend"].disabled=false;
} else {
document.test["zend"].disabled=true;
}
}
</script>

<script>

$.validate({
		   borderColorOnError : '#F000000',
		   addValidClassOnAll : true
		   });

var meldingen = {
	errorTitle : 'Form submission failed!',
	requiredFields : '<font color="#F000000">(?)</font>',
	badEmail : '<font color="#F000000">(?)</font>',
};

$.validate({
		   language : meldingen
		   });

</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
