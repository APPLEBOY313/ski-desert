<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: Winterbergbus.nl</title>
<meta name="viewport" content="width=1135px">
<link href="booking_css/booking_step_2.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
<link href="winterberg_wintersport_winterbergen_font/webfont.css" rel="stylesheet" type="text/css" />
<link href="winterberg_wintersport_winterbergen_font/icons.css" rel="stylesheet" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
</head>

<body>
<div id="booking-wrap">
  <div id="head">
    <div class="head-logo-winterbergbus"><a href="groupon_step_2.html"><img src="booking_images/logo_winterbergbus.png" width="80" height="45" /></a></div>
    <div class="head-txt">
      <ul>
        <li><a href="l_mijn_boeking.php" target="_blank"><span class="icon-lock"></span> Mijn boeking</a></li> |
        <li><a href="l_mailform.php" target="_blank"><span class="icon-mail"></span> Mail ons</a></li> |
        <li><a href="l_callform.php" target="_blank"><span class="icon-call"></span> Bel mij</a></li>
      </ul>
    </div>
  </div>
</div>
<div id="booking-line1">
  <div id="booking-row1">
    <ul>
      <li>verzilveren</li>
      <li class="step-blu">de busreis</li>
    </ul>
  </div>
</div>
<div id="booking-line2">
  <div id="booking-row2-1115">
    <div class="newsbox">
      <div class="colortop-blu-newsbox"></div>
      <div class="newsbox-row1">
        <div class="newsbox-row"><span class="icon-message"></span></div>
        <div class="newsbox-row20">nieuwtjes</div>
        <div class="newsbox-row14">
          <p class="blub">03/10/2017</p>
          <p>Nieuw in onze webshop: een <strong>skihelm</strong> reserveren!</p>
        </div>
        <div class="newsbox-row14">
          <p class="blub">25/09/2017</p>
          <p>Gereserveerde <strong>stoelen</strong> bij elkaar vanaf nu beschikbaar in onze webshop!</p>
        </div>
      </div>
    </div>
    <div class="samenstellen-box">
      <div class="colortop-blu"></div>
      <div class="samenstellen-box-row1">
        <div class="box-row"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></div>
        <div class="box-row26">stel je busreis samen</div>
        <div class="box-form"><form action="" method="post">
          <div class="box-form-title">vertrekplaats</div>
          <div class="box-formoption"><select name="from" class="box-text-input2" data-validation="required">
  <option value=""></option>
  <option value="amsterdam">Amsterdam</option>
  <option value="eindhoven">Eindhoven</option>
</select></div>
          <div class="box-form-title">bestemming</div>
          <div class="box-formoption"><select name="to" class="box-text-input2-frozen" data-validation="required">
  <option value=""></option>
  <option value="winterbergzursonne">Winterberg (centrum skigebied)</option>
  <option value="winterbergderbrabander">Winterberg (centrum skigebied)</option>
</select></div>
          <div class="box-form-title">vertrekdatum</div>
          <div class="box-formblok"><input name="departdate" type="text" class="box-text-input1" data-validation="required"/></div>
          
          
          
          <div class="box-form-title">aantal reizigers bus + choco + huur</div>
          <div class="box-formoption"><select name="grouponsmet" class="box-text-input2-frozen" id="numbergrouponsmet" data-validation="required">
  <option value=""></option>
  <option value="0">geen Groupons</option>
  <option value="1">1 Groupon</option>
  <option value="2">2 Groupons</option>
  <option value="3">3 Groupons</option>
  <option value="4">4 Groupons</option>
  <option value="5">5 Groupons</option>
  <option value="6">6 Groupons</option>
  <option value="7">7 Groupons</option>
  <option value="8">8 Groupons</option>
  <option value="9">9 Groupons</option>
  <option value="10">10 Groupons</option>
  <option value="11">11 Groupons</option>
  <option value="12">12 Groupons</option>
  <option value="13">13 Groupons</option>
  <option value="14">14 Groupons</option>
  <option value="15">15 Groupons</option>
  <option value="16">16 Groupons</option>
  <option value="17">17 Groupons</option>
  <option value="18">18 Groupons</option>
  <option value="19">19 Groupons</option>
  <option value="20">20 Groupons</option>
  <option value="21">21 Groupons</option>
  <option value="22">22 Groupons</option>
  <option value="23">23 Groupons</option>
  <option value="24">24 Groupons</option>
  <option value="25">25 Groupons</option>
</select>
            <div id="numbergrouponsmet-sub"></div>
          </div>
          <div class="box-form-title">aantal reizigers bus + huur</div>
          <div class="box-formoption"><select name="grouponszonder" class="box-text-input2-frozen" id="numbergrouponszonder" data-validation="required">
  <option value=""></option>
  <option value="0">geen Groupons</option>
  <option value="1">1 Groupon</option>
  <option value="2">2 Groupons</option>
  <option value="3">3 Groupons</option>
  <option value="4">4 Groupons</option>
  <option value="5">5 Groupons</option>
  <option value="6">6 Groupons</option>
  <option value="7">7 Groupons</option>
  <option value="8">8 Groupons</option>
  <option value="9">9 Groupons</option>
  <option value="10">10 Groupons</option>
  <option value="11">11 Groupons</option>
  <option value="12">12 Groupons</option>
  <option value="13">13 Groupons</option>
  <option value="14">14 Groupons</option>
  <option value="15">15 Groupons</option>
  <option value="16">16 Groupons</option>
  <option value="17">17 Groupons</option>
  <option value="18">18 Groupons</option>
  <option value="19">19 Groupons</option>
  <option value="20">20 Groupons</option>
  <option value="21">21 Groupons</option>
  <option value="22">22 Groupons</option>
  <option value="23">23 Groupons</option>
  <option value="24">24 Groupons</option>
  <option value="25">25 Groupons</option>
</select>
            <div id="numbergrouponszonder-sub"></div>
          </div>
          
          
          
          
          <div class="box-button"><input name="" type="button" class="button-addbustrip" value="toevoegen" /></div>
        </form></div>
      </div>
      <div class="colortop-pur2-error-20"></div>
      <div class="samenstellen-box-row2">
        <div class="box-row26-error">het spijt ons :(</div>
        <div class="box-row14-error">In deze bus zijn niet voldoende stoelen vrij. Er komen geen extra stoelen beschikbaar voor deze datum.</div>
        <div class="box-row14-error-title">wat wil je doen? (klik op je keuze)</div>
        <div class="box-row14-error">
          <p><u>Sluit deze pagina af en geef mijn codes vrij, ik kom later terug</u></p>
          <p><u>Ik probeer een andere vertrekplaats</u></p>
          <p><u>Ik probeer een andere vertrekdatum</u></p>
        </div>
      </div>
    </div>
    <div class="samenstellen-overzicht">
      <div class="colortop-pur2-300"></div>
      <div class="overzicht-row1">
        <div class="overzicht-row"><span class="icon-travel"></span></div>
        <div class="overzicht-row20">dit is je arrangement</div>
        <div class="overzicht-row14-title">verzilverd</div>
        <div class="overzicht-row12">
          <p>3 x groupon bus + choco + huur</p>
          <p>6 x groupon bus + choco</p>
        </div>
        <div class="overzicht-row14-title">de busreis</div>
        <div class="overzicht-row12">
          <p><strong>aantal reizigers</strong></p>
          <p>9 </p>
          <p class="br4">&nbsp;</p>
          <p><strong>zaterdag 26 januari 2018, 3:00 uur</strong></p>
          <p>amsterdam → winterberg</p>
          <p class="br4">&nbsp;</p>
          <p><strong>zaterdag 26 januari 2018, 18:45 uur</strong></p>
          <p>winterberg → amsterdam</p>
        </div>
        <div class="overzicht-row14-title">webshop</div>
        <div class="overzicht-row-next"><input name="" type="button" class="button-next" value="naar de webshop" /></div>
        <div class="overzicht-row14-light-title">persoonlijke gegevens</div>
        <div class="overzicht-row12-light"></div>
        <div class="overzicht-row14-light-title">overzicht</div>
      </div>
      <div class="overzicht-close"></div>
    </div>
  </div>
</div>
<div id="foot-colortop"></div>
<div id="foot">
  <div id="foot-wrap">
    <div class="foot-box">
      <div class="foot-box-txtt">meer info</div>
      <div class="foot-box-link">
        <p><a href="https://www.winterbergbus.nl" target="_blank">winterbergbus.nl home</a></p>
        <p><a href="l_uitleg_1_een_dag_dagje_dagtocht_winterberg_bus_skieen_snowboarden_langlaufen_wintersport_winterbergen.html" target="_blank">dagje winterberg</a></p>
        <p><a href="l_winterberg_skipas_ski_snowboard_langlauf_slee_skihelm_huur_huren_prijs_prijzen_skiles_snowboardles_skischool_snowboardschool_winterberg_winterbergen.html" target="_blank">in winterberg</a></p>
        <p><a href="https://www.winterbergwinkel.nl" target="_blank">winterberg webshop</a></p>
        <p><a href="https://www.winterberghotel.nl" target="_blank">hotels winterberg</a></p>
      </div>
    </div>
    <div class="foot-box">
      <div class="foot-box-txtt">over winterberg</div>
      <div class="foot-box-link">
        <p><a href="l_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html" target="_blank">weer &amp; sneeuw</a></p>
        <p><a href="https://www.skiliftkarussell.de/nl/skigebiet/panorama-karte.html" target="_blank">plattegrond skigebied</a></p>
        <p><a href="l_wat_is_er_te_doen_in_winterberg_sauerland_activiteiten.html" target="_blank">things to do</a></p>
        <p><a href="http://www.nordicsport-arena.de/de/nordic-winter/" target="_blank">langlaufen</a></p>
      </div>
    </div>
    <div class="foot-box">
      <div class="foot-box-txtt">winterbergbus.nl</div>
      <div class="foot-box-link">
        <p><a href="l_mijn_boeking.php" target="_blank">boeking wijzigen</a></p>
        <p><a href="l_mijn_boeking.php" target="_blank">boeking annuleren</a></p>
        <p><a href="https://www.winterbergbus.nl/h_bedrijfsuitje_personeelsuitje_winterberg.html" target="_blank">met een groep op reis</a></p>
        <p><a href="l_winterbergbus_busreis_busreizen_winterberg_winterbergen_voorwaarden.html" target="_blank">algemene voorwaarden</a></p>
        <p>&nbsp;</p>
        <p><a href="l_mailform.php" target="_blank"><span class="icon-mail"></span> mail ons</a></p>
        <p><a href="l_callform.php" target="_blank"><span class="icon-call"></span> bel mij</a></p>
      </div>
    </div>
    <div class="foot-box">
      <div class="foot-box-txtt">volg ons</div>
      <div class="foot-box-social">
        <ul>
          <li><a href="https://www.facebook.com/Winterbergbus/" target="_blank"><img src="booking_images/winterbergbus_facebook_winterberg.png" width="45" height="45" /></a></li>
          <li><a href="https://twitter.com/Winterbergbus" target="_blank"><img src="booking_images/winterbergbus_twitter_winterberg.png" width="45" height="45" /></a></li>
          <li><img src="booking_images/winterbergbus_instagram_winterberg.png" width="45" height="45" /></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div id="dagje-winterberg-bus">
  <div id="dagje-winterbergen-bus">
    <div class="busreis-winterberg">
      <p>© Winterbergbus™ is onderdeel van Winterbergbus Snow Included™, KvK nummer 54143357</p>
      <p>Winterbergbus Snow Included, Simon Carmiggelthof 158, 2492 JN Den Haag</p>
    </div>
  </div>
</div>
<script>
$( "#numbergrouponsmet" )
.change(function () {
	var text = "";
	var i = 0;
	var z = $( "#numbergrouponsmet" ).val();
	while (i < z) {
		i++;
		text += "<div class='numbergroupons'>controlecode coupon ";
		text += i;
		text += ": <input class='numbergroupons-input checkme' placeholder='hier 10 karakters' type='text' name='grouponcontrolecode";
		text += i;
		text += "' data-validation='required' pattern='.{10,10}' oninvalid=\"setCustomValidity('Dit klopt niet. De controlecode bestaat uit 10 karakters. ')\" onchange=\"try{setCustomValidity('')}catch(e){}\" /></div>";
		text += "<div class='numbergroupons'>grouponcode coupon ";
		text += i;
		text += ": <input class='numbergroupons-input' placeholder='Grouponcode begint met MH' type='text' name='grouponcode";
		text += i;
		text += "' data-validation='required' /></div>";
	}
	document.getElementById("numbergrouponsmet-sub").innerHTML = text;
	$( ".numbergroupons:odd" );
})
.change();
</script>
<script>
$( "#numbergrouponszonder" )
.change(function () {
	var text = "";
	var i = 0;
	var z = $( "#numbergrouponszonder" ).val();
	while (i < z) {
		i++;
		text += "<div class='numbergroupons'>controlecode coupon ";
		text += i;
		text += ": <input class='numbergroupons-input checkme' placeholder='hier 10 karakters' type='text' name='grouponcontrolecode";
		text += i;
		text += "' data-validation='required' pattern='.{10,10}' oninvalid=\"setCustomValidity('Dit klopt niet. De controlecode bestaat uit 10 karakters. ')\" onchange=\"try{setCustomValidity('')}catch(e){}\" /></div>";
		text += "<div class='numbergroupons'>grouponcode coupon ";
		text += i;
		text += ": <input class='numbergroupons-input' placeholder='Grouponcode begint met ZH' type='text' name='grouponcode";
		text += i;
		text += "' data-validation='required' /></div>";
	}
	document.getElementById("numbergrouponszonder-sub").innerHTML = text;
	$( ".numbergroupons:odd" );
})
.change();
</script>
</body>
</html>