  function cycleImages()
  {
    var $active = $('#cycler .active');
    var $next = ($active.next().length > 0) ? $active.next() : $('#cycler img:first');
    var $checkme = "ja";

    if ( $active.hasClass("eerste") )
    {
      var $next = $('#cycler .active');
      var $checkme = "nee";
	  };

    if( $checkme === "ja")
    {
      $next.css('z-index',2);//move the next image up the pile
    };

    var $activediv = $('#drop .active');
    var $nextdiv = ($activediv.next().length > 0) ? $activediv.next() : $('#drop div:first');

    $activediv.delay(600).animate({"marginTop": "0px"}, 500 );

  	if( $checkme === "ja")
    {
      $active.fadeOut(1500,function()
      {
        //fade out the top image
	      $active.css('z-index',1).show().removeClass('active');//reset the z-index and unhide the image
        $next.css('z-index',3).addClass('active');//make the next image the top one
      });
    };

    if ( $checkme === "nee" ) $active.removeClass("eerste");

    $activediv.delay(5500).animate({"marginTop": "300px"}, 400 ).removeClass("active").removeAttr("style");
    $nextdiv.addClass("active").stop();
  }

  function mijnupdate()
  {
    var slideId = parseInt($('#updater').attr('value'));
    $('#updater').attr('value',slideId+1);
    $('#updater').change();
  }

  $(document).ready(function()
  {
    // run every 7s
    $("#drop .active").css('marginTop', 0);
    cycleImages();
    setInterval('cycleImages()', 7000);

    setInterval('mijnupdate()', 400);
  });

  $("form > *").change(function()
  {
    var fields = $("#kalender, #updater, form select");
    var filledFields = fields.filter(function()
    {
      if( $(this).val() == null ) return false;

      return $(this).val().length > 0;
    });

    if (filledFields.length == fields.length)
    {
      $("input[type='submit']").removeAttr("disabled");
      $("input[type='submit']").removeClass("inactief").addClass("actief");
    }
    else
    {
      $("input[type='submit']").attr("disabled", 'disabled');
      $("input[type='submit']").removeClass("actief").addClass("inactief");
    }
  });

//$( "#kalender" ).datepicker();

//var monthNames = $( "#kalender" ).datepicker( "option", "monthNames" );
//$( "#kalender" ).datepicker( "option", "monthNames", [ "Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December" ] );
//
//var dayNamesMin = $( "#kalender" ).datepicker( "option", "dayNamesMin" );
//$( "#kalender" ).datepicker( "option", "dayNamesMin", [ "Zo", "Ma", "Di", "Wo", "Do", "Vr", "Za" ] );
//
//var dateFormat = $( "#kalender" ).datepicker( "option", "dateFormat" );
//$( "#kalender" ).datepicker( "option", "dateFormat", "dd-mm-yy" );
//
//// onderstaande is niet nodig, maar ik zet hem er toch tussen mocht je de dag erbij willen zetten bij het invullen (DD dd-mm-yy dan hierboven)
//var dayNames = $( "#kalender" ).datepicker( "option", "dayNames" );
//$( "#kalender" ).datepicker( "option", "dayNames", [ "Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag" ] );
