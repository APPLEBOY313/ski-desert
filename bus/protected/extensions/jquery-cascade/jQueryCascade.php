<?php
class jQueryCascade extends CWidget
{
    private $baseUrl;
    public $dependentNothingFoundLabel      = "No elements found";
    public $dependentStartingLabel          = "Please select one";
    public $dependentLoadingLabel           = "Loading ...";
    public $form                            = null;
    public $model                           = null;
    public $source                          = '';
    public $cascaded                        = '';
    public $extraParams                     = '';
    public $id                              = '';
    public $selected                        = '';
    public $data                            = array();
    public $htmlOptions                     = array();

    public function init()
    {
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'js';
        $this->baseUrl = Yii::app()->getAssetManager()->publish($assets);
        $this->registerClientScript();

        parent::init();
    }

    protected function registerClientScript()
    {
        $js = $this->baseUrl . '/jquery.cascade-select.js';
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerScriptFile($js);
    }

    public function run()
    {
        if (isset($this->form) && isset($this->cascaded))
        {
            $script = "$('#" . CHtml::getIdByName(CHtml::resolveName($this->model, $this->id)) . "').cascade({source:'" . $this->source . "',cascaded:'" . CHtml::getIdByName(CHtml::resolveName($this->model, $this->cascaded)) . "',dependentNothingFoundLabel:'".$this->dependentNothingFoundLabel."',dependentStartingLabel:'".$this->dependentStartingLabel."',dependentLoadingLabel:'".$this->dependentLoadingLabel."' ,extraParams:{".$this->extraParams."}});";
            Yii::app()->clientScript->registerScript('jQueryCascade' . CHtml::resolveName($this->model, $this->id), $script, CClientScript::POS_READY);

            echo $this->form->dropDownListRow($this->model,$this->id, $this->data, $this->htmlOptions);
        }
    }
}