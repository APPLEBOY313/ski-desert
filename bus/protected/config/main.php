<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Winterberg boekingen',
    'sourceLanguage' => 'en_us',
    'language' => 'nl',
    'theme'=>'default',

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.extensions.yii-mail.*',
	),
    'preload' => array('bootstrap'),

    'modules'=>array('admin'),

    'behaviors' => array('application.components.ApplicationConfigBehavior'),

	'defaultController'=>'default',

    'controllerMap'=>array(
        'min'=>array(
            'class'=>'ext.minScript.controllers.ExtMinScriptController',
        ),
    ),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
	/*
        'clientScript'=>array(
            'class'=>'ext.minScript.components.ExtMinScript',
        ),
	*/
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'php',
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=p41870_tbe',
			'emulatePrepare' => true,
			'username' => 'p41870_tbe',
			'password' => 'pommes0815',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
		),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
            'responsiveCss' => true,
        ),
        /*
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
        */
		'urlManager'=>array(
			'urlFormat'=>'path',
		        'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
);
