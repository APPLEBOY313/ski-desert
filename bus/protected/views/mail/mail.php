  <fieldset style="margin-bottom: 20px; font-weight: bold;">
    <legend>Beste wintersporter,</legend>
    <p style="margin-left:10px; font-weight: normal;">Bedankt voor je reservering en welkom (terug) bij Winterbergbus. Deze e-mail is je boekingsbevestiging en tevens je ticket voor de reis naar Winterberg. Neem deze e-mail geprint of digitaal mee tijdens de reis.</p>
    <p style="margin-left:10px; font-weight: normal;"><strong>Reisinformatie</strong></p>
    <p style="margin-left:10px; font-weight: normal;">We hebben alle belangrijke informatie voor je op een rijtje gezet. Vergeet niet deze informatie door te nemen. Klik hiervoor op onderstaande link:<br />
      <br />
    <a href="http://www.winterbergbus.nl/reisinformatie.html">Reisinformatie dagje Winterberg</a></p>
    <p style="margin-left:10px; font-weight: normal;"><strong>Winterbergwinkel: maak je reis compleet</strong></p>
    <p style="margin-left:10px; font-weight: normal;">Regel vooraf materiaalhuur, les, extra's zoals stoelen bij elkaar etc. voordelig online in onze eigen webwinkel: <strong>www.winterbergwinkel.nl</strong> (let op: huurmateriaal en les zijn niet gegarandeerd beschikbaar zonder reservering vooraf).</p>
    <p style="margin-left:10px; font-weight: normal;">Zorg tot slot dat je bereikbaar bent op het mobiele telefoonnummer dat je aan ons hebt doorgegeven, in geval van wijzigingen, vertraging etc. ontvang je hierop een SMS.<br />
      <br />
      Wij wensen je een sportieve dag toe!<br />
      <br />
      Team Winterbergbus Snow Included<br />
    </p>
  </fieldset>

  <fieldset style="margin-bottom: 20px; font-weight: bold;">
    <legend>Overzicht</legend>
    <p style="margin-left:10px; font-weight: normal;">Je reist <?php echo $persons; ?> op <?php echo $datum; ?> naar <?php echo $besteming; ?> <?php if ($dagen == '1 dag'): ?><?php endif; ?> en vertrekt vanaf opstappunt:</p>
    <p style="margin-left:10px; "><?php echo $depart; ?></p>
    <p style="margin-left:10px; font-weight: normal;">Je wordt verzocht 10 minuten voor vertrek aanwezig te zijn. Om 18:45 uur vertrekt de bus terug naar Nederland.<br />
    </p>
  </fieldset>

  <fieldset style="margin-bottom: 20px;font-weight: bold">
    <legend>Je gegevens</legend>
    <table style="width:400px; margin-left:10px; font-weight: normal;" cellpadding="2" cellspacing="2">
      <tr>
        <td style="width:150px;">Naam</td>
        <td><?php echo $model->voornaam.' '.$model->achternaam; ?></td>
      </tr>
      <tr>
        <td>Woonplaats</td>
        <td><?php echo $model->plaats; ?></td>
      </tr>
      <tr>
        <td>Email</td>
        <td><?php echo $model->email; ?></td>
      </tr>
      <tr>
      <tr>
        <td>Enquete</td>
        <td><?php echo $model->enquete; ?></td>
      </tr>
    </table>
  </fieldset>

  <?php if( count( $model->persons ) > 0 ) : ?>
    <fieldset style="margin-bottom: 20px; font-weight: bold;">
      <legend>Medereizigers</legend>
      <table style="width:400px; margin-left:10px; font-weight: normal;" cellpadding="0" cellspacing="0">
        <?php foreach($model->persons as $person): ?>
          <tr>
            <td style="width:150px;">Naam</td>
            <td><?php echo $person->voornaam . ' ' . $person->achternaam; ?></td>
          </tr>
        <?php endforeach; ?>
      </table>
    </fieldset>
  <?php endif; ?>

  <fieldset style="margin-bottom: 20px; font-weight: bold;">
    <p style="margin-left:10px; font-weight: normal;">Neem deze e-mail geprint of digitaal mee tijdens de reis. Er kan naar je legitimatie gevraagd worden.</p>
  </fieldset>
