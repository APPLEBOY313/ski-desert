<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 08-11-12
 * Time: 15:17
 */

class AdminModule extends CWebModule
{
    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(array(
            'admin.models.*',
            'admin.components.*',
        ));

        $this->layout = 'main';
        Yii::app()->homeUrl = Yii::app()->createUrl('/admin');
    }
}