<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 09-11-12
 * Time: 11:12
 */

class SettingsForm extends CFormModel
{
    public $username;
    public $password;
    public $bedankt_msg;
    public $booking_cost;

    public function rules()
    {
        return array(
            // username and password are required
            array('username, password, bedankt_msg, booking_cost', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'username'=>'Gebruikersnaam',
            'password'=>'Wachtwoord',
            'bedankt_msg'=>'Bedankt pagina',
            'booking_cost'=>'Boekingskosten',
        );
    }
}