<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 09-11-12
 * Time: 10:36
 */

class SettingsController extends BackendController
{
    public function actionIndex()
    {
        $model = new SettingsForm();
        if(isset($_POST['SettingsForm']))
        {
            $model->attributes=$_POST['SettingsForm'];
            if($model->validate())
            {
                // Save settings
                Setting::setValue('username',$model->username);
                Setting::setValue('password',$model->password);
                Setting::setValue('bedankt_msg',$model->bedankt_msg);
                Setting::setValue('booking_cost',$model->booking_cost);
            }
        }
        else
        {
            // Load settings
            $model->username = Setting::getValue('username');
            $model->password = Setting::getValue('password');
            $model->bedankt_msg = Setting::getValue('bedankt_msg');
            $model->booking_cost = Setting::getValue('booking_cost');
        }
        $this->render('index',array('model'=>$model));
    }
}