<?php
  /**
   * Created by CentrioSoft.
   * User: Jean van den Bogaard
   * Date: 20-11-12
   * Time: 14:25
   */

  class BookingsController extends BackendGridController
  {
    protected $model        ='Booking';
    protected $title        ='Boekingen';
    protected $title_single ='Boeking';

    protected $old_depart_id;

//    public function beforeDelete($model)
//    {
//      if ($model->betaald == 1)
//      {
//        $event = $model->event->event;
//        if ($event)
//        {
//          $event->booked = $event->booked - $model->members;
//          if ($event->booked < 0) $event->booked = 0;
//          $event->save();
//        }
//      }
//    }

    public function getEventDepartList()
    {
      $result =array();

      $criteria =new CDbCriteria( );
      $criteria->join ="LEFT JOIN tbl_event AS event ON event.id = t.event_id";
      $criteria->order ="event.depart ASC, t.depart ASC";

      $items =EventDepart::model()->findAll( $criteria );
      foreach($items as $item)
      {
        if ($item->event!=null)
        {
          $result[$item->id] = date('d-m-Y',strtotime($item->event->depart)).' - '.date('H:i',strtotime($item->depart)) .' - ' . ( $item->city != NULL ? $item->city->name : '' ) . ' - '. ( $item->city != NULL ? $item->city->description : '' );
        }
      }
      return $result;
    }

    public function beforeSave( $model )
    {
      // Store the old depart id for later processing
      $dummy =Booking::model()->findByPk( $model->id );
      if( $dummy ) $this->old_depart_id =$dummy->event_depart_id;
      else         $this->old_depart_id =null;
    }

    public function afterSave( $model )
    {
      // Process the list
      if( isset( $_POST['Booking']['BookingPerson'] ))
      {
        // Delete all the ID's that are removed by the user
        foreach( $model->persons as $item )
        {
          $found = false;
          foreach( $_POST['Booking']['BookingPerson']['id'] as $person )
          {
            if( $person == '' ) continue;

            if( $person == $item->id ) $found = true;
          }

          if( !$found ) $item->delete();
        }

        // Add all the records to the database
        foreach( $_POST['Booking']['BookingPerson']['id'] as $key => $index )
        {
          if( $index == '' ) $item =new BookingPerson( );
          else               $item =BookingPerson::model()->findByPk( $index );

          // Only store if both time and price are used
          if( $_POST['Booking']['BookingPerson']['voornaam'][$key] != '' && $_POST['Booking']['BookingPerson']['achternaam'][$key] != '' )
          {
            $item->booking_id =$model->id;
            $item->voornaam   =$_POST['Booking']['BookingPerson']['voornaam'][$key];
            $item->achternaam =$_POST['Booking']['BookingPerson']['achternaam'][$key];
            $item->save( );
          }
        }

        // Store the number of members
        $model->members =count( $_POST['Booking']['BookingPerson']['id'] );
        $model->update();
      }

      if( $this->old_depart_id != null )
      {
        // Update the booked count for the old depart
        $dummy =EventDepart::model()->findByPk( $this->old_depart_id );

        // Create a query to count the number of payed bookings for a certain event
        $criteria =new CDbCriteria( );
        $criteria->join  ='LEFT JOIN tbl_event_depart as depart ON depart.id = t.event_depart_id ';
        $criteria->join .='LEFT JOIN tbl_event as event ON event.id = depart.event_id';
        $criteria->addCondition( "event.id ='" . $dummy->event->id . "'" );
        $criteria->addCondition( "t.betaald ='1'" );

        $booked   =0;
        $bookings =Booking::model()->findAll( $criteria );
        foreach( $bookings as $booking )
        {
          if( $booking->members == 0 ) $booked++;
          else                         $booked +=$booking->members;
        }

        // Create a query to count the number of reserved bookings for a certain event
        $criteria =new CDbCriteria( );
        $criteria->join  ='LEFT JOIN tbl_event_depart as depart ON depart.id = t.event_depart_id ';
        $criteria->join .='LEFT JOIN tbl_event as event ON event.id = depart.event_id';
        $criteria->addCondition( "event.id ='" . $dummy->event->id  . "'" );
        $criteria->addCondition( "t.betaald ='0'" );

        $reserved =0;
        $bookings =Booking::model()->findAll( $criteria );
        foreach( $bookings as $booking )
        {
          if( $booking->members == 0 ) $reserved++;
          else                         $reserved +=$booking->members;
        }

        $dummy->event->booked   =$booked;
        $dummy->event->reserved =$reserved;
        $dummy->event->update( );
      }
    }
  }