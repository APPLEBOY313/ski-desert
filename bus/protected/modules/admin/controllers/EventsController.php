<?php
  /**
   * Created by CentrioSoft.
   * User: Jean van den Bogaard
   * Date: 19-11-12
   * Time: 23:36
   */

  class EventsController extends BackendGridController
  {
    protected $model = 'Event';
    protected $title = 'Reizen';
    protected $title_single = 'Reis';

    public function beforeSave( $model )
    {
      // Save days
      if( isset( $_POST['Event']['days'] ))
      {
        $days =serialize( $_POST['Event']['days'] );

        $model->days =$days;
      }
      else
      {
        $model->days =serialize( array( ));
      }
    }

    public function afterSave( $model )
    {
      // Process the list
      if( isset( $_POST['Event']['EventDepart'] ))
      {
        // Delete all the ID's that are removed by the user
        foreach( $model->departs as $item )
        {
          $found = false;
          foreach( $_POST['Event']['EventDepart']['id'] as $event_depart )
          {
            if( $event_depart == '' ) continue;

            if( $event_depart == $item->id ) $found = true;
          }

          if( !$found ) $item->delete();
        }

        // Add all the records to the database
        foreach( $_POST['Event']['EventDepart']['id'] as $key => $index )
        {
          if( $index == '' ) $item =new EventDepart( );
          else               $item =EventDepart::model()->findByPk( $index );

          // Only store if both time and price are used
          if( $_POST['Event']['EventDepart']['time'][$key] != '' && $_POST['Event']['EventDepart']['price'][$key] != '' )
          {
            $item->event_id =$model->id;
            $item->city_id  =$_POST['Event']['EventDepart']['city_id'][$key];
            $item->depart   =$_POST['Event']['EventDepart']['time'][$key];
            $item->price    =$_POST['Event']['EventDepart']['price'][$key];
            $item->save( );
          }
        }
      }
    }

    public function actionLijst( $id )
    {
      $event =Event::model()->findByPk( $id );

      $this->render( 'overview', array( 'event' => $event ));
    }
  }