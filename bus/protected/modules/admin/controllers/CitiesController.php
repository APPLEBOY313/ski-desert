<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 20-11-12
 * Time: 01:05
 */

class CitiesController extends BackendGridController
{
    protected $model            = 'City';
    protected $title            = 'Opstapplaatsen';
    protected $title_single     = 'Opstapplaats';

}