<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 08-11-12
 * Time: 16:09
 */

class DefaultController extends BackendController
{
    public function actionIndex()
    {
        $this->setBreadCrumbs(array());
        $this->render('index');
    }

}