<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 08-11-12
 * Time: 15:20
 */

class AuthController extends CController
{
    public function init()
    {
        parent::init();
        Yii::app()->theme = 'admin';
    }
    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        $model=new AdminLoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['AdminLoginForm']))
        {
            $model->attributes=$_POST['AdminLoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect('/admin');
        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect('/admin/auth/login');
    }

    public function actionTest()
    {
        $this->layout = 'main';
        $this->render('test');
    }

}