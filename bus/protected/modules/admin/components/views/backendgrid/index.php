<?php
$form=$this->beginWidget('CActiveForm', array(
    'enableAjaxValidation'=>true,
));
?>
<div class="row">
    <!-- Data block -->
    <article class="span12 data-block">
        <div class="data-container">
            <header>
                <h2><?php echo $this->title; ?></h2>
            </header>
            <?php $this->widget('bootstrap.widgets.TbAlert'); ?>
            <div class="row">
                <div class="span8" style="margin-bottom:0px;">
                    <?php $this->widget('bootstrap.widgets.TbButton',array(
                    'label' => $this->title_single.' toevoegen',
                    'type' => 'primary',
                    'size' => 'large',
                    'url' => Yii::app()->request->requestUri.'/create'
                )); ?>
                </div>
                <div class="span4" style="margin-bottom:0px;">
                    <?php if (isset(Yii::app()->session['searchText_'.$this->model])) { $searchText = Yii::app()->session['searchText_'.$this->model]; } else { $searchText = ''; } ?>
                    Zoeken: <input type="text" name="grid_search_text" id="grid_search_text" value="<?php echo $searchText; ?>"">
                    <?php
                    $script = "";
                    $script .= 'function reloadGrid(data) {$.fn.yiiGridView.update("SearchGrid");}';
                    $script .= "$('body').on('change','#grid_display_items',function(){";
                    $script .= "jQuery.ajax({'success':reloadGrid,'type':'POST','url':'".Yii::app()->request->requestUri."/change_items','cache':false,'data':jQuery(this).parents(\"form\").serialize()});return false;";
                    $script .= "});\r\n";
                    $script .= "var timeId = null;";
                    $script .= "$('#grid_search_text').bind('keypress',function(e){";
                    $script .= "var code = (e.keyCode ? e.keyCode : e.which);";
                    $script .= "if(code == 13) {";
                    $script .= "return false;";
                    $script .= "}";
                    $script .= "});";
                    $script .= "$('#grid_search_text').bind('keyup',function(e){";
                    $script .= "clearTimeout(timeId);";
                    $script .= "timeId = setTimeout(function(){";
                    $script .= "jQuery.ajax({'success':reloadGrid,'type':'POST','url':'".Yii::app()->request->requestUri."/search','cache':false,'data':$('#grid_search_text').parents(\"form\").serialize()});";
                    $script .= "},500);";
                    $script .= "});\r\n";
                    Yii::app()->clientScript->registerScript('GH_SCRIPT',$script);
                    ?>
                </div>
            </div>
            <?php
            $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                'id'=>'SearchGrid',
                'dataProvider' => $model->search(),
                'type' => 'striped bordered condensed',
                'summaryText' => false,
                'columns' =>CMap::mergeArray(
                    $model->getColomns(),
                    array(
                        array(
                            'htmlOptions' => array('nowrap'=>'nowrap'),
                            'class'=>'bootstrap.widgets.TbButtonColumn',
                            'header'=>'Actions',
                            'template'=>'{update} {delete}',
                        )
                    )
                ),
            ));
            ?>
        </div>
    </article>
</div>
<?php $this->endWidget(); ?>
