<div class="row">
    <!-- Data block -->
    <article class="span12 data-block">
        <div class="data-container">

            <header>
                <h2><?php echo $this->title_single; ?> bewerken</h2>
            </header>
            <?php $this->widget('bootstrap.widgets.TbAlert'); ?>
            <?php echo $form ?>
        </div>
    </article>
</div>