<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class AdminUserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        // TODO replace fixed username and password with items from settings
        if (strtolower($this->username)==Setting::getValue('username'))
        {
            if (strtolower($this->password)==Setting::getValue('password'))
            {
                $this->_id = 1;
                $this->username = $this->username;
                $this->errorCode = self::ERROR_NONE;
            }
            else
            {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
        }
        else
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
		return $this->errorCode==self::ERROR_NONE;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}