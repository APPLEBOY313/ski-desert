<?php
/**
 * Created by CentrioSoft
 * User: Jean van den Bogaard
 * Date: 08-11-12
 * Time: 15:14
 */

class BackendController extends CController
{
    private $_breadcrumbs = array();

    public function init()
    {
        parent::init();
        Yii::app()->theme = 'admin';
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'users'=>array('?'),
            ),
        );
    }

    public function getBreadCrumbs()
    {
        return $this->_breadcrumbs;
    }

    public function setBreadCrumbs($values)
    {
        if (is_array($values))
        {
            $this->_breadcrumbs = $values;
        }
    }

    public function addBreadCrumb($key, $value)
    {
        $this->_breadcrumbs[$key] = $value;
    }

/*
   public function __set($name, $value)
   {
       if (method_exists($this, 'set'.$name))
       {
           $method = 'set'.$name;
           $this->$method($value);
       }
       else
       {
           throw new OutOfBoundsException('Member is not settable');
       }
   }

    public function __get($name) {
        if (method_exists($this, 'get'.$name)) {
            $method = 'get' . $name;
            return $this->$method();
        } else {
            throw new OutOfBoundsException('Member is not gettable');
        }
    }

*/

}