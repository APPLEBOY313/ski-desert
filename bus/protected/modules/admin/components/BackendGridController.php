<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 19-11-12
 * Time: 18:36
 */

class BackendGridController extends BackendController
{
    protected $model             = '';
    protected $title             = '';
    protected $title_single      = '';

    // Basic actions
    public function actionIndex()
    {
        $this->setBreadCrumbs(array($this->title));
        $this->setPageTitle($this->title);

       $this->render('admin.components.views.backendgrid.index',array('model'=>$this->loadModel()));

    }

    public function actionCreate()
    {
        $this->setBreadCrumbs(array($this->title=>$this->createUrl('/'. $this->module->getId() . '/' . $this->getId() ),$this->title_single.' toevoegen'));
        $this->setPageTitle($this->title_single.' toevoegen');

        $this->render('admin.components.views.backendgrid.create',array(
                'form'=>$this->renderPartial('_form',array('model'=>$this->saveModel()),true)
            )
        );

    }

    public function actionUpdate($id)
    {
        $this->setBreadCrumbs(array($this->title=>$this->createUrl('/'. $this->module->getId() . '/' . $this->getId() ),$this->title_single.' bewereken'));
        $this->setPageTitle($this->title_single.' bewerken');

        $this->render('admin.components.views.backendgrid.update',array(
                'form'=>$this->renderPartial('_form',array('model'=>$this->saveModel($id)),true)
            )
        );
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        if ($model)
        {
            if (method_exists($this,'beforeDelete')) $this->beforeDelete($model);
            $model->delete();
            if (method_exists($this,'afterDelete')) $this->afterDelete($id);
        }
    }

    public function actionSearch()
    {
        if (isset($_POST['grid_search_text']))
        {
            Yii::app()->session['searchText_'.$this->model] = $_POST['grid_search_text'];
        }
    }

    // Load model
    public function loadModel($id = -1)
    {
        $model = call_user_func($this->model."::model")->findByPk($id);
        if ($model === null )
        {
            $model = new $this->model;
        }
        return $model;
    }

    // Save model
    public function saveModel($id = -1)
    {
        $model = $this->loadModel($id);
        if (isset($_POST[$this->model]))
        {
            $model->attributes=$_POST[$this->model];
            if (method_exists($this,'beforeSave')) $this->beforeSave($model);
            if($model->save())
            {
                if (method_exists($this,'afterSave')) $this->afterSave($model);

                Yii::app()->user->setFlash('success','Gegevens zijn opgeslagen');
                $this->redirect($this->createUrl('/'. $this->module->getId() . '/' . $this->getId() ));
            }
            Yii::app()->user->setFlash('error','Gegevens zijn niet opgeslagen');
        }
        return $model;
    }

}