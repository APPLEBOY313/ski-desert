<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 22-11-12
 * Time: 10:40
 */

class BookingStep1 extends CFormModel
{
    public $dayId = 0;
    public $cityId = 0;
    public $dateId = 0;
    public $personId = 0;

    public function rules()
    {
        return array(
            array('dayId, cityId, dateId, personId', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'dayId'         => 'Aantal dagen',
            'cityId'        => 'Vertrekplaats',
            'dateId'        => 'Datum vertrek',
            'personId'      => 'Aantal personen'
        );
    }
}