<?php
  class Step2 extends CFormModel
  {
    public $voornaam;
    public $achternaam;
    public $plaats;
    public $email;
    public $email2;
    public $telefoon1;
    public $telefoon2;

    public $voornaam1;
    public $achternaam1;
    public $voornaam2;
    public $achternaam2;
    public $voornaam3;
    public $achternaam3;
    public $voornaam4;
    public $achternaam4;
    public $voornaam5;
    public $achternaam5;
    public $voornaam6;
    public $achternaam6;
    public $voornaam7;
    public $achternaam7;
    public $voornaam8;
    public $achternaam8;
    public $voornaam9;
    public $achternaam9;
    public $voornaam10;
    public $achternaam10;
    public $voornaam11;
    public $achternaam11;
    public $voornaam12;
    public $achternaam12;
    public $voornaam13;
    public $achternaam13;
    public $voornaam14;
    public $achternaam14;
    public $voornaam15;
    public $achternaam15;
    public $voornaam16;
    public $achternaam16;
    public $voornaam17;
    public $achternaam17;
    public $voornaam18;
    public $achternaam18;
    public $voornaam19;
    public $achternaam19;
    public $voornaam20;
    public $achternaam20;

    public function rules()
    {
      $voornamen   ='';
      $achternamen ='';

      $max =Yii::app()->session['people'];
      for( $i=1;$i<=$max;$i++ )
      {
        $voornamen   .="voornaam" . $i . ", ";
        $achternamen .="achternaam" . $i . ", ";
      }

      return array(
        array( 'voornaam, achternaam,  plaats, email, email2, telefoon1, telefoon2', 'required' ),
        array( $voornamen, 'required'),
        array( $achternamen, 'required'),
        array( 'email, email2', 'email' ),
        array( 'email2', 'compare', 'compareAttribute' => 'email' ),
      );
    }

    public function attributeLabels()
    {
      $results = array();
      $results['nr_ski'] = "Aantal ski's";
      $max = Yii::app()->session['people'];
      for($i=1; $i<=$max; $i++)
      {
        $results['voornaam'.$i]   ="Voornaam " . $i;
        $results['achternaam'.$i] ="Achternaam " . $i;
      }

      $results['email']      ='E-mail';
      $results['email2']     ='Herhaal E-mail';
      $results['plaats']     ='Uw woonplaats';
      $results['voornaam']   ='Voornaam';
      $results['telefoon1']  ='Telefoon';
      $results['telefoon2']  ='Telefoon (tijdens reis)';
      $results['achternaam'] ='Achternaam';

      return $results;
    }
  }