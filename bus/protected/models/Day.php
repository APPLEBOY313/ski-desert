<?php

  class Day extends CActiveRecord
  {
    public static function model( $className = __CLASS__ )
    {
      return parent::model( $className );
    }

    public function tableName()
    {
      return '{{day}}';
    }

    public function rules()
    {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
        array( 'name, number', 'required' ),
        array( 'name, number', 'safe' ),
        array( 'id, name', 'safe', 'on' => 'search' ),
      );
    }

    public function attributeLabels()
    {
      return array(
        'name'   => "Omschijving",
        'number' => "Aantal dagen",
      );
    }

    public function relations()
    {
      return array(
        'days' => array( self::HAS_MANY, 'EventDays', 'day_id' ),
      );
    }

    public function search()
    {
      $searchTxt = Yii::app()->session['searchText_' . __CLASS__];

      $criteria = new CDbCriteria;
      $criteria->compare( 'name', $searchTxt, true );

      return new CActiveDataProvider('Day', array(
        'criteria'   => $criteria,
        'pagination' => array(
          'pageSize' => 10,
          'pageVar'  => 'page',
        ),
      ));
    }

    public function getColomns()
    {
      return array(
        'id',
        'name',
        array(
          'header' => 'Aantal dagen',
          'value'  => '$data->number',
        )
      );
    }

    static public function getItemsAsArray()
    {
      $result =array();

      $items =Day::model()->findAll();
      foreach( $items as $item )
      {
        $result[$item->id] =$item->name;
      }

      return $result;
    }
  }