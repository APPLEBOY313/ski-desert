<?php
  /**
   * Created by CentrioSoft.
   * User: Jean van den Bogaard
   * Date: 09-11-12
   * Time: 10:17
   */

  class City extends CActiveRecord
  {
    public static function model($className=__CLASS__)
    {
      return parent::model($className);
    }

    public function tableName()
    {
      return '{{city}}';
    }

    public function rules()
    {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
        array('name','required'),
        array('name,description','safe'),
        array('id, name', 'safe', 'on'=>'search'),
      );
    }

    public function relations()
    {
      return array(
        'departs'=>array(self::HAS_MANY,'EventDepart','city_id'),
      );
    }

    public function search()
    {
      $searchTxt = Yii::app()->session['searchText_'.__CLASS__];
      $criteria=new CDbCriteria;

      $criteria->compare('name',$searchTxt,true);
      $criteria->order ='name ASC';

      return new CActiveDataProvider('City', array(
        'criteria'=>$criteria,
        'pagination'=>array(
          'pageSize'=>10,
          'pageVar'=>'page',
        ),
      ));
    }

    public function getColomns()
    {
      return array(
        'id',
        'name',
        'description',
      );
    }

  }