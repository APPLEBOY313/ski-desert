<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 20-11-12
 * Time: 15:10
 */

class BookingPerson extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{booking_person}}';
    }

    public function relations()
    {
        return array(
            'booking'=>array(self::BELONGS_TO, 'Booking', 'booking_id'),
        );
    }
}