<?php
  /**
   * Created by CentrioSoft.
   * User: Jean van den Bogaard
   * Date: 09-11-12
   * Time: 10:18
   */

  class Booking extends CActiveRecord
  {
    public $event_depart;
    public $event_city;

    public static function model($className=__CLASS__)
    {
      return parent::model($className);
    }

    public function tableName()
    {
      return '{{booking}}';
    }

    public function rules()
    {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
        array('event_depart_id, voornaam, achternaam, plaats, email, betaald, telefoon1, telefoon2','required'), // straat, postcode,
        array('email','email'),
        array('event_depart_id, voornaam, achternaam, straat, postcode, plaats, email, nr_ski, nr_snowboard, nr_langlauf' ,'safe'), // geboorte,
        array('id, voornaam, achternaam, postcode, plaats', 'safe', 'on'=>'search'),
      );
    }

    public function relations()
    {
      return array(
        'event'=>array(self::BELONGS_TO, 'EventDepart', 'event_depart_id'),
        'persons'=>array(self::HAS_MANY, 'BookingPerson','booking_id'),
      );
    }

    public function attributeLabels()
    {
      return array(
        'telefoon1'       => "Telefoon nummer",
        'telefoon2'       => "Telefoon tijdens reis",
        'nr_ski'          => "Aantal Ski's",
        'betaald'         => 'Betaald ?',
        'geboorte'        => 'Geboortedatum',
        'nr_langlauf'     => "Aantal Langlaufski's",
        'nr_snowboard'    => "Aantal Snowboards",
        'event_depart_id' => 'Vertrek datum en locatie',
      );
    }

    public function search()
    {
      $searchTxt =Yii::app()->session['searchText_'.__CLASS__];
      $criteria=new CDbCriteria;

      $criteria->compare( 'achternaam', $searchTxt, true );

      return new CActiveDataProvider('Booking', array(
        'criteria'=>$criteria,
        'pagination'=>array(
          'pageSize'=>10,
          'pageVar'=>'page',
        ),
        'sort'=>array(
          'attributes'    => array(
            'id',
            'voornaam',
            'achternaam',
            'betaald',
            'event_id' => array(
              'asc'=>'event.id ASC',
              'desc'=>'event.id DESC',
            ),
          ),
          'defaultOrder' => array("id"=>true),
        )
      ));
    }

    public function getColomns()
    {
      return array(
        'id',
        'voornaam',
        'achternaam',
        'email',
        array(
          'header'=>'Reis',
          'name'=>'event_id',
          'value'=>'(isset($data->event) && isset($data->event->event))?date("d-m-Y",strtotime($data->event->event->depart)). " - ".$data->event->depart ." - ". $data->event->city->name:""',
        ),
        array(
          'header'=>'Betaald',
          'name'=>'betaald',
          'value'=>'($data->betaald)?"Ja":"Nee"',
        ),
        array(
          'header'=>'Via',
          'name'=>'betaald_via',
          'value'=>'$data->betaald_via',
        ),
      );
    }

    public function afterSave()
    {
      // Calculate Booked places (people that paid)
      if( isset( $this->event ) && $this->event != '' )
      {
        $depart =$this->event;
        if( isset( $depart->event ) && $depart->event != '' )
        {
          // Get the event
          $event =$depart->event;

          // Create a query to count the number of payed bookings for a certain event
          $criteria =new CDbCriteria( );
          $criteria->join  ='LEFT JOIN tbl_event_depart as depart ON depart.id = t.event_depart_id ';
          $criteria->join .='LEFT JOIN tbl_event as event ON event.id = depart.event_id';
          $criteria->addCondition( "event.id ='" . $event->id . "'" );
          $criteria->addCondition( "t.betaald ='1'" );

          $booked   =0;
          $bookings =Booking::model()->findAll( $criteria );
          foreach( $bookings as $booking )
          {
            if( $booking->members == 0 ) $booked++;
            else                         $booked +=$booking->members;
          }

          $event->booked =$booked;
          $event->update( );
        }
      }

      // Calculate Reserved places (people that booked but have not payed yet)
      if( isset( $this->event ) && $this->event != '' )
      {
        $depart =$this->event;
        if( isset( $depart->event ) && $depart->event != '' )
        {
          // Get the event
          $event =$depart->event;

          // Create a query to count the number of payed bookings for a certain event
          $criteria =new CDbCriteria( );
          $criteria->join  ='LEFT JOIN tbl_event_depart as depart ON depart.id = t.event_depart_id ';
          $criteria->join .='LEFT JOIN tbl_event as event ON event.id = depart.event_id';
          $criteria->addCondition( "event.id ='" . $event->id . "'" );
          $criteria->addCondition( "t.betaald ='0'" );

          $reserved =0;
          $bookings =Booking::model()->findAll( $criteria );
          foreach( $bookings as $booking )
          {
            if( $booking->members == 0 ) $reserved++;
            else                         $reserved +=$booking->members;
          }

          $event->reserved =$reserved;
          $event->update( );
        }
      }
    }

    public function afterDelete( )
    {
      // Calculate Booked places (people that paid)
      if( isset( $this->event ) && $this->event != '' )
      {
        $depart =$this->event;
        if( isset( $depart->event ) && $depart->event != '' )
        {
          // Get the event
          $event =$depart->event;

          // Create a query to count the number of payed bookings for a certain event
          $criteria =new CDbCriteria( );
          $criteria->join  ='LEFT JOIN tbl_event_depart as depart ON depart.id = t.event_depart_id ';
          $criteria->join .='LEFT JOIN tbl_event as event ON event.id = depart.event_id';
          $criteria->addCondition( "event.id ='" . $event->id . "'" );
          $criteria->addCondition( "t.betaald ='1'" );

          $booked   =0;
          $bookings =Booking::model()->findAll( $criteria );
          foreach( $bookings as $booking )
          {
            if( $booking->members == 0 ) $booked++;
            else                         $booked +=$booking->members;
          }

          $event->booked =$booked;
          $event->update( );
        }
      }

      // Calculate Reserved places (people that booked but have not payed yet)
      if( isset( $this->event ) && $this->event != '' )
      {
        $depart =$this->event;
        if( isset( $depart->event ) && $depart->event != '' )
        {
          // Get the event
          $event =$depart->event;

          // Create a query to count the number of payed bookings for a certain event
          $criteria =new CDbCriteria( );
          $criteria->join  ='LEFT JOIN tbl_event_depart as depart ON depart.id = t.event_depart_id ';
          $criteria->join .='LEFT JOIN tbl_event as event ON event.id = depart.event_id';
          $criteria->addCondition( "event.id ='" . $event->id . "'" );
          $criteria->addCondition( "t.betaald ='0'" );

          $reserved =0;
          $bookings =Booking::model()->findAll( $criteria );
          foreach( $bookings as $booking )
          {
            if( $booking->members == 0 ) $reserved++;
            else                         $reserved +=$booking->members;
          }

          $event->reserved =$reserved;
          $event->update( );
        }
      }
    }
  }