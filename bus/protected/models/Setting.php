<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 09-11-12
 * Time: 10:28
 */

class Setting extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{setting}}';
    }

    static public function getValue($name,$default='')
    {
        $item = Setting::model()->find('name=:name',array('name'=>$name));
        if ($item)
        {
            return $item->value;
        }
        return $default;
    }

    static public function setValue($name, $value)
    {
        $item = Setting::model()->find('name=:name',array('name'=>$name));
        if (!$item)
        {
            $item = new Setting;
            $item->name = $name;
        }
        $item->value = $value;
        $item->save();
    }
}