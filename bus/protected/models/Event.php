<?php
  /**
   * Created by CentrioSoft.
   * User: Jean van den Bogaard
   * Date: 09-11-12
   * Time: 10:19
   */

  class Event extends CActiveRecord
  {
    public static function model( $className = __CLASS__ )
    {
      return parent::model( $className );
    }

    public function tableName()
    {
      return '{{event}}';
    }

    public function rules()
    {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
        array( 'depart, name, avail, days', 'required' ),
        array( 'depart, name, avail, days, booked', 'safe' ),
        array( 'id, depart', 'safe', 'on' => 'search' ),
      );
    }

    public function relations()
    {
      return array(
        'departs' => array( self::HAS_MANY, 'EventDepart', 'event_id' ),
      );
    }

    public function search()
    {
      $searchTxt = Yii::app()->session['searchText_' . __CLASS__];

      $criteria = new CDbCriteria;
      $criteria->compare( 'depart', $searchTxt, true );
      $criteria->order ="depart ASC";

      return new CActiveDataProvider('Event', array(
        'criteria'   => $criteria,
        'pagination' => array(
          'pageSize' => 100,
          'pageVar'  => 'page',
        ),
      ));
    }

    public function getColomns()
    {
      return array(
        'id',
        'name',
        'depart',
        'reserved',
        'booked',
        'avail',
        array(
          'header' => 'Boekingen',
          'class' => 'bootstrap.widgets.TbButtonColumn',
          'template' => '{create}',
          'buttons' => array(
            'create' => array(
              'label' => 'Overzicht Bekijken',
              'icon'  => 'list',
              'url'   => 'Yii::app()->createUrl( "admin/events/lijst/id/" . $data->id )',
            ),
          ),
        ),
      );
    }
  }
