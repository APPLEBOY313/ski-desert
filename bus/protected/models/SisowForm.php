<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 24-11-12
 * Time: 02:35
 */

class SisowForm extends CFormModel
{
    public $bank;

    public function rules()
    {
        return array(
            array('bank','required'),
            array('bank','safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'bank'      => "Kies uw bank",
        );
    }
}