<?php
  /**
   * Created by CentrioSoft.
   * User: Jean van den Bogaard
   * Date: 09-11-12
   * Time: 10:17
   */

  class Hema extends CActiveRecord
  {
    public static function model($className=__CLASS__)
    {
      return parent::model($className);
    }

    public function tableName()
    {
      return '{{hema}}';
    }

    public function rules()
    {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
        array( 'code', 'required' ),
        array( 'id, code, used', 'safe' ),
      );
    }
  }
