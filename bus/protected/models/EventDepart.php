<?php
  class EventDepart extends CActiveRecord
  {
    public static function model( $className = __CLASS__ )
    {
      return parent::model( $className );
    }

    public function tableName()
    {
      return '{{event_depart}}';
    }

    public function relations()
    {
      return array(
        'event'    => array( self::BELONGS_TO, 'Event', 'event_id' ),
        'city'     => array( self::BELONGS_TO, 'City', 'city_id' ),
        'bookings' => array( self::HAS_MANY, 'Booking', 'event_depart_id' ),
      );
    }
  }