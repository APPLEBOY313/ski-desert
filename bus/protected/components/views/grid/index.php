<?php
$form=$this->beginWidget('CActiveForm', array(
    'enableAjaxValidation'=>true,
));
?>
<div class="grid740">
    <?php $this->displayButtons(); ?>
</div>
<div class="grid740">
    <?php $this->widget('bootstrap.widgets.TbAlert'); ?>
    <div id="example_wrapper" class="dataTables_wrapper">
        <?php $this->displayGrid(); ?>
    </div>
    <?php $this->displayActions(); ?>
</div>
<?php $this->endWidget(); ?>