<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">
    <?php
    $display = 10;
    if (isset(Yii::app()->session['grid_display_items_'.Yii::app()->controller->id]))
    {
        $display = Yii::app()->session['grid_display_items_'.Yii::app()->controller->id];
    }
    ?>
    <div id="example_length" class="dataTables_length">
        <label><?php echo Yii::t('gridcontroller','show'); ?>
            <select name="grid_display_items" size="1" id="grid_display_items">
                <option <?php if ($display == 10) { echo 'selected="selected" ';} ?>value="10">10</option>
                <option <?php if ($display == 25) { echo 'selected="selected" ';} ?>value="25">25</option>
                <option <?php if ($display == 50) { echo 'selected="selected" ';} ?>value="50">50</option>
                <option <?php if ($display == 100) { echo 'selected="selected" ';} ?>value="100">100</option>
            </select> <?php echo Yii::t('gridcontroller','items'); ?></label>
    </div>
    <?php if(count($this->getSearchColumns())>0): ?>
    <div id="example_filter" class="dataTables_filter">
        <?php if (isset(Yii::app()->session['searchText_'.Yii::app()->controller->id])) { $searchText = Yii::app()->session['searchText_'.Yii::app()->controller->id]; } else { $searchText = ''; } ?>
        <label><?php echo Yii::t('gridcontroller','search'); ?>: <input type="text" name="grid_search_text" id="grid_search_text" value="<?php echo $searchText; ?>""></label>
    </div>
    <?php endif; ?>
    <div style="clear:both"></div>
</div>
<?php

// script for display items
$script = "";
$script .= "$('body').on('change','#grid_display_items',function(){";
$script .= "jQuery.ajax({'success':reloadGrid,'type':'POST','url':'".Yii::app()->request->requestUri."/change_items','cache':false,'data':jQuery(this).parents(\"form\").serialize()});return false;";
$script .= "});\r\n";

// script for search field
if (($this->getSearchColumns())>0)
{
    $script .= "var timeId = null;";
    $script .= "$('#grid_search_text').bind('keypress',function(e){";
    $script .= "var code = (e.keyCode ? e.keyCode : e.which);";
    $script .= "if(code == 13) {";
    $script .= "return false;";
    $script .= "}";
    $script .= "});";
    $script .= "$('#grid_search_text').bind('keyup',function(e){";
    $script .= "clearTimeout(timeId);";
    $script .= "timeId = setTimeout(function(){";
    $script .= "jQuery.ajax({'success':reloadGrid,'type':'POST','url':'".Yii::app()->request->requestUri."/search','cache':false,'data':$('#grid_search_text').parents(\"form\").serialize()});";
    $script .= "},500);";
    $script .= "});\r\n";
}

Yii::app()->clientScript->registerScript('GH_SCRIPT',$script);

?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>$this->getGridId(),
    'dataProvider'=>$this->getGridData(),
    'enableSorting'=>true,
    'enablePagination'=>true,
    'ajaxUpdate'=>true,
    'selectableRows'=>2,
    'pager'=>array(
        'header'         => '',
        'firstPageLabel' => '&lt;&lt;',
        'prevPageLabel'  => 'Vorige',
        'nextPageLabel'  => 'Volgende',
        'lastPageLabel'  => '&gt;&gt;',
    ),
    'summaryText'=>'{start}-{end} van {count} items',
    // 'template'=>'{items}<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix ui-border-top"><div style="float:left;">{summary}</div><div style="float:right;">{pager}</div><div style="clear:both;"></div></div>',
    //'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/style.css',
    'columns'=> $columns,
    'itemsCssClass'=>'table table-striped',
));
?>