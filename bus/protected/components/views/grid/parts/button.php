<a class="icon-button" href="<?php echo $url; ?>">
    <img width="18" height="18" alt="icon" src="<?php echo $icon; ?>">
    <span><?php echo $title; ?></span>
</a>