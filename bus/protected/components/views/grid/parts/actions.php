<div class="dataTables_footer">
    <img width="18" height="18" alt="icon" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/icons/button/corner.png" class="arrow">
    <select name="select_action" id="grid_action_select">
        <option value="-1" selected="selected"><?php echo Yii::t('gridcontroller','option_message'); ?></option>
        <?php foreach($actions as $action): ?>
        <option value="<?php echo $action['code']; ?>"><?php echo $action['title']; ?></option>
        <?php endforeach; ?>
    </select>
    <select name="select_option" id="grid_action_select_options" style="display:none;">
        <option value="1">Dit is een test</option>
    </select>
    <a class="icon-button" href="#" id="grid_action_button">
        <?php echo Yii::t('gridcontroller','action_button'); ?>
    </a>
</div>
<?php

// on button click event
$script = "";
$script .= 'function reloadGrid(data) {$.fn.yiiGridView.update("'.$this->getGridId().'");$("#grid_action_select").val("-1");$("#grid_action_select_options").css("display","none");}';
$script .= "$('body').on('click','#grid_action_button',function(){";
$script .= "if ($('#grid_action_select').val() != -1) {";
// TODO extra controlle inbouwen om te kijken of er iets geselecteerd is.
$script .= "switch($('#grid_action_select').val())";
$script .= "{";
foreach($actions as $action){
    $script .= "case '".$action['code']."':";
    if ($action['warning'])
    {
        $script .= "if (confirm('".$action['warning']."'))";
        $script .= "{";
    }
    $script .= "jQuery.ajax({'success':reloadGrid,'type':'POST','url':'/backend/settings/test/ajaxupdate','cache':false,'data':jQuery(this).parents(\"form\").serialize()});return false;";
    if ($action['warning'])
    {
        $script .= "}";
    }
    $script .= "break;";
}
$script .= "}";
$script .= "$('#grid_action_select').val('-1');";
$script .= "$('#grid_action_select_options').css('display','none');";
$script .= "}";
$script .= "});\r\n";

// onchange event
$script .= "$('body').on('change','#grid_action_select',function(){";
// TODO extra controlle inbouwen om te kijken of er iets geselecteerd is.
$script .= "switch($('#grid_action_select').val())";
$script .= "{";
foreach($actions as $action){
    $script .= "case '".$action['code']."':";
    if (count($action['options'])>0)
    {
        $script .= "$('#grid_action_select_options').css('display','inline');";
        $script .= "$('#grid_action_select_options').find('option').remove();";
        foreach($action['options'] as $k => $v)
        {
            $script .= "$('#grid_action_select_options').append('<option selected=\"".$k."\" value=\"whatever\">".$v."</option>');";
        }
        $script .= "$('#grid_action_select_options').find('option:first').attr('selected','selected');";
    }
    else
    {
        $script .= "$('#grid_action_select_options').css('display','none');";
    }
    $script .= "break;";
}
$script .= "}});";

Yii::app()->clientScript->registerScript('GAB_SCRIPT',$script);
?>