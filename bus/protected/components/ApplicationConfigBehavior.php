<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 08-11-12
 * Time: 16:12
 */

class ApplicationConfigBehavior extends CBehavior
{

    public function events()
    {
        return array_merge(parent::events(),array(
            'onBeginRequest'=>'beginRequest',
        ));
    }


    public function beginRequest()
    {
        $uri = explode('/', $_SERVER['REQUEST_URI']);

        if ($uri[1] == 'admin')
            Yii::app()->user->loginUrl = '/admin/auth/login';

    }
}