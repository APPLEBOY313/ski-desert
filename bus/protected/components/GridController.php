<?php
/**
 * Created by CentrioSoft.
 * User: Jean van den Bogaard
 * Date: 13-11-12
 * Time: 01:14
 */

class GridController extends BackendController
{
    private $_buttons = array();
    private $_actions = array();
    private $_columns = array();
    private $_searchColumns = array();
    private $_model = '';
    private $_gridid = 'default_grid';
    private $_displayItems = 10;

    protected function setModel($model)
    {
        if (is_string($model))
        {
            $model = new $model;
        }
        if ($model instanceof CActiveRecord)
        {
            $this->_model = $model;
        }
    }

    protected function loadModel($id = -1)
    {
        $model = null;
        if ($id != -1)
        {
            $model = $this->_model->findByPk($id);
        }
        if (!$model)
        {
            $model = new $this->_model;
        }
        return $model;
    }

    protected function addButton($title, $url = '#', $icon = '/')
    {
        $this->_buttons[] = array(
            'title'=>$title,
            'url'=>$url,
            'icon'=>Yii::app()->theme->baseUrl.$icon
        );
    }

    protected function displayButtons()
    {
        foreach($this->_buttons as $button)
        {
            echo $this->renderPartial('application.components.views.grid.parts.button',array("title"=>$button['title'],"url"=>$button['url'],"icon"=>$button['icon']));

        }
    }

    protected function addAction($title, $code, $warning = '', $options = array())
    {
        $this->_actions[] = array(
            'title'=>$title,
            'code'=>$code,
            'warning'=>$warning,
            'options'=>$options
        );
    }

    protected function displayActions()
    {
        if (count($this->_actions)>0)
        {
            echo $this->renderPartial('application.components.views.grid.parts.actions',array('actions'=>$this->_actions));
        }
    }

    protected function setGridId($value)
    {
        $this->_gridid = $value;
    }

    protected function getGridId()
    {
        return $this->_gridid;
    }

    protected function addColumn($data)
    {
        $this->_columns[] = $data;
    }

    protected function addSearchColumn($value)
    {
        $this->_searchColumns[] = $value;
    }

    protected function getSearchColumns()
    {
        return $this->_searchColumns;
    }

    protected function displayGrid()
    {
        $columns = array(
            array('id'=>'autoId','class'=>'CCheckBoxColumn','selectableRows' => '100','htmlOptions'=>array('style'=>'width:15px;')),
        );
        $columns = CMap::mergeArray($columns, $this->_columns);
        $columns[] = array(
            'class'=>'CButtonColumn',
            'template'=>'{update} {delete}',
            'updateButtonImageUrl'=>Yii::app()->theme->baseUrl.'/img/icons/sidemenu/file_edit.png',
            'updateButtonLabel'=>'Bewerken',
            'deleteButtonImageUrl'=>Yii::app()->theme->baseUrl.'/img/icons/sidemenu/file_delete.png',
            'deleteButtonLabel'=>'Verwijderen',
            'deleteConfirmation'=>'Weet u zeker dat u dit item wilt verwijderen?',
            'htmlOptions'=>array('style'=>'width:40px;'),
        );
        echo $this->renderPartial('application.components.views.grid.parts.grid',array('columns'=>$columns));
    }

    public function getCriteria()
    {
        $criteria = new CDbCriteria();
        if (isset(Yii::app()->session['searchText_'.Yii::app()->controller->id]))
        {
            foreach($this->_searchColumns as $item)
            {
                $criteria->addSearchCondition($item, Yii::app()->session['searchText_'.Yii::app()->controller->id],true,'OR');
            }
        }
        return $criteria;
    }

    public function getGridData()
    {
        $displayItems = $this->_displayItems;
        if (isset(Yii::app()->session['grid_display_items_'.Yii::app()->controller->id]))
        {
            $displayItems = Yii::app()->session['grid_display_items_'.Yii::app()->controller->id];
        }
        $dataProvider = new CActiveDataProvider($this->_model, array(
            'criteria'=>$this->getCriteria(),
            'pagination'=>array('pageSize'=>$displayItems),
        ));

        return $dataProvider;
    }

    public function doAction($model, $action)
    {
        if ($action == 'delete')
        {
            $model->delete();
        }
    }


    // default actions
    public function actionIndex()
    {
        $this->render('application.components.views.grid.index',array(
            'model'=>$this->_model,
        ));
    }

    // Ajax events
    public function actionChange_items()
    {
        if (isset($_POST['grid_display_items']))
        {
            Yii::app()->session['grid_display_items_'.Yii::app()->controller->id] = (int)$_POST['grid_display_items'];
        }
    }

    public function actionSearch()
    {
        if (isset($_POST['grid_search_text']))
        {
            Yii::app()->session['searchText_'.Yii::app()->controller->id] = $_POST['grid_search_text'];
        }
    }

    public function actionDelete($id)
    {
        $model = $this->_model->findByPk($id);
        if ($model)
        {
            $this->doAction($model,'delete');
        }
    }

    public function actionAjaxupdate()
    {
        $autoIdAll = $_POST['autoId'];
        if(count($autoIdAll)>0)
        {
            foreach($autoIdAll as $autoId)
            {
                $model = $this->_model->findByPk($autoId);
                if ($model)
                {
                    $this->doAction($model,$_POST['select_action']);
                }
            }
        }
    }


}