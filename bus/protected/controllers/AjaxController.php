<?php
  class AjaxController extends CController
  {
    public function actionIndex()
    {
      echo 'No access';
      exit;
    }

    public function actionCheckVoucher( )
    {
      // Get the specified voucher
      $voucher ="";
      if( isset( $_POST['voucher'] )) $voucher =strip_tags( $_POST['voucher' ] );

      // Valid input ?
      if( $voucher == '' ) die( json_encode( array( 'status' => 'failed' )));

      $args =array(
        'voucherNumber' => $voucher
      );

      // cURL stuff
      $curl =curl_init( 'https://www.emesa-auctions.com/partner-api/voucher/isvalid' );

      curl_setopt_array( $curl, array(
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_CONNECTTIMEOUT => 8,
        CURLOPT_TIMEOUT        => 60,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPAUTH       => CURLAUTH_DIGEST,
        CURLOPT_POST           => 1,
        CURLOPT_HTTPHEADER     => array( 'Content-Type: application/json' ),
        CURLOPT_POSTFIELDS     => json_encode( $args ),
        CURLOPT_USERPWD        => 'WinterBergBus:hgtf43Fd'
      ));

      // Do the request
      $dummy =curl_exec( $curl );
      if( $dummy === false ) echo 'Curl error: ' . curl_error( $curl );

      // Close curl
      curl_close( $curl );

      // Decode the response
      $response =json_decode( $dummy );

      // Return true or false depending if it is valid or not
      if( isset( $response->OK )) die( json_encode( array( 'status' => 'success' )));

      // Error
      die( json_encode( array( 'status' => 'failed' )));
    }

    public function actionCheckHema( )
    {
      $json =array( 'status' => 'failed' );
      if( isset( Yii::app()->session['people'] )) $people =Yii::app()->session['people'];
      if( isset( Yii::app()->session['persons'] )) $people =Yii::app()->session['persons'];

      // Pre-process array
      $raw =$_POST['codes'];
      $raw =preg_replace( '/[^ \w]+/', ' ', $raw );
      $raw =preg_replace( '/\s+/', ' ', $raw );

      // Create an array
      $codes =explode( ' ', $raw );
      $codes =array_unique( $codes );
      $codes =array_filter( $codes );

      // Do we have the right number of codes ?
      if( count( $codes ) == $people )
      {
        // Check if all the used codes present and not used
        foreach( $codes as $code )
        {
          $dummy =Hema::model()->findByAttributes( array( 'code' => $code, 'used' => 0 ));
          if( $dummy == NULL )
          {
            $json['error']  ='Niet alle codes zijn correct';
            $json['status'] ='failed';
            die( json_encode( $json ));
            break;
          }

          $json['status'] ='success';
        }
      }
      else
      {
        $json['error'] ='Codes komen niet overeen met het aantal opgegeven personen';
      }

      // Return data
      die( json_encode( $json ));
    }

    public function actionCheckgroupon( )
    {
      $json =array( 'status' => 'failed' );

      $codes  =$_POST['codes'];
      $result =array();
      if( count( $codes ) > 0 )
      {
        $free =0;
        foreach( $codes as $index => $code )
        {
          // Is the specified value double ?
          $dummy =array_count_values( $codes );
          if( $dummy[$code] > 1 )
          {
            $result[] ='error';
            continue;
          }

          // Find the supplied code
          $dummy =Groupon::model()->findByAttributes( array( 'code' => $code, 'used' => 0 ));

          // Check if the code has been found
          if( $dummy == NULL )
          {
            $result[] ='error';
            continue;
          }

          // Rental included ?
          if( $dummy->rental ) $free++;

          // If we end up here then the supplied code is ok
          $result[] ='ok';
        }

        // Did we get an error ?
        if( !in_array( 'error', $result ))
        {
          Yii::app()->session['rental'] =$free;
          $json['status'] ='success';
        }
        else
        {
          $json['error']  ='Niet alle codes zijn correct';
          Yii::app()->session['rental'] =0;
          $json['status'] ='failed';
        }
      }

      // Add results
      $json['checks'] =$result;

      // Return data
      die( json_encode( $json ));
    }

    public function actionCityList( $type='depart', $filter='' )
    {
      $json =array(
        'status' => 'success'
      );

      $criteria =new CDbCriteria( );
      $criteria->order ='name ASC';

      if( $type == 'depart' )
      {
        if( $filter == 'nl' ) $criteria->condition = "name NOT LIKE '%winterberg%'";
        if( $filter == 'de' ) $criteria->condition ="name LIKE '%winterberg%'";
      }
      else
      {
        if( $filter == 'nl' ) $criteria->condition ="name NOT LIKE '%winterberg%' AND name NOT LIKE '%Assen%' AND name NOT LIKE '%Zwolle%' AND name NOT LIKE '%Deventer%' AND name NOT LIKE '%Enschede%' AND name NOT LIKE '%Groningen%' AND name NOT LIKE '%Hoogeveen%'";
        if( $filter == 'de' ) $criteria->condition ="name LIKE '%winterberg%'";
      }

      $cities =City::model()->findAll( $criteria );

      $total =count( $cities );
      if( $total > 0 )
      {
        $json['data'] ='';
        foreach( $cities as $city )
        {
          $json['data'] .='<option value="' . $city->id . '">' . $city->name . '</option>';
        }
      }

      die( json_encode( $json ));
    }
  }
