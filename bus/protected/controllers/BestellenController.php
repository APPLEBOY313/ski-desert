<?php
  /**
   * Created by CentrioSoft
   * User: Jean van den Bogaard
   * Date: 08-11-12
   * Time: 15:14
   */

  class BestellenController extends CController
  {
    public function actionIndex()
    {
      // Set layout
      $this->layout ='step1';

      // Render the index
      $this->render( 'index' );
    }

    public function actionStap2( )
    {
      // Set layout
      $this->layout ='step2';

      // Are we allowed to be here ?
      if( !isset( $_POST['Step1'] ) && !isset( $_POST['Step2'] ))
      {
        // Redirect to step 1
        $this->redirect( '/bestellen/index' );
        exit;
      }

      // Store information from Step 1 or redirect to step 1 if no data is available
      if( isset( $_POST['Step1'] ) && !empty( $_POST['Step1'] ))
      {
        // Set new information
        Yii::app()->session['type']        =strip_tags( $_POST['Step1']['type'] );
        Yii::app()->session['event']       =(int)strip_tags( $_POST['Step1']['depart'] );
        Yii::app()->session['people']      =(int)strip_tags( $_POST['Step1']['people'] );
        Yii::app()->session['departure']   =(int)strip_tags( $_POST['Step1']['departure'] );
        Yii::app()->session['destination'] =(int)strip_tags( $_POST['Step1']['destination'] );

        if( !isset( $_POST['Step1']['duration'] )) Yii::app()->session['duration'] =0;
        else                                       Yii::app()->session['duration'] =(int)strip_tags( $_POST['Step1']['duration'] );
      }

      // Get the form for step 2
      $model =new Step2( );

      // Is there any data that is submitted ?
      if( isset( $_POST['Step2'] ))
      {
        $model->attributes =$_POST['Step2'];
        if( $model->validate() )
        {
          // New booking
          $booking =new Booking;

          // General
          $booking->betaald         =0;
          $booking->event_depart_id =Yii::app()->session['event'];

          // NAW gegevens
          $booking->voornaam   =$model->voornaam;
          $booking->achternaam =$model->achternaam;
          $booking->plaats     =$model->plaats;
          $booking->email      =$model->email;
          $booking->telefoon1  =$model->telefoon1;
          $booking->telefoon2  =$model->telefoon2;

          // Save session data
          $booking->days        =Yii::app()->session['duration'];
          $booking->single      =( Yii::app()->session['type'] == 'oneway' ? true : false );
          $booking->members     =Yii::app()->session['people'];
          $booking->destination =City::model()->findByPk( Yii::app()->session['destination'] )->name;

          // Enquete
          if( !isset( $_POST['radiox'] )) $booking->enquete ='Niet ingevuld';
          else
          {
            // Store extra information
            switch( $_POST['radiox'] )
            {
              case '1' : $booking->enquete ='Zoekmachine'; break;
              case '2' : $booking->enquete ='Social Media'; break;
              case '3' : $booking->enquete ='Reclame / advertentie'; break;
              //case '4' : $booking->enquete ='Vakantieveilingen'; break;
              case '5' : $booking->enquete ='Via vrienden'; break;
              case '7' : $booking->enquete ='Via Groupon:'; break;
              case '6' : $booking->enquete ='Anders, namelijk: ' . $_POST['other']; break;
            }
          }

          // Calculate total price
          $depart =$booking->event;

          // Single or retour ?
          if( $booking->single ) $booking->total =24 * $booking->members;
          else                   $booking->total =$depart->price * $booking->members;

          // Store reis sub totaal
          Yii::app()->session['reis_sub'] =$booking->total;

          // Store the booking
          if( !$booking->save( )) throw new CHttpException( '410', 'Could not store the booking' );
          else
          {
            // Store bookings ID
            Yii::app()->session['booking_id'] =$booking->id;

            // Number of persons
            $max =$booking->members;
            if( $max > 0 )
            {
              for( $i=1;$i<=$max;$i++ )
              {
                $person =new BookingPerson;

                $person->booking_id =$booking->id;
                $person->voornaam   =$_POST['Step2']['voornaam'.$i];
                $person->achternaam =$_POST['Step2']['achternaam'.$i];

                $person->save();
              }
            }

            // Goto step 3
            $this->redirect( '/bestellen/stap3' );
            exit;
          }
        }
      }

      // Render the page
      $this->render( 'step2', array( 'model' => $model ));
    }

    public function actionStap3( )
    {
      // Set layout
      $this->layout ='step3';

      // Get Booking information
      $booking =Booking::model()->findByPk( Yii::app()->session['booking_id'] );

      // Check give input
      if( isset( $_POST['Step3'] ) && !empty( $_POST['Step3'] ))
      {
        $method =strip_tags( $_POST['Step3']['method'] );

        // Store the selected payment and add the supplied code to the session
        switch( $method )
        {
          case 'hema'              : Yii::app()->session['coupon-code'] =$_POST['Step3']['hema-code']; break;
          case 'groupon'           : Yii::app()->session['coupon-code'] =$_POST['Step3']['groupon-code']; break;
          case 'vakantieveilingen' : Yii::app()->session['coupon-code'] =$_POST['Step3']['vv-code']; break;
        }

        // Store the method used to pay
        Yii::app()->session['coupon-type'] =$method;

        // Redirect to the nest step
        $this->redirect('/bestellen/stap4');
        exit;
      }

      // Render the page
      $this->render( 'step3' );
    }

    public function actionStap4( )
    {
      // Set layout
      $this->layout ='step4';

      // Get information
      $rental  =Yii::app()->session['rental'];
      $booking =Booking::model()->findByPk( Yii::app()->session['booking_id'] );

      // Check give input
      if( isset( $_POST['Step4'] ) && !empty( $_POST['Step4'] ))
      {
        // Initial values
        $order =array(
          'subtotal' => 0,
          'vat'      => 0,
          'total'    => 0,
          'lines'    => array( ),
        );

        // Side by Side
        if( isset( $_POST['Step4']['sidebyside'] ) && $_POST['Step4']['sidebyside'] == 'ja' )
        {
          $price  =3.50;
          $amount =1;

          $order['lines'][] =array(
            'title'  => 'Langs elkaar zitten',
            'amount' => $amount,
            'price'  => $price,
          );

          // Calculate totals
          $order['total'] +=$price * $amount;
        }

        // Ski Helm
        if( isset( $_POST['Step4']['helmets'] ) && $_POST['Step4']['helmets'] != 'geen' && $_POST['Step4']['helmets'] != '' )
        {
          $price  =3.50;
          $amount =intval( strip_tags( $_POST['Step4']['helmets'] ));

          $order['lines'][] =array(
            'title'  => 'Skihelm',
            'amount' => $amount,
            'price'  => $price,
          );

          // Calculate totals
          $order['total'] +=$price * $amount;
        }

        // Set Langlauf latten
        if( isset( $_POST['Step4']['langlauf'] ) && $_POST['Step4']['langlauf'] != 'geen' && $_POST['Step4']['langlauf'] != '' )
        {
          $price  =9.00;
          $title  ='Langlauflatten + schoenen + stokken';
          $amount =intval( strip_tags( $_POST['Step4']['langlauf'] ));

          // More free rentals then number of sets wanted
          if( $rental == 0 )
          {
            $order['lines'][] =array(
              'price'  => $price,
              'title'  => $title,
              'amount' => $amount - $rental,
            );

            // Calculate totals
            $order['total'] +=$price * $amount;
          }
          else if( $rental >= $amount )
          {
            $order['lines'][] =array(
              'price'  => 0,
              'title'  => $title,
              'amount' => $amount,
            );

            // Subtract the number of items
            $rental -=$amount;
          }
          else
          {
            $order['lines'][] =array(
              'price'  => 0,
              'title'  => $title,
              'amount' => $rental,
            );

            $order['lines'][] =array(
              'price'  => $price,
              'title'  => $title,
              'amount' => $amount - $rental,
            );

            // Calculate totals
            $order['total'] +=$price * ( $amount - $rental );

            // Set rental to 0
            $rental =0;
          }
        }

        // Ski's
        if( isset( $_POST['Step4']['skis'] ) && $_POST['Step4']['skis'] != 'geen' && $_POST['Step4']['skis'] != '' )
        {
          $price  =14.50;
          $title  ='Ski\'s + schoenen + stokken';
          $amount =intval( strip_tags( $_POST['Step4']['skis'] ));

          // More free rentals then number of sets wanted
          if( $rental == 0 )
          {
            $order['lines'][] =array(
              'price'  => $price,
              'title'  => $title,
              'amount' => $amount - $rental,
            );

            // Calculate totals
            $order['total'] +=$price * $amount;
          }
          else if( $rental >= $amount )
          {
            $order['lines'][] =array(
              'price'  => 0,
              'title'  => $title,
              'amount' => $amount,
            );

            // Subtract the number of items
            $rental -=$amount;
          }
          else
          {
            $order['lines'][] =array(
              'price'  => 0,
              'title'  => $title,
              'amount' => $rental,
            );

            $order['lines'][] =array(
              'price'  => $price,
              'title'  => $title,
              'amount' => $amount - $rental,
            );

            // Calculate totals
            $order['total'] +=$price * ( $amount - $rental );

            // Set rental to 0
            $rental =0;
          }
        }

        // Snowboards
        if( isset( $_POST['Step4']['snowboards'] ) && $_POST['Step4']['snowboards'] != 'geen' && $_POST['Step4']['snowboards'] != '' )
        {
          $price  =17.50;
          $title  ='Snowboard + schoenen';
          $amount =intval( strip_tags( $_POST['Step4']['snowboards'] ));

          // More free rentals then number of sets wanted
          if( $rental == 0 )
          {
            $order['lines'][] =array(
              'price'  => $price,
              'title'  => $title,
              'amount' => $amount - $rental,
            );

            // Calculate totals
            $order['total'] +=$price * $amount;
          }
          else if( $rental >= $amount )
          {
            $order['lines'][] =array(
              'price'  => 0,
              'title'  => $title,
              'amount' => $amount,
            );

            // Subtract the number of items
            $rental -=$amount;
          }
          else
          {
            $order['lines'][] =array(
              'price'  => 0,
              'title'  => $title,
              'amount' => $rental,
            );

            $order['lines'][] =array(
              'price'  => $price,
              'title'  => $title,
              'amount' => $amount - $rental,
            );

            // Calculate totals
            $order['total'] +=$price * ( $amount - $rental );

            // Set rental to 0
            $rental =0;
          }
        }

        // Slee
        if( isset( $_POST['Step4']['slee'] ) && $_POST['Step4']['slee'] != 'geen' && $_POST['Step4']['slee'] != '' )
        {
          $price  =6.00;
          $amount =intval( strip_tags( $_POST['Step4']['slee'] ));

          $order['lines'][] =array(
            'title'  => 'Slee',
            'amount' => $amount,
            'price'  => $price,
          );

          // Calculate totals
          $order['total'] +=$price * $amount;
        }

        // Wandel tocht
        if( isset( $_POST['Step4']['walking'] ) && $_POST['Step4']['walking'] != 'geen' && $_POST['Step4']['walking'] != '' )
        {
          $price  =7.50;
          $amount =intval( strip_tags( $_POST['Step4']['walking'] ));

          $order['lines'][] =array(
            'title'  => 'Winter wandel tocht',
            'amount' => $amount,
            'price'  => $price,
          );

          // Calculate totals
          $order['total'] +=$price * $amount;
        }

        // Duitse lunch
        if( isset( $_POST['Step4']['food'] ) && $_POST['Step4']['food'] != 'geen' && $_POST['Step4']['food'] != '' )
        {
          $price  =4.5;
          $amount =intval( strip_tags( $_POST['Step4']['food'] ));

          $order['lines'][] =array(
            'title'  => 'Duitse lunch',
            'amount' => $amount,
            'price'  => $price,
          );

          // Calculate totals
          $order['total'] +=$price * $amount;
        }

        // Store info in the database
        $booking->order =json_encode( $order );
        $booking->save();

        // Redirect to the nest step
        $this->redirect('/bestellen/stap5');
        exit;
      }

      // Render the page
      $this->render( 'step4' );
    }

    public function actionStap5( )
    {
      // Set layout
      $this->layout ='step5';

      // Get information
      $booking =Booking::model()->findByPk( Yii::app()->session['booking_id'] );
      $booking->betaald_via =Yii::app()->session['coupon-type'];

      // If there is no order then redirect to the first step
      $order =json_decode( $booking->order );

      // Get all needed information
      $admin       =Setting::getValue( 'booking_cost', '4.5' );
      $departure   =City::model()->findByPk( Yii::app()->session['departure'] );
      $passengers  =Yii::app()->session['people'];
      $destination =City::model()->findByPk( Yii::app()->session['destination'] );

      if( isset( $_POST['Step5'] ) && !empty( $_POST['Step5'] ))
      {
        $payment ='';
        if( isset( $_POST['Step5']['payment'] )) $payment =strip_tags( $_POST['Step5']['payment'] );

        //
        switch( Yii::app()->session['coupon-type'] )
        {
          // Hema payment
          case 'hema' : // Pre-process array
                        $raw =Yii::app()->session['coupon-code'];
                        $raw =preg_replace( '/[^ \w]+/', ' ', $raw );
                        $raw =preg_replace( '/\s+/', ' ', $raw );

                        // Create an array
                        $codes =explode( ' ', $raw );
                        $codes =array_unique( $codes );
                        $codes =array_filter( $codes );

                        if( count( $codes ) != $passengers ) throw new CHttpException( '400', 'One or more of the supplied codes is not valid' );

                        // Updates the codes
                        foreach( $codes as $code )
                        {
                          $dummy =Hema::model()->findByAttributes( array( 'code' => $code, 'used' => 0 ));

                          if( $dummy == NULL ) throw new CHttpException( '400', 'One or more of the supplied codes is not valid' );

                          $dummy->used =1;
                          $dummy->save();
                        }

                        // Set payment method
                        $booking->voucher =$raw;
                        $booking->save( );
                        break;

          // Groupon payment
          case 'groupon' : $codes =Yii::app()->session['coupon-code'];
                           $control =Yii::app()->session['coupon-code'];

                           $codes   =$codes['code'];
                           $control =$control['control'];
                           if( count( $codes ) <= 0 ) throw new CHttpException( '400', 'The number of supplied codes do not match the number of passengers' );

                           // Validate the given codes
                           foreach( $codes as $index => $code )
                           {
                             // Is the specified value double ?
                             $dummy =array_count_values( $codes );
                             if( $dummy[$code] > 1 ) throw new CHttpException( '400', 'Some codes are used more then once' );

                             // Find the supplied code
                             $dummy =Groupon::model()->findByAttributes( array( 'code' => $code, 'used' => 0 ));

                             // Check if the code has been found
                             if( $dummy == NULL ) throw new CHttpException( '400', 'One or more coupon codes have already been used' );
                           }

                           // Set the used codes to used
                           foreach( $codes as $index => $code )
                           {
                             $dummy =Groupon::model()->findByAttributes( array( 'code' => $code, 'used' => 0 ));

                             if( $dummy == NULL ) throw new CHttpException( '400', 'One or more coupon codes have already been used' );

                             $dummy->used =1;
                             $dummy->save();
                           }

                           // Set payment method
                           $dummy ='';
                           for( $i=0;$i<count($codes);$i++ ) $dummy .='code: ' . $codes[$i] . ' controle: ' . $control[$i] . '<br />';

                           $booking->voucher =$dummy;
                           $booking->save( );
                           break;

          // Vakantie Veilingen
          case 'vakantieveilingen' : // Try to cash the voucher
                                     $coupon =Yii::app()->session['coupon-code'];
                                     if( !$this->cashIn( $coupon )) throw new CHttpException( '400', 'Voucher is not valid' );

                                     // Set payment method
                                     $booking->voucher =$coupon;
                                     $booking->save( );
                                     break;
        }

        // No further payments needed ?
        if( $order->total == 0 )
        {
          $booking->betaald =1;
          $booking->save( );

          // send mails
          $this->sendMail( $booking->id );

          // Redirect to thanks page
//          $this->redirect( 'http://www.winterbergbus.nl/l_boeking_wordt_verwerkt.html' );
          $this->redirect( 'http://www.winterbergbus.nl/l_end.php' );
          exit;
        }

        // If we end up here then we still have some payment to settle
        switch( $payment )
        {
          case 'ideal'  : $this->redirect( '/bestellen/step5_ideal' ); exit; break;
          case 'paypal' : $this->redirect( '/bestellen/step5_paypal' ); exit; break;
          default       : throw new CHttpException( '400', 'No payment was selected' ); break;
        }
      }

      // Render the page
      $this->render( 'step5', array(
        'admin'       => $admin,
        'order'       => $order,
        'booking'     => $booking,
        'departure'   => $departure,
        'passengers'  => $passengers,
        'destination' => $destination,
      ));
    }

    public function actionStep5_ideal()
    {
      $this->layout ='step5';

      $arg = array();
      $model = new SisowForm();
      if (isset($_POST['SisowForm']))
      {
        $model->attributes = $_POST['SisowForm'];

        // booking ophalen...
        $id = Yii::app()->session['booking_id'];

        $booking =Booking::model()->findByPk( $id );
        if( $booking )
        {
          $arg['ipaddress']            =$_SERVER['REMOTE_ADDR'];
          $arg['shipping_firstname']   =$booking->voornaam;
          $arg['shipping_lastname']    =$booking->achternaam;
          $arg['shipping_mail']        =$booking->email;
          $arg['shipping_address1']    =$booking->straat;
          $arg['shipping_address2']    ='';
          $arg['shipping_zip']         =$booking->postcode;
          $arg['shipping_city']        =$booking->plaats;
          $arg['shipping_countrycode'] ='NL';
          $arg['shipping_phone']       ='';
          $arg['shipping']             =0;

          $arg['billing_firstname']   =$booking->voornaam;
          $arg['billing_lastname']    =$booking->achternaam;
          $arg['billing_mail']        =$booking->email;
          $arg['billing_address1']    =$booking->straat;
          $arg['billing_address2']    ='';
          $arg['billing_zip']         =$booking->postcode;
          $arg['billing_city']        =$booking->plaats;
          $arg['billing_countrycode'] ='NL';
          $arg['billing_phone']       ='';

          $arg['currency'] ='EUR';
          $arg['testmode'] =false;

          $sisow =new Sisow( "2537508393", "680f88ea61e2c865374b7b41c69f6d721ed0ec28" );

          // General information
          $order =json_decode( $booking->order );

          // Used a voucher ?
          $admin =Setting::getValue( 'booking_cost', '4.5' );
          $dummy =array( 'hema', 'groupon', 'vakantieveilingen' );

          if( !in_array( $booking->betaald_via, $dummy )) $sisow->amount =$booking->total + $order->total + $admin;
          else                                            $sisow->amount =$order->total + $admin;

          $sisow->payment     ='iDeal';
          $sisow->issuerId    =$model->bank;
          $sisow->purchaseId  =$booking->id;
          $sisow->description ='Boeking winterbergbus';

          // Url information
          $sisow->notifyUrl =$this->createAbsoluteUrl( '/default/notify/id/' . $booking->id );
//          $sisow->returnUrl ='http://www.winterbergbus.nl/l_boeking_wordt_verwerkt.html';
          $sisow->returnUrl ='http://www.winterbergbus.nl/l_end.php';

          if(( $ex = $sisow->transactionRequest( $arg )) < 0 )
          {
            throw new CHttpException( '400', 'Betaling is op dit moment niet mogelijk' );
          }
          else
          {
            // Update booking
            $dummy =$booking->betaald_via;
            if( $dummy == 'hema' || $dummy == 'groupon' || $dummy == 'vakantieveilingen' ) $dummy =$dummy . ' + iDeal';
            else                                                                           $dummy ='iDeal';

            $booking->betaald     =0;
            $booking->trans_id    =$sisow->trxId;
            $booking->betaald_via =$dummy;
            $booking->save( );

            // Redirect to Sisow
            $this->redirect( $sisow->issuerUrl );
          }
        }
        else
        {
          throw new CHttpException('400','Invalid request');
        }
      }

      $this->render( 'step5_ideal', array( 'model' => $model ));
    }

    public function actionStep5_paypal()
    {
      $this->layout ='step5';

      $arg = array();

      // booking ophalen...
      $id = Yii::app()->session['booking_id'];

      $booking =Booking::model()->findByPk( $id );
      if( $booking )
      {
        $arg['ipaddress']            =$_SERVER['REMOTE_ADDR'];
        $arg['shipping_firstname']   =$booking->voornaam;
        $arg['shipping_lastname']    =$booking->achternaam;
        $arg['shipping_mail']        =$booking->email;
        $arg['shipping_address1']    =$booking->straat;
        $arg['shipping_address2']    ='';
        $arg['shipping_zip']         =$booking->postcode;
        $arg['shipping_city']        =$booking->plaats;
        $arg['shipping_countrycode'] ='NL';
        $arg['shipping_phone']       ='';
        $arg['shipping']             =0;

        $arg['billing_firstname']   =$booking->voornaam;
        $arg['billing_lastname']    =$booking->achternaam;
        $arg['billing_mail']        =$booking->email;
        $arg['billing_address1']    =$booking->straat;
        $arg['billing_address2']    ='';
        $arg['billing_zip']         =$booking->postcode;
        $arg['billing_city']        =$booking->plaats;
        $arg['billing_countrycode'] ='NL';
        $arg['billing_phone']       ='';

        $arg['currency'] ='EUR';
        $arg['testmode'] =false;

        $sisow =new Sisow( "2537508393", "680f88ea61e2c865374b7b41c69f6d721ed0ec28" );

        // General information
        $order =json_decode( $booking->order );

        // Used a voucher ?
        $admin =Setting::getValue( 'booking_cost', '4.5' );
        $dummy =array( 'hema', 'groupon', 'vakantieveilingen' );

        if( !in_array( $booking->betaald_via, $dummy )) $sisow->amount =$booking->total + $order->total + $admin;
        else                                            $sisow->amount =$order->total + $admin;

        $sisow->payment     ='paypalec';
        $sisow->purchaseId  =$booking->id;
        $sisow->description ='Boeking winterbergbus';

        // Url information
        $sisow->notifyUrl =$this->createAbsoluteUrl( '/default/notify/id/' . $booking->id );
//        $sisow->returnUrl ='http://www.winterbergbus.nl/l_boeking_wordt_verwerkt.html';
        $sisow->returnUrl ='http://www.winterbergbus.nl/l_end.php';

        if(($ex = $sisow->transactionRequest($arg)) < 0)
        {
          throw new CHttpException('400','Betaling is op dit moment niet mogelijk');
        }
        else
        {
          // Update booking
          $dummy =$booking->betaald_via;
          if( $dummy == 'hema' || $dummy == 'groupon' || $dummy == 'vakantieveilingen' ) $dummy =$dummy . ' + PayPal';
          else                                                                           $dummy ='PayPal';

          $booking->betaald     =0;
          $booking->trans_id    =$sisow->trxId;
          $booking->betaald_via =$dummy;
          $booking->save( );

          // Redirect to Sisow
          $this->redirect( $sisow->issuerUrl );
        }
      }

      throw new CHttpException('400','Invalid request');
    }

    public function actionBedankt()
    {
      $this->render( 'thanks' );
    }

    public function actionNotify( $id )
    {
      if( isset( $id ))
      {
        $model =Booking::model()->findByPk( $id );
        if( $model )
        {
          // Kijken of transactie betaald is.
          $sisow =new Sisow("2537508393", "680f88ea61e2c865374b7b41c69f6d721ed0ec28");
          $response =$sisow->StatusRequest( $model->trans_id );
          if( $response == 0 )
          {
            if( $sisow->status == Sisow::statusSuccess )
            {
              // Set booking to paid.
              $model->betaald =1;
              $model->save();

              // Increase booked (just update it, will be saved automatically
              $model->event->event->update();

              // Send email to customer and administrator
              $this->sendMail( $id );

              // Exit successfully
              die( "Ok" );
            }
            else
            {
              $model->betaald =0;
              $model->save();

              echo $sisow->status;
            }
          }
          else
          {
            die( "Error" );
          }
        }
      }
    }

    public function sendMail($id)
    {
      // Send email to customer
      $message = new YiiMailMessage('Uw boeking bij winterbergbus');
      $message->view = 'bestellen';

      $model =Booking::model()->findByPk($id);

      ($model->members == 1)? $persons_str = 'alleen':$persons_str = 'met '.$model->members.' personen';

      $datum     =date('d-m-Y',strtotime($model->event->event->depart));
      $dagen     =Day::model()->findByPk( $model->days );
      $besteming =$model->destination;

      $depart_str =$model->event->city->name.' - '.$model->event->city->description.' om ' .date('H:i',strtotime($model->event->depart)) . ' uur';

      $message->setBody( array(
        'model'     => $model,
        'datum'     => $datum,
        'dagen'     => ( $dagen == NULL ? '' : $dagen->name ),
        'total'     => number_format( $model->total, 2, '.', ',' ),
        'depart'    => $depart_str,
        'persons'   => $persons_str,
        'besteming' => $besteming,
      ), 'text/html' );

      $message->addTo( $model->email );
      $message->from ='winterbergbus@outlook.com';

      Yii::app()->mail->send($message);

      // Send email to administrator
      $message = new YiiMailMessage('Uw boeking bij winterbergbus');
      $message->view = 'bestellen';

      $model = Booking::model()->findByPk($id);

      ($model->members == 1)? $persons_str = 'alleen':$persons_str = 'met '.$model->members.' personen';

      $datum = date('d-m-Y',strtotime($model->event->event->depart));

      $dagen =Day::model()->findByPk( $model->days );

      $besteming = 'Winterberg';

      $depart_str = $model->event->city->name.' - '.$model->event->city->description.' om ' .date('H:i',strtotime($model->event->depart)) . ' uur';

      $message->setBody( array(
        'model'     => $model,
        'datum'     => $datum,
        'dagen'     => ( $dagen == NULL ? '' : $dagen->name ),
        'total'     => number_format($model->total,2,'.',','),
        'depart'    => $depart_str,
        'persons'   => $persons_str,
        'besteming' => $besteming,
      ), 'text/html' );

      $message->addTo('winterbergbus@outlook.com');
      $message->from ='winterbergbus@outlook.com';

      Yii::app()->mail->send($message);
    }

    public function cashIn( $voucher )
    {
      // Valid input ?
      if( $voucher == '' ) return false;

      $args =array(
        'voucherNumber' => $voucher
      );

      // cURL stuff
      $curl =curl_init( 'https://www.emesa-auctions.com/partner-api/voucher/cashin' );

      curl_setopt_array( $curl, array(
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_CONNECTTIMEOUT => 8,
        CURLOPT_TIMEOUT        => 60,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPAUTH       => CURLAUTH_DIGEST,
        CURLOPT_POST           => 1,
        CURLOPT_HTTPHEADER     => array( 'Content-Type: application/json' ),
        CURLOPT_POSTFIELDS     => json_encode( $args ),
        CURLOPT_USERPWD        => 'WinterBergBus:hgtf43Fd'
      ));

      // Do the request
      $dummy =curl_exec( $curl );
      if( $dummy === false )
      {
        echo 'Curl error: ' . curl_error( $curl );

        return false;
      }

      // Close curl
      curl_close( $curl );

      // Decode the response
      $response =json_decode( $dummy );

      // Return true or false depending if it is valid or not
      if( isset( $response->OK )) return true;

      // Error
      return false;
    }
  }