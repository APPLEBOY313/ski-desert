<?php
  class DataController extends CController
  {
    public function actionIndex()
    {
      echo 'No access';
      exit;
    }

    public function actionDepaturelist( )
    {
      $json =array();

      $criteria =new CDbCriteria( );
      $criteria->order ='name ASC';

      $cities =City::model()->findAll( $criteria );
      foreach( $cities as $city ) $json[$city->id] =$city->name;

      // Return the data
      header('Content-type: application/json; charset=utf-8');
      header("access-control-allow-origin: *");

      $json =json_encode( $json );

      echo isset( $_GET['callback'] ) ? "{$_GET['callback']}($json)" : $json;
      die;
    }

    public function actionDepartInfo( $id )
    {
      $json =array( );

      $city =City::model()->findByPk( $id );
      foreach( $city->departs as $depart )
      {
        $days =array();

        // Skip event departs that do not belong to an event
        if( $depart->event == null ) continue;

        $dummy =unserialize( $depart->event->days );
        foreach( $dummy as $item )
        {
          $day =Day::model()->findByPk( $item );

          $days[] =array(
            'id'   => $day->id,
            'name' => $day->name,
          );
        }

        $json[] =array(
          'id'   => $depart->id,
          'date' => $depart->event->depart,
          'time' => $depart->depart,
          'name' => $depart->event->name,
          'left' => $depart->event->avail - $depart->event->booked,
          'days' => $days,
        );
      }

      header('Content-type: application/json; charset=utf-8');
      header("access-control-allow-origin: *");

      // Encode the data as json object
      $json =json_encode( $json );

      echo isset( $_GET['callback'] ) ? "{$_GET['callback']}($json)" : $json;
      die;
    }
  }
