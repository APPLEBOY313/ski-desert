<?php
  /**
   * Created by CentrioSoft
   * User: Jean van den Bogaard
   * Date: 08-11-12
   * Time: 15:14
   */

  class DefaultController extends CController
  {
    public function actionIndex()
    {
      $this->layout ='winterberg';

      $this->render( 'index' );
    }

    public function actionStep2( )
    {
      // set layout
      $this->layout ='winterberg-step2';

      // Do the remaining
      if( isset( $_POST['step1'] ) && !empty( $_POST['step1'] ))
      {
        Yii::app()->session['type']    =$_POST['step1']['type'];

        Yii::app()->session['city']    =(int)$_POST['step1']['vertrek'];
        Yii::app()->session['dest']    =(int)$_POST['step1']['bestemming'];
        Yii::app()->session['event']   =(int)$_POST['step1']['depart'];
        if( !isset( $_POST['step1']['personen'] )) Yii::app()->session['persons'] =1;
        else                                       Yii::app()->session['persons'] =(int)$_POST['step1']['personen'];

        if( !isset( $_POST['step1']['verblijfsduur'] )) Yii::app()->session['days'] =1;
        else                                            Yii::app()->session['days'] =(int)$_POST['step1']['verblijfsduur'];
      }

      $model =new BookingStep2( );

      if( isset( $_POST['BookingStep2'] ))
      {
        $model->attributes =$_POST['BookingStep2'];

        if( $model->validate( ))
        {
          // New booking
          $booking =new Booking;

          // General
          $booking->betaald         =0;
          $booking->betaald_via     =$model->payment;
          $booking->event_depart_id =Yii::app()->session['event'];

          // NAW gegevens
          $booking->voornaam   =$model->voornaam;
          $booking->achternaam =$model->achternaam;
          $booking->plaats     =$model->plaats;
          $booking->email      =$model->email;
          $booking->telefoon1  =$model->telefoon1;
          $booking->telefoon2  =$model->telefoon2;

          // Save session data
          $booking->days        =Yii::app()->session['days'];
          $booking->single      =( Yii::app()->session['type'] == 'oneway' ? true : false );
          $booking->members     =Yii::app()->session['persons'];
          $booking->destination =City::model()->findByPk( Yii::app()->session['dest'] )->name;

          if( !isset( $_POST['radiox'] )) $booking->enquete ='Niet ingevuld';
          else
          {
            // Store extra information
            switch( $_POST['radiox'] )
            {
              case '1' : $booking->enquete ='Zoekmachine'; break;
              case '2' : $booking->enquete ='Social Media'; break;
              case '3' : $booking->enquete ='Reclame / advertentie'; break;
              case '4' : $booking->enquete ='Vakantieveilingen'; break;
              case '5' : $booking->enquete ='Via vrienden'; break;
              case '6' : $booking->enquete ='Anders, namelijk: ' . $_POST['other']; break;
            }
          }

          // calculate total price
          $depart =$booking->event;

          // Single or retour ?
          if( $booking->single ) $total =24 * $booking->members;
          else                   $total =$depart->price * $booking->members;

          $total  =$total + Setting::getValue( 'booking_cost', '4.5' );
          $booking->total =$total;

          // Store the booking
          if( $booking->save( ))
          {
            Yii::app()->session['booking_id'] =$booking->id;

            // Number of persons
            $max =$booking->members;
            if( $max > 0 )
            {
              for( $i=1;$i<=$max;$i++ )
              {
                $person =new BookingPerson;

                $person->booking_id =$booking->id;
                $person->voornaam   =$_POST['BookingStep2']['voornaam'.$i];
                $person->achternaam =$_POST['BookingStep2']['achternaam'.$i];

                $person->save();
              }
            }

            // Check payment method and redirect to the right link
            switch( $model->payment )
            {
              case 'hema' : Yii::app()->session['codes'] =strip_tags( $_POST['codes'] );
                            $this->redirect('/default/step4_hema');
                            exit;
                            break;

              case 'ideal' : $this->redirect('/default/step4_ideal');
                             exit;
                             break;

              case 'paypal' : $this->redirect('/default/step4_paypal');
                              exit;
                              break;
              case 'mistercash' : $this->redirect('/default/step4_mistercash');
                              exit;
                              break;
              case 'vakantieveilingen' : Yii::app()->session['coupon'] =strip_tags( $_POST['coupon'] );
                                         $this->redirect('/default/step4_veilingen');
                                         exit;
                                         break;
            }
          }
        }
      }

      $this->render( 'step2', array( 'model' => $model ));
    }

    public function actionStep4_ideal()
    {
      $this->layout ='winterberg-step4';

      $arg = array();
      $model = new SisowForm();
      if (isset($_POST['SisowForm']))
      {
        $model->attributes = $_POST['SisowForm'];

        // booking ophalen...
        $id = Yii::app()->session['booking_id'];

        $booking =Booking::model()->findByPk( $id );
        if( $booking )
        {
          $arg['ipaddress']            =$_SERVER['REMOTE_ADDR'];
          $arg['shipping_firstname']   =$booking->voornaam;
          $arg['shipping_lastname']    =$booking->achternaam;
          $arg['shipping_mail']        =$booking->email;
          $arg['shipping_address1']    =$booking->straat;
          $arg['shipping_address2']    ='';
          $arg['shipping_zip']         =$booking->postcode;
          $arg['shipping_city']        =$booking->plaats;
          $arg['shipping_countrycode'] ='NL';
          $arg['shipping_phone']       ='';
          $arg['shipping']             =0;

          $arg['billing_firstname']   =$booking->voornaam;
          $arg['billing_lastname']    =$booking->achternaam;
          $arg['billing_mail']        =$booking->email;
          $arg['billing_address1']    =$booking->straat;
          $arg['billing_address2']    ='';
          $arg['billing_zip']         =$booking->postcode;
          $arg['billing_city']        =$booking->plaats;
          $arg['billing_countrycode'] ='NL';
          $arg['billing_phone']       ='';

          $arg['currency'] ='EUR';
          $arg['testmode'] =false;

          $sisow =new Sisow( "2537508393", "680f88ea61e2c865374b7b41c69f6d721ed0ec28" );

          // General information
          $sisow->amount      =$booking->total;
          $sisow->payment     ='iDeal';
          $sisow->issuerId    =$model->bank;
          $sisow->purchaseId  =$booking->id;
          $sisow->description ='Boeking winterbergbus';

          // Url information
          $sisow->notifyUrl   =$this->createAbsoluteUrl( '/default/notify/id/' . $booking->id );
//          $sisow->returnUrl   ='http://www.winterbergbus.nl/l_boeking_wordt_verwerkt.html';
          $sisow->returnUrl   ='http://www.winterbergbus.nl/l_end.php';

          if(($ex = $sisow->transactionRequest($arg)) < 0)
          {
            throw new CHttpException('400','Betaling is op dit moment niet mogelijk');
          }
          else
          {
            $booking->trans_id = $sisow->trxId;
            $booking->save();
            $this->redirect($sisow->issuerUrl);
          }
        }
        else
        {
          throw new CHttpException('400','Invalid request');
        }
      }

      $this->render('step4_ideal',array('model'=>$model));
    }

    /*
    public function actionStep4_mistercash()
    {
      $this->layout ='winterberg-step4';

      $arg = array();
      $model = new SisowForm();
      if (isset($_POST['SisowForm']))
      {
        $model->attributes = $_POST['SisowForm'];

        // booking ophalen...
        $id = Yii::app()->session['booking_id'];

        $booking =Booking::model()->findByPk( $id );
        if( $booking )
        {
          $arg['ipaddress']            =$_SERVER['REMOTE_ADDR'];
          $arg['shipping_firstname']   =$booking->voornaam;
          $arg['shipping_lastname']    =$booking->achternaam;
          $arg['shipping_mail']        =$booking->email;
          $arg['shipping_address1']    =$booking->straat;
          $arg['shipping_address2']    ='';
          $arg['shipping_zip']         =$booking->postcode;
          $arg['shipping_city']        =$booking->plaats;
          $arg['shipping_countrycode'] ='NL';
          $arg['shipping_phone']       ='';
          $arg['shipping']             =0;

          $arg['billing_firstname']   =$booking->voornaam;
          $arg['billing_lastname']    =$booking->achternaam;
          $arg['billing_mail']        =$booking->email;
          $arg['billing_address1']    =$booking->straat;
          $arg['billing_address2']    ='';
          $arg['billing_zip']         =$booking->postcode;
          $arg['billing_city']        =$booking->plaats;
          $arg['billing_countrycode'] ='NL';
          $arg['billing_phone']       ='';

          $arg['currency'] ='EUR';
          $arg['testmode'] =false;

          $sisow =new Sisow( "2537508393", "680f88ea61e2c865374b7b41c69f6d721ed0ec28" );

          // General information
          $sisow->amount      =$booking->total;
          $sisow->payment     ='mistercash';
          $sisow->issuerId    =$model->bank;
          $sisow->purchaseId  =$booking->id;
          $sisow->description ='Boeking winterbergbus';

          // Url information
          $sisow->notifyUrl   =$this->createAbsoluteUrl( '/default/notify/id/' . $booking->id );
//          $sisow->returnUrl   ='http://www.winterbergbus.nl/l_boeking_wordt_verwerkt.html';
          $sisow->returnUrl   ='http://www.winterbergbus.nl/l_end.php';

          if(($ex = $sisow->transactionRequest($arg)) < 0)
          {
            throw new CHttpException('400','Betaling is op dit moment niet mogelijk');
          }
          else
          {
            $booking->trans_id = $sisow->trxId;
            $booking->save();
            $this->redirect($sisow->issuerUrl);
          }
        }
        else
        {
          throw new CHttpException('400','Invalid request');
        }
      }

      $this->render('step4_mistercash',array('model'=>$model));
    }
*/    

    public function actionStep4_mistercash()
    {
        $this->layout ='winterberg';

        $arg = array();

        // booking ophalen...
        $id = Yii::app()->session['booking_id'];

        $booking =Booking::model()->findByPk( $id );
        if( $booking )
        {
          $arg['ipaddress']            =$_SERVER['REMOTE_ADDR'];
          $arg['shipping_firstname']   =$booking->voornaam;
          $arg['shipping_lastname']    =$booking->achternaam;
          $arg['shipping_mail']        =$booking->email;
          $arg['shipping_address1']    =$booking->straat;
          $arg['shipping_address2']    ='';
          $arg['shipping_zip']         =$booking->postcode;
          $arg['shipping_city']        =$booking->plaats;
          $arg['shipping_countrycode'] ='NL';
          $arg['shipping_phone']       ='';
          $arg['shipping']             =0;

          $arg['billing_firstname']   =$booking->voornaam;
          $arg['billing_lastname']    =$booking->achternaam;
          $arg['billing_mail']        =$booking->email;
          $arg['billing_address1']    =$booking->straat;
          $arg['billing_address2']    ='';
          $arg['billing_zip']         =$booking->postcode;
          $arg['billing_city']        =$booking->plaats;
          $arg['billing_countrycode'] ='NL';
          $arg['billing_phone']       ='';

          $arg['currency'] ='EUR';
          $arg['testmode'] =false;

          $sisow =new Sisow( "2537508393", "680f88ea61e2c865374b7b41c69f6d721ed0ec28" );

          // General information
          $sisow->amount      =$booking->total;
          $sisow->payment     ='mistercash';
          $sisow->purchaseId  =$booking->id;
          $sisow->description ='Boeking winterbergbus';

          // Url information
          $sisow->notifyUrl =$this->createAbsoluteUrl( '/default/notify/id/' . $booking->id );
        //        $sisow->returnUrl ='http://www.winterbergbus.nl/l_boeking_wordt_verwerkt.html';
          $sisow->returnUrl ='http://www.winterbergbus.nl/l_end.php';

          if(($ex = $sisow->transactionRequest($arg)) < 0)
          {
            throw new CHttpException('400','Betaling is op dit moment niet mogelijk');
          }
          else
          {
            $booking->trans_id = $sisow->trxId;
            $booking->save();
            $this->redirect($sisow->issuerUrl);
          }
        }

        throw new CHttpException('400','Invalid request');
    }
    
    
    public function actionStep4_paypal()
    {
      $this->layout ='winterberg';

      $arg = array();

      // booking ophalen...
      $id = Yii::app()->session['booking_id'];

      $booking =Booking::model()->findByPk( $id );
      if( $booking )
      {
        $arg['ipaddress']            =$_SERVER['REMOTE_ADDR'];
        $arg['shipping_firstname']   =$booking->voornaam;
        $arg['shipping_lastname']    =$booking->achternaam;
        $arg['shipping_mail']        =$booking->email;
        $arg['shipping_address1']    =$booking->straat;
        $arg['shipping_address2']    ='';
        $arg['shipping_zip']         =$booking->postcode;
        $arg['shipping_city']        =$booking->plaats;
        $arg['shipping_countrycode'] ='NL';
        $arg['shipping_phone']       ='';
        $arg['shipping']             =0;

        $arg['billing_firstname']   =$booking->voornaam;
        $arg['billing_lastname']    =$booking->achternaam;
        $arg['billing_mail']        =$booking->email;
        $arg['billing_address1']    =$booking->straat;
        $arg['billing_address2']    ='';
        $arg['billing_zip']         =$booking->postcode;
        $arg['billing_city']        =$booking->plaats;
        $arg['billing_countrycode'] ='NL';
        $arg['billing_phone']       ='';

        $arg['currency'] ='EUR';
        $arg['testmode'] =false;

        $sisow =new Sisow( "2537508393", "680f88ea61e2c865374b7b41c69f6d721ed0ec28" );

        // General information
        $sisow->amount      =$booking->total;
        $sisow->payment     ='paypalec';
        $sisow->purchaseId  =$booking->id;
        $sisow->description ='Boeking winterbergbus';

        // Url information
        $sisow->notifyUrl =$this->createAbsoluteUrl( '/default/notify/id/' . $booking->id );
//        $sisow->returnUrl ='http://www.winterbergbus.nl/l_boeking_wordt_verwerkt.html';
        $sisow->returnUrl ='http://www.winterbergbus.nl/l_end.php';

        if(($ex = $sisow->transactionRequest($arg)) < 0)
        {
          throw new CHttpException('400','Betaling is op dit moment niet mogelijk');
        }
        else
        {
          $booking->trans_id = $sisow->trxId;
          $booking->save();
          $this->redirect($sisow->issuerUrl);
        }
      }

      throw new CHttpException('400','Invalid request');
    }

    public function actionStep4_veilingen()
    {
      // booking ophalen...
      $id      =Yii::app()->session['booking_id'];
      $booking =Booking::model()->findByPk( $id );
      if( $booking )
      {
        if( Yii::app()->session['coupon'] != '' )
        {
          // Try to cash the voucher
          $coupon =Yii::app()->session['coupon'];
          if( $this->cashIn( $coupon ))
          {
            // Set payment on yes
            $booking->betaald =1;
            $booking->voucher =$coupon;
            $booking->save( );

            // send mails
            $this->sendMail( $id );

            // Redirect to thanks page
//            $this->redirect( 'http://www.winterbergbus.nl/l_boeking_wordt_verwerkt.html' );
            $this->redirect( 'http://www.winterbergbus.nl/l_end.php' );
          }
        }
      }

      // If we get here throw an exception
      throw new CHttpException( '400', 'Voucher is not valid' );
    }

    public function actionStep4_hema()
    {
      // booking ophalen...
      $id      =Yii::app()->session['booking_id'];
      $booking =Booking::model()->findByPk( $id );
      if( $booking )
      {
        // Pre-process array
        $raw =Yii::app()->session['codes'];
        $raw =preg_replace( '/[^ \w]+/', ' ', $raw );
        $raw =preg_replace( '/\s+/', ' ', $raw );

        // Create an array
        $codes =explode( ' ', $raw );
        $codes =array_unique( $codes );
        $codes =array_filter( $codes );

        if( count( $codes ) == Yii::app()->session['persons'] )
        {
          // Updates the codes
          foreach( $codes as $code )
          {
            $dummy =Hema::model()->findByAttributes( array( 'code' => $code, 'used' => 0 ));

            if( $dummy == NULL ) throw new CHttpException( '400', 'One or more of the supplied codes is not valid' );

            $dummy->used =1;
            $dummy->save();
          }

          // Set payment on yes
          $booking->betaald =1;
          $booking->save( );

          // send mails
          $this->sendMail( $id );

          // Redirect to thanks page
//          $this->redirect( 'http://www.winterbergbus.nl/l_boeking_wordt_verwerkt.html' );
          $this->redirect( 'http://www.winterbergbus.nl/l_end.php' );
        }
      }

      // If we get here throw an exception
      throw new CHttpException( '400', 'One or more of the supplied codes is not valid' );
    }

    public function actionBedankt()
    {
      $this->render( 'step5' );
    }

    public function actionNotify( $id )
    {
      if( isset( $id ))
      {
        $model =Booking::model()->findByPk( $id );
        if( $model )
        {
          // Kijken of transactie betaald is.
          $sisow =new Sisow("2537508393", "680f88ea61e2c865374b7b41c69f6d721ed0ec28");
          $response =$sisow->StatusRequest( $model->trans_id );
          if( $response == 0 )
          {
            if( $sisow->status == Sisow::statusSuccess )
            {
              // Set booking to paid.
              $model->betaald = 1;
              $model->save();

              // Increase booked (just update it, will be saved automatically
              $model->event->event->update();

              // Send email to customer and administrator
              $this->sendMail( $id );

              // Exit successfully
              die( "Ok" );
            }
            else
            {
              $model->betaald =0;
              $model->save();

              echo $sisow->status;
            }
          }
          else
          {
            die( "Error" );
          }
        }
      }
    }

    public function actionMaurice( $id )
    {
      $this->sendMail( $id );
    }

    public function sendMail($id)
    {
      // Send email to customer
      $message = new YiiMailMessage('Uw boeking bij winterbergbus');
      $message->view = 'mail';

      $model =Booking::model()->findByPk($id);

      ($model->members == 1)? $persons_str = 'alleen':$persons_str = 'met '.$model->members.' personen';

      $datum     =date('d-m-Y',strtotime($model->event->event->depart));
      $dagen     =Day::model()->findByPk( $model->days );
      $besteming =$model->destination;

      $depart_str =$model->event->city->name.' - '.$model->event->city->description.' om ' .date('H:i',strtotime($model->event->depart)) . ' uur';

      $message->setBody( array(
        'model'     => $model,
        'datum'     => $datum,
        'dagen'     => ( $dagen == NULL ? '' : $dagen->name ),
        'total'     => number_format( $model->total, 2, '.', ',' ),
        'depart'    => $depart_str,
        'persons'   => $persons_str,
        'besteming' => $besteming,
      ), 'text/html' );

      $message->addTo( $model->email );
      $message->from ='winterbergbus@outlook.com';

      Yii::app()->mail->send($message);

      // Send email to administrator
      $message = new YiiMailMessage('Uw boeking bij winterbergbus');
      $message->view = 'mail';

      $model = Booking::model()->findByPk($id);

      ($model->members == 1)? $persons_str = 'alleen':$persons_str = 'met '.$model->members.' personen';

      $datum = date('d-m-Y',strtotime($model->event->event->depart));

      $dagen =Day::model()->findByPk( $model->days );

      $besteming = 'Winterberg';

      $depart_str = $model->event->city->name.' - '.$model->event->city->description.' om ' .date('H:i',strtotime($model->event->depart)) . ' uur';

      $message->setBody( array(
        'model'     => $model,
        'datum'     => $datum,
        'dagen'     => ( $dagen == NULL ? '' : $dagen->name ),
        'total'     => number_format($model->total,2,'.',','),
        'depart'    => $depart_str,
        'persons'   => $persons_str,
        'besteming' => $besteming,
      ), 'text/html' );

      $message->addTo('winterbergbus@outlook.com');
      $message->from ='winterbergbus@outlook.com';

      Yii::app()->mail->send($message);
    }

    public function actionCities()
    {
      if( Yii::app()->request->isAjaxRequest )
      {
        $cities = array();
        $results = array();

        Yii::app()->session['days'] = (int)$_GET['selected'];

        $events = Event::model()->findAll('days=:days',array('days'=>Yii::app()->session['days']));
        foreach ($events as $event) {
          foreach($event->departs as $depart)
          {
            $cities[$depart->city->id] = array('label'=>$depart->city->name,'value'=>$depart->city->id);
          }
        }
        if (count($cities)>0)
        {
          $results[] = array('label'=>'Maak een keuze','value'=>'');
          foreach($cities as $city)
          {
            $results[] = $city;
          }
        }
        echo json_encode($results);
        Yii::app()->end();
      }
      else
      {
        throw new CHttpException(400, 'Invalid request.');
      }
    }

    public function actionDates()
    {
      $dates = array();
      $results = array();

      Yii::app()->session['city'] = (int)$_GET['selected'];

      $criteria = new CDbCriteria();
      $criteria->params = array('days'=>Yii::app()->session['days']);
      $criteria->addCondition('days=:days');
      $criteria->order = "depart ASC";

      $events = Event::model()->findAll($criteria);

      // $events = Event::model()->findAll('days=:days',array('days'=>Yii::app()->session['days']));
      foreach ($events as $event) {
        foreach($event->departs as $depart)
        {
          if ($depart->city->id == Yii::app()->session['city'])
          {
            $dates[$depart->id] = array('label'=>$event->depart,'value'=>$depart->id);
          }
        }
      }
      if (count($dates)>0)
      {
        $results[] = array('label'=>'Maak een keuze','value'=>'');
        foreach($dates as $date)
        {
          $results[] = $date;
        }
      }
      echo json_encode($results);
      Yii::app()->end();
    }

    public function actionPersons()
    {
      if (Yii::app()->request->isAjaxRequest)
      {
        $results = array();
        Yii::app()->session['event'] = (int)$_GET['selected'];

        $depart = EventDepart::model()->findByPk(Yii::app()->session['event']);

        if ($depart && $depart->event)
        {
          $event = $depart->event;
          $results[] = array('label'=>'Maak een keuze','value'=>'');
          $max = 20;
          if (($event->avail-$event->booked) < 20 ) $max = ($event->avail-$event->booked);
          for($p=1;$p<=$max;$p++)
          {
            $results[] = array('label'=>$p,'value'=>$p);
          }
        }
        echo json_encode($results);
        Yii::app()->end();
      }
      else
      {
        throw new CHttpException(400,'Invalid request');
      }
    }

    public function getAmount()
    {
      $max = Yii::app()->session['persons'];

      $results = array();
      $results[''] = "Maak een keuze";
      for($i=1;$i<=$max;$i++)
      {
        $results[$i] = $i;
      }

      return $results;
    }

    public function cashIn( $voucher )
    {
      // Valid input ?
      if( $voucher == '' ) return false;

      $args =array(
        'voucherNumber' => $voucher
      );

      // cURL stuff
      $curl =curl_init( 'https://www.emesa-auctions.com/partner-api/voucher/cashin' );

      curl_setopt_array( $curl, array(
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_CONNECTTIMEOUT => 8,
        CURLOPT_TIMEOUT        => 60,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPAUTH       => CURLAUTH_DIGEST,
        CURLOPT_POST           => 1,
        CURLOPT_HTTPHEADER     => array( 'Content-Type: application/json' ),
        CURLOPT_POSTFIELDS     => json_encode( $args ),
        CURLOPT_USERPWD        => 'WinterBergBus:hgtf43Fd'
      ));

      // Do the request
      $dummy =curl_exec( $curl );
      if( $dummy === false )
      {
        echo 'Curl error: ' . curl_error( $curl );

        return false;
      }

      // Close curl
      curl_close( $curl );

      // Decode the response
      $response =json_decode( $dummy );

      // Return true or false depending if it is valid or not
      if( isset( $response->OK )) return true;

      // Error
      return false;
    }
  }