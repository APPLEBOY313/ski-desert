<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>:: Winterbergbus.nl - Busreis Winterberg boeken</title>
  <?php
    $cs =Yii::app()->getClientScript();

    $cs->registerCssFile( Yii::app()->baseUrl . '/themes/default/css/style2.css' );
    $cs->registerCssFile( 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,800' );
  ?>
</head>

<body>
<div id="onder">
  <div id="wrapper">

    <div id="kopw">
      <div id="kopwl">
        <div id="language">
          <ul>
            <li><img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/nl_vakantie_winterberg_sauerland.png" width="18" height="12" /></li>
            <li>
              <a href="http://www.winterbergbus.nl/l_ski_snowboard_snow_trip_company_day_out_winterberg_sauerland_germany_by_bus.html">
                <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/en_ski_snowboard_snow_trip_company_day_out_winterberg_sauerland_germany_by_bus.png" width="18" height="12" border="0" />
              </a>
            </li>
          </ul>
        </div>
        <div id="menu">
          <ul>
            <li><a href="http://www.winterbergbus.nl/l_winterberg_bus_reis_dagje_dagtocht_lang_weekend_mid_week_wintersport_winterberg_winterbergen_sauerland_duitsland.html">UITLEG</a></li> /
            <li><a href="http://www.winterbergbus.nl/l_winterberg_weer_bericht_sneeuw_hoogte_webcams_pistes_langlauf_loipes_ski_gebied_winterberg_winterbergen_sauerland_duitsland.html">SNEEUW &amp; WEBCAMS</a></li> /
            <li><a href="http://www.winterbergbus.nl/l_winterberg_skipas_ski_snowboard_langlauf_slee_huur_huren_prijs_prijzen_skiles_snowboardles_skischool_snowboardschool_winterberg_winterbergen.html">SKIHUUR / PAS / ETC</a></li> /
            <li><a href="http://www.winterbergbus.nl/h_bedrijfsuitje_personeelsuitje_winterberg.html">GROEPEN</a></li> /
            <li><a href="http://www.winterbergbus.nl/l_skikleding_wintersportkleding_outlet_skibroek_ski_jas_handschoenen_skibril_skihelm_wintersport_ski_snowboard_kleding_slee_webshop.html">WEBSHOP</a></li> /
          </ul>
        </div>
      </div>
      <div id="kopwr">
        <a href="http://www.winterbergbus.nl">
          <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/busreis_busreizen_winterberg_sauerland_duitsland.png" width="125" height="125" border="0" />
        </a>
      </div>
    </div>

    <?php echo $content; ?>

  </div>

  <div id="foot">
    <div id="foot-wrap">
      <div class="foot-boxs">
        <div class="foot-txtt">SNEL NAAR</div>
        <div class="foot-link"><a href="http://www.winterbergbus.nl/index.html">Home</a><br />
          <a href="http://www.winterbergbus.nl/l_bus_winterberg_sauerland_bus_winterbergen.html">Ritschema bus Winterberg</a><br />
          <a href="http://www.winterbergbus.nl/l_uitleg_1_een_dag_dagje_dagtocht_winterberg_bus_skieen_snowboarden_langlaufen_wintersport_winterbergen.html">Dagje Winterberg</a><br />
          <a href="http://www.winterbergbus.nl/l_uitleg_lang_weekend_winterberg_midweek_week_sauerland_winterbergen.html">Weekend Winterberg</a><br />
          <a href="http://www.winterbergbus.nl/h_bedrijfsuitje_personeelsuitje_winterberg.html">Bedrijfsuitje Winterberg</a><br />
          <a href="http://www.winterbergbus.nl/l_winterberg_hotel_sauerland_eifel_hotels_appartement_pension.html">Hotels Winterberg</a></div>
      </div>
      <div class="foot-boxs">
        <div class="foot-txtt">WINTERBERG ACTUEEL</div>
        <div class="foot-link"><a href="http://www.winterbergbus.nl/l_winterberg_weer_bericht_sneeuw_hoogte_webcams_pistes_langlauf_loipes_ski_gebied_winterberg_winterbergen_sauerland_duitsland.html">Weer Winterberg</a><br />
          <a href="http://www.winterbergbus.nl/http://www.wintersport-arena.de/nl/winterberg/skigebiet-alpine/winterberg" target="_blank">Sneeuw Winterberg</a><br />
          <a href="http://www.winterbergbus.nl/http://www.skiliftkarussell.de/nl/skigebiet/lift-pisteninfo.html" target="_blank">Pistes Winterberg</a><br />
          <a href="http://www.winterbergbus.nl/http://www.nordicsport-arena.de/de/nordic-winter/" target="_blank">Langlaufen Winterberg</a><br />
          <a href="http://www.winterbergbus.nl/l_winterberg_weer_bericht_sneeuw_hoogte_webcams_pistes_langlauf_loipes_ski_gebied_winterberg_winterbergen_sauerland_duitsland.html">Webcams Winterberg</a></div>
      </div>
      <div class="foot-boxs">
        <div class="foot-txtt">WINTERBERGBUS</div>
        <div class="foot-link"><a href="http://www.winterbergbus.nl/l_winterbergbus_busreis_busreizen_winterberg_winterbergen_voorwaarden.html">Voorwaarden busreizen</a><br />
          <a href="http://www.winterbergbus.nl/l_winterbergbus_vraag_en_antwoord_wintersport_bus_reizen_winterberg.html">Vraag &amp; Antwoord</a><br />
          <a href="http://www.winterbergbus.nl/l_winterbergbus_contact.html">Contact</a></div>
      </div>
      <div class="foot-boxl"><div class="fb-like" data-href="http://www.winterbergbus.nl" data-send="true" data-layout="button_count" data-show-faces="true"></div></div>
      <div id="foot-close">
        <ul>
          <li>© Winterbergbus Snow Included</li> /
          <li>KvK nummer 54143357</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div id="fb-root"></div>

<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html>
