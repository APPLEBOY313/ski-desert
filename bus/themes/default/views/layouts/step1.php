<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>:: Winterbergbus.nl - Boek uw busreis</title>

    <meta name="viewport" content="width=1002px">

    <?php
      $cs =Yii::app()->getClientScript();

      $cs->registerCssFile( Yii::app()->baseUrl . '/themes/default/css/bestellen.css' );

      Yii::app()->clientScript->registerCoreScript( 'jquery' );
      Yii::app()->clientScript->registerCoreScript( 'jquery.ui' );
    ?>

    <script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-55193387-1', 'auto');
      ga('send', 'pageview');
    </script>

  </head>

  <body class="step1">
    <div id="onder">
      <div id="wrapper">

        <div id="kopw">
          <div id="kopwl">
            <div id="language">
              <ul>
                <li><a href="http://www.winterbergbus.nl/"><img src="/themes/default/images/nl_vakantie_winterberg_sauerland.png" width="18" height="12" /></a></li>
                <li><a href="http://www.winterbergbus.nl/l_weekend_day_trip_skiing_winterberg_sauerland_germany_bus_train_car_snow_ski_snowboarding_kids_holland_netherlands.html"><img src="/themes/default/images/en_ski_snowboard_snow_trip_company_day_out_winterberg_sauerland_germany_by_bus.png" width="18" height="12" /></a></li>
              </ul>
            </div>
            <div id="menu">
              <ul>
                <li><a href="http://www.winterbergbus.nl/l_winterberg_bus_reis_dagje_dagtocht_lang_weekend_mid_week_wintersport_winterberg_winterbergen_sauerland_duitsland.html">onze reizen</a></li> /
                <li><a href="http://www.winterbergbus.nl/l_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">sneeuw &amp; webcams</a></li> /
                <li><a href="http://www.winterbergbus.nl/l_wat_is_er_te_doen_in_winterberg_sauerland_activiteiten.html">things to do</a></li> /
                <li><a href="http://www.winterbergbus.nl/l_winterberg_skipas_ski_snowboard_langlauf_slee_skihelm_huur_huren_prijs_prijzen_skiles_snowboardles_skischool_snowboardschool_winterberg_winterbergen.html">skipas / huur / les</a></li> /
                <li><a href="http://www.winterbergbus.nl/h_bedrijfsuitje_personeelsuitje_winterberg.html">groepen</a></li> /
              </ul>
            </div>
          </div>
          <div id="kopwr"><a href="http://www.winterbergbus.nl"><img src="/themes/default/images/busreis_busreizen_winterberg_sauerland_duitsland.png" width="125" height="125" /></a></div>
        </div>

        <div id="boek-wrap">
          <?php echo $content; ?>
        </div>

      </div>
    </div>

    <div id="foot-border"></div>

    <div id="foot">
      <div id="foot-wrap">
        <div class="foot-boxs">
          <div class="foot-txtt">snel naar</div>
          <div class="foot-link">
            <a href="http://www.winterbergbus.nl/">home</a><br />
            <a href="http://www.winterbergbus.nl/l_bus_winterberg_sauerland_bus_winterbergen_schema.html">ritschema bus winterberg</a><br />
            <a href="http://www.winterbergbus.nl/l_uitleg_1_een_dag_dagje_dagtocht_winterberg_bus_skieen_snowboarden_langlaufen_wintersport_winterbergen.html">dagje winterberg</a><br />
            <a href="http://www.winterbergbus.nl/l_uitleg_lang_weekend_winterberg_midweek_week_sauerland_winterbergen.html">weekend winterberg</a><br />
            <a href="http://www.winterbergbus.nl/h_bedrijfsuitje_personeelsuitje_winterberg.html">bedrijfsuitje winterberg</a><br />
            <a href="http://www.winterbergbus.nl/h_studenten_schoolreis_winterberg_schoolreizen_snowworld_bottrop_school_reis_reizen_dag_willingen_winterbergen.html">schoolreis winterberg</a></div>
        </div>
        <div class="foot-boxs">
          <div class="foot-txtt">winterberg actueel</div>
          <div class="foot-link">
            <a href="http://www.winterbergbus.nl/l_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">weer winterberg</a><br />
            <a href="http://www.winterbergbus.nl/l_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">sneeuw winterberg</a><br />
            <a href="http://www.winterbergbus.nl/l_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">pistes winterberg</a><br />
            <a href="http://www.winterbergbus.nl/l_webcams_winterberg_weer_bericht_sneeuw_hoogte_pistes_langlauf_loipes_ski_gebied_winterberg_bergfex_winterbergen_sauerland_duitsland.html">webcams winterberg</a><br />
            <a href="http://www.nordicsport-arena.de/de/nordic-winter/" target="_blank">langlaufen winterberg</a></div>
        </div>
        <div class="foot-boxs">
          <div class="foot-txtt">winterbergbus</div>
          <div class="foot-link">
            <a href="http://www.winterbergbus.nl/h_bedrijfsuitje_personeelsuitje_schoolreis_winterberg_aanvragen.php" target="_blank">offerte groepen</a><br />
            <a href="http://www.winterbergbus.nl/l_winterbergbus_busreis_winterberg_busreizen_winterbergen_voorwaarden.html">voorwaarden busreizen</a><br />
          </div>
        </div>
        <div class="foot-boxl">
          <div class="foot-txtt">volg ons</div>
          <div class="foot-social1">
            <ul>
              <li><a href="https://www.facebook.com/pages/Winterbergbusnl/304489432908047" target="_blank"><img src="/themes/default/images/winterberg_facebook_winterbergbus.png" width="37" height="37" /></a></li>
              <li><a href="https://twitter.com/Winterbergbus" target="_blank"><img src="/themes/default/images/winterberg_twitter_winterbergbus.png" width="37" height="37" /></a></li>
              <li><a href="http://www.pinterest.com/Winterbergbus/" target="_blank"><img src="/themes/default/images/winterberg_pinterest_winterbergbus.png" width="37" height="37" /></a></li>
              <li><a href="http://www.instagram.com/Winterbergbus.nl" target="_blank"><img src="/themes/default/images/winterberg_instagram_winterbergbus.png" width="37" height="37" /></a></li>
            </ul>
          </div>
          <div class="foot-social2">
            <ul>
              <li><a href="http://www.youtube.com/playlist?list=PLcMqWG9AdEF4FmaTATWPQ5dXtrWZ5Ovnv" target="_blank"><img src="/themes/default/images/winterberg_film_youtube_winterbergbus.png" width="37" height="36" /></a></li>
              <li><a href="http://open.spotify.com/user/winterbergbus.nl/playlist/1Mg1TT63Wu3PR3qFi1Pn7m" target="_blank"><img src="/themes/default/images/winterberg_wintersport_apres_ski_muziek_spotify_winterbergbus.png" width="37" height="37" /></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div id="foot-close">
      <div id="foot-close-wrap">
        <div class="foot-close-logo1">
          <ul>
            <li><img src="/themes/default/images/logo_dutchweek_dutchweekend_winterberg_winterbergbus.png" width="119" height="21" /></li>
            <li><img src="/themes/default/images/logo_apres_ski_apresski_winterberg_flying_deer_vliegend_hert_red_bull_jagermeister.png" width="37" height="45" /></li>
            <li><img src="/themes/default/images/logo_hotel_der_brabander_winterberg_winterbergbus.png" width="84" height="45" /></li>
            <li><img src="/themes/default/images/logo_wintersport_groupon_winterberg_sneeuw_winterbergbus_travelbird.png" width="88" height="25" /></li>
            <li><img src="/themes/default/images/logo_rtl4_snowmagazine_rtl5_wintersport_sneeuw_winterberg_winterbergbus.png" width="44" height="45" /></li>
            <li><img src="/themes/default/images/logo_bergfex_webcams_weerbericht_sneeuw_winterberg.png" width="95" height="31" /></li>
            <li><img src="/themes/default/images/logo_snowworld_alpincenter_wintersport_winterberg.png" width="106" height="31" /></li>
          </ul>
        </div>
        <div class="foot-close-txt">© Winterbergbus is een merk van Winterbergbus Snow Included, KvK nummer 54143357<br />
          Winterbergbus Snow Included, Simon Carmiggelthof 158, 2492 JN Den Haag
        </div>
      </div>
    </div>

    <script type="text/javascript">
      function initDeparture( filter )
      {
        filter =filter || '';

        // Disable fields
        $('#days').attr('disabled', 'disabled').addClass('disabled').empty();
        $('#calendar').val( '' ).attr('disabled', 'disabled').addClass('disabled');
        $('#passengers').attr('disabled', 'disabled').addClass('disabled').empty();
        $('#bookingbutton').attr('disabled', 'disabled').addClass('disabled');

        // Get departure data
        $.ajax({
          url      : 'http://bus.winterbergbus.nl/ajax/citylist/type/depart/filter/' + filter,
          cache    : false,
          dataType : "json",
          error : function( data )
          {
            console.log( 'error: initDeparture' );
          },
          success : function( data )
          {
            // Display data
            if( data.status == 'success' )
            {
              var departure =$('#departure');

              // Place
              departure.empty( );
              departure.append( '<option value="">Selecteer vertrekplaats</option>' );
              departure.append( data.data );

              // Enable departure
              departure.removeAttr('disabled').removeClass('disabled');
            }
          }
        });
      }

      function initDestination( filter )
      {
        filter =filter || '';

        // Get destination data
        $.ajax({
          url      : 'http://bus.winterbergbus.nl/ajax/citylist/type/dest/filter/' + filter,
          cache    : false,
          dataType : "json",
          error : function( data )
          {
            console.log( 'error: initDestination' );
          },
          success : function( data )
          {
            // Display data
            if( data.status == 'success' )
            {
              var dest =$('#destination');

              // Place
              dest.empty( );
              dest.append( '<option value="">Winterberg (centrum skigebied)</option>' );
              dest.append( data.data );

              // Enable destination
              dest.removeAttr('disabled').removeClass('disabled');
            }
          }
        });
      }

      jQuery(document).ready(function()
      {
        // global variable to store retrieved data
        var dates=[];

        // Init the form
        initDeparture( 'nl' );
        initDestination( 'de' );

        // Select retour
        $('#retour').on('click', null, function()
        {
          $(this).addClass('selected');
          $('#type').val('retour');
          $('#oneway').removeClass('selected');

          // Init form
          initDeparture( 'nl' );
          initDestination( 'de' );
        });

        // Select oneway
        $('#oneway').on('click', null, function()
        {
          $(this).addClass('selected');
          $('#type').val('oneway');
          $('#retour').removeClass('selected');

          // Init form
          initDeparture();

          // Disable departure
          $('#destination').empty().attr('disabled', 'disabled').addClass('disabled');
        });

        // When a departure place has been selected
        $('#departure').on('change', null, function()
        {
          // Retour or single ?
          if( $('#oneway').hasClass('selected') )
          {
            var city =$(this).find('option:selected').html().toLowerCase();

            if( city.indexOf('winter') != -1 ) initDestination('nl');
            else                               initDestination('de');

            $.ajax({
              url      : 'http://bus.winterbergbus.nl/data/departinfo/id/' + $(this).val( ),
              cache    : false,
              dataType : 'JSONP',
              error : function( data )
              {
                console.log( 'error' );
              },
              success : function( data )
              {
                dates =data;

                if( data.length == 0 )
                {
                  alert( 'Ik heb helaas geen vertrek data gevonden');

                  initDeparture( 'nl' );
                }
                else
                {
                  $('#calendar').datepicker(
                    {
                      dateFormat: 'yy-mm-dd',
                      beforeShowDay: function( date )
                      {
                        // Get a date to compare with
                        day =$.datepicker.formatDate( 'yy-mm-dd', date );

                        // Loop through all the data to see if we got a match
                        for( i=0;i<dates.length;i++ )
                        {
                          if( dates[i].date == day ) return [true, 'highlight', ''];
  //                        if( dates[i].date == day ) return [true, 'highlight', dates[i].left + ' plaatsen vrij'];
                        }

                        return [false, "", ""];
                      }
                    });

                  // Enable calendar
                  $('#calendar').removeAttr('disabled').removeClass('disabled');
                }
              }
            });
          }
          else
          {
            if( $(this).val() == '' ) initDeparture( 'nl' );
            else
            {
              $.ajax({
                url      : 'http://bus.winterbergbus.nl/data/departinfo/id/' + $(this).val( ),
                cache    : false,
                dataType : 'JSONP',
                error : function( data )
                {
                  console.log( 'error' );
                },
                success : function( data )
                {
                  dates =data;

                  if( data.length == 0 )
                  {
                    alert( 'Ik heb helaas geen vertrek data gevonden');

                    initDeparture( 'nl' );
                  }
                  else
                  {
                    $('#calendar').datepicker(
                      {
                        dateFormat: 'yy-mm-dd',
                        beforeShowDay: function( date )
                        {
                          // Get a date to compare with
                          day =$.datepicker.formatDate( 'yy-mm-dd', date );

                          // Loop through all the data to see if we got a match
                          for( i=0;i<dates.length;i++ )
                          {
                            if( dates[i].date == day ) return [true, 'highlight', ''];
  //                        if( dates[i].date == day ) return [true, 'highlight', dates[i].left + ' plaatsen vrij'];
                          }

                          return [false, "", ""];
                        }
                      });

                    // Enable calendar
                    $('#calendar').removeAttr('disabled').removeClass('disabled');
                  }
                }
              });
            }
          }
        });

        // When a date has been selected
        $('#calendar').on('change', null, function()
        {
          // Get a date to compare with
          var day =$(this).val( );
          var type =$('#type');

          // lookup the selected date
          for( i=0;i<dates.length;i++ )
          {
            if( dates[i].date == day )
            {
              // Store the selected depart id
              $('#depart').val( dates[i].id );

              // Fill the days dropdown
              if( type.val() != 'retour' ) $('#days').empty();
              else
              {
                days =$("#days");
                data =dates[i].days
                for( j=0;j<data.length;j++ )
                {
                  days.append( '<option value="' + data[j].id + '">' + data[j].name + '</option>' );
                }
              }

              // Fill the free places dropdown
              passengers =$("#passengers");
              passengers.empty( );
              for( j=1;j<=20&&j<=dates[i].left;j++ )
              {
                if( j == 1 ) passengers.append( '<option value="' + j + '">' + j + ' persoon</option>' );
                else         passengers.append( '<option value="' + j + '">' + j + ' personen</option>' );
              }

              // Enable days and passengers
              if( type.val() == 'retour' ) days.removeAttr('disabled').removeClass('disabled');
              passengers.removeAttr('disabled').removeClass('disabled');
              $('#bookingbutton').removeAttr('disabled').removeClass('disabled');
            }
          }
        });
      });
    </script>

  </body>
</html>
