<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $this->pageTitle; ?></title>
    <link href="http://www.bedrijfsuitje-winterberg.nl/bus_reis_winterberg_files/style02.css" rel="stylesheet" type="text/css" />
    <style>
        .datepicker {font-size:13px;}
        .txt3 { font-size:13px;}
        .control-label {font-size:13px;}
    </style>
</head>

<body style="-webkit-text-size-adjust:none">
<table width="885" border="0" align="center" cellpadding="0" cellspacing="0" class="bgwhi">
    <tr>
        <td><table width="885" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="34" height="35">&nbsp;</td>
                <td width="735" class="txt1"><a href="http://www.winterbergbus.nl">HOME</a> &nbsp;&nbsp;<img src="http://www.bedrijfsuitje-winterberg.nl/bus_reis_winterberg_files/winterberg_winterbergen_pijl.gif" width="4" height="7" /> &nbsp;&nbsp;</td>
                <td width="25" valign="bottom"><a href="http://www.facebook.com/pages/Winterbergbus-Snow-Included/304489432908047" target="_blank"><img src="http://www.bedrijfsuitje-winterberg.nl/bus_reis_winterberg_files/bus_winterberg_facebook_klein.png" width="25" height="25" border="0" /></a></td>
                <td width="5">&nbsp;</td>
                <td width="25" valign="bottom"><a href="https://twitter.com/#!/Winterbergbus" target="_blank"><img src="http://www.bedrijfsuitje-winterberg.nl/bus_reis_winterberg_files/winterberg_route_auto_twitter_klein.png" width="25" height="25" border="0" /></a></td>
                <td width="5">&nbsp;</td>
                <td width="25" valign="bottom"><a href="http://www.youtube.com/user/FerienweltWinterberg/videos" target="_blank"><img src="http://www.bedrijfsuitje-winterberg.nl/bus_reis_winterberg_files/winterberg_youtube_filmpjes_filmpje_film_plaatjes_plaatje.png" width="25" height="25" border="0" /></a></td>
                <td width="31">&nbsp;</td>
            </tr>
        </table>
            <table width="885" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="5">&nbsp;</td>
                </tr>
            </table>
            <table width="885" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20" height="205"></td>
                    <td width="845" height="205" class="tl">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/winterberg_wandelen_wandel_routes_langlaufen_loipes-1.png" width="845" height="205" />
                    </td>
                    <td width="20">&nbsp;</td>
                </tr>
            </table>
            <table width="885" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20" height="46">&nbsp;</td>
                    <td width="835" class="txt2">Boek ticket(s)</td>
                    <td width="30">&nbsp;</td>
                </tr>
            </table>
            <table width="885" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="22" height="20">&nbsp;</td>
                    <td width="835">&nbsp;</td>
                    <td width="28">&nbsp;</td>
                </tr>
            </table>
            <table width="885" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="22">&nbsp;</td>
                    <td width="835" class="txt3"><?php echo $content; ?></td>
                    <td width="28">&nbsp;</td>
                </tr>
            </table>
            <table width="885" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="25" height="33">&nbsp;</td>
                    <td width="835" class="txt5"><a href="http://www.winterbergbus.nl" style="color:#EE4097;">HOME</a> &nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;</td>
                    <td width="25">&nbsp;</td>
                </tr>
            </table>
            <table width="885" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="40">&nbsp;</td>
                </tr>
            </table></td>
    </tr>
</table>
</body>
</html>
