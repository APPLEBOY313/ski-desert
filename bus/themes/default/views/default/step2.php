  <div id="kassablok">

    <?php

      $form =$this->beginWidget( 'CActiveForm', array( 'id'                     => 'step2Form',

                                                       'enableAjaxValidation'   => false,

                                                       'enableClientValidation' => true,

                                                       'clientOptions'          => array( 'validateOnSubmit' => true,

                                                                                          'validateOnChange' => true,

                                                                                          'validateOnType'   => false,

                                                                                          'hideErrorMessage' => true,

                                                       ),

      ));

    ?>



      <div id="kassawrap">

        <div class="kassakop">

          <h1>KASSA</h1>

          <p>Vul de onderstaande velden in om je boeking te voltooien</p>

        </div>



        <div class="kassakolom">

          <div class="blok">

            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_1.png" class="nummer" />



            <h3>hoofdboeker</h3>



            <div class="halve">

              <?php

                echo $form->labelEx( $model, 'voornaam' );

                echo $form->textfield( $model, 'voornaam' );

                echo $form->error( $model, 'voornaam' );

              ?>

            </div>

            <div class="halve">

              <?php

                echo $form->labelEx( $model, 'achternaam' );

                echo $form->textfield( $model, 'achternaam' );

                echo $form->error( $model, 'achternaam' );

              ?>

            </div>

            <div class="halve">

              <?php

                echo $form->labelEx( $model, 'email' );

                echo $form->textfield( $model, 'email' );

                echo $form->error( $model, 'email' );

              ?>

            </div>

            <div class="halve">

              <?php

                echo $form->labelEx( $model, 'email2' );

                echo $form->textfield( $model, 'email2' );

                echo $form->error( $model, 'email2' );

              ?>

            </div>

            <div class="full">

              <?php

                echo $form->labelEx( $model, 'telefoon1' );

                echo $form->textfield( $model, 'telefoon1' );

                echo $form->error( $model, 'telefoon1' );

              ?>

            </div>

            <div class="full">

              <?php

                echo $form->labelEx( $model, 'telefoon2' );

                echo $form->textfield( $model, 'telefoon2' );

                echo $form->error( $model, 'telefoon2' );

              ?>

            </div>

            <div class="full">

              <?php

                echo $form->labelEx( $model, 'plaats' );

                echo $form->textfield( $model, 'plaats' );

                echo $form->error( $model, 'plaats' );

              ?>

            </div>

          </div>



          <div class="blok">

            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_2.png" class="nummer" />



            <h3>reizigers (incl. jezelf)</h3>

            <?php for( $i=1;$i<=Yii::app()->session['persons'];$i++ ) : ?>

              <div class="halve">

                <?php

                  echo $form->labelEx( $model, 'voornaam' . $i );

                  echo $form->textfield( $model, 'voornaam' . $i );

                  echo $form->error( $model, 'voornaam' . $i );

                ?>

              </div>

              <div class="halve">

                <?php

                  echo $form->labelEx( $model, 'achternaam' . $i );

                  echo $form->textfield( $model, 'achternaam' . $i );

                  echo $form->error( $model, 'achternaam' . $i );

                ?>

              </div>

            <?php endfor; ?>

          </div>

        </div>



        <div class="kassakolom">

          <div class="blok">

            <img class="bus" src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/winterbergbus.jpg" />

          </div>



          <div class="blok">

            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_3.png" class="nummer" />



            <h3>hoe ken je ons?</h3>

            <input type="radio" id="radiox1" name="radiox" value="1" /><label for="radiox1" class="ref">Zoekmachine</label><br />

            <input type="radio" id="radiox2" name="radiox" value="2" /><label for="radiox2" class="ref">Social media (Facebook etc.)</label><br />

            <input type="radio" id="radiox3" name="radiox" value="3" /><label for="radiox3" class="ref">Reclame / advertentie</label><br />

            <input type="radio" id="radiox5" name="radiox" value="5" /><label for="radiox5" class="ref">Via vrienden</label><br />

            <input type="radio" id="radiox6" name="radiox" value="6" /><label for="radiox6" class="ref">Anders, namelijk</label><input type="text" name="other" id="referrer" />

          </div>

        </div>



        <div class="kassakolom">

          <div class="blok">

            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_4.png" class="nummer" />

            <h3>kies betaalmethode</h3>

            <input id="method" type="hidden" value="" name="BookingStep2[payment]" >



            <div class="paymentGroup">

                <?php

                    /*

                     * Steuerung siehe:

                     * \domains\winterbergbus.nl\public_html\bus\protected\controllers\DefaultController.php

                     * Daniel, 23.11.2015

                     */

                ?>



              <div id="payment_ideal" data="ideal" title="Ideal" class="payment"></div>

              <div id="payment_paypal" data="paypal" title="Paypal" class="payment"></div>

              <div style="width: 120px; height: 65px;" id="payment_mistercash" data="mistercash" title="MisterCash" class="payment"></div>

              



              <?php if( Yii::app()->session['type'] == 'retour' ) { ?>

              <div id="payment_hema" data="hema" title="SnowVoucher" class="payment"></div>

              <?php } ?>



              <?php if( Yii::app()->session['persons'] == 2 && Yii::app()->session['type'] == 'retour' ) { ?>

                <div id="payment_vakantieveilingen" data="vakantieveilingen" class="payment"></div>

              <?php } ?>



            </div>



            <?php if( Yii::app()->session['persons'] == 2 ) { ?>

              <div id="coupon" style="display: none;">

                <input type="text" value="" name="coupon" />

                <div id="vv-check">Check</div>

              </div>

            <?php } ?>



            <div id="codes">

              <textarea name="codes" placeholder="Voer hier uw vouchercode(s) in zonder komma. Bijvoorbeeld: s43res3e t2retred etc"></textarea>

              <div id="hema-check">Check</div>

            </div>



          </div>



          <div class="blok">

            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_5.png" class="nummer" />



            <?php

              $city   =City::model()->findByPk( Yii::app()->session['city'] );

              $depart =EventDepart::model()->findByPk( Yii::app()->session['event'] );



              $people   =Yii::app()->session['persons'];



              if( Yii::app()->session['type'] == 'retour' ) $price =$depart->price;

              else                                          $price =24;



              $subtotal =$price * $people;

              $costs    =Setting::getValue( 'booking_cost', '4.5' );

              $total    =$subtotal + $costs;

            ?>



            <h3>controleer je boeking</h3>



            <table>

              <tr class="dik"><td>Reis</td><td class="rechts">Aantal pers.</td><td>Subtotaal</td></tr>

              <tr><td class="departure"><?= $city->name; ?><br /> vertrek om <?= date( 'H:i', strtotime( $depart->depart )); ?></td><td class="rechts"><?php echo $people; ?></td><td>&euro; <?php echo number_format( $price, 2 ); ?></td></tr>

              <tr><td colspan="2" class="rechts">Subtotaal</td><td>&euro; <?php echo number_format( $subtotal, 2 ); ?></td></tr>

              <tr><td colspan="2" class="rechts">Eindtotaal</td><td>&euro; <?php echo number_format( $subtotal, 2 ); ?></td></tr>

              <tr>
                <td colspan="2" class="rechts">Reserveringskosten</td><td>&euro; <?php echo number_format( $costs, 2 ); ?></td></tr>

              <tr><td colspan="2" class="rechts">Eindtotaal</td><td>&euro; <?php echo number_format( $total, 2 ); ?></td></tr>

              <tr><td colspan="3">&nbsp;</td></tr>

            </table>



            <input type="checkbox" id="akkoord" name="agree" value="1" /><label for="akkoord">Ik accepteer de <a target="_blank" href="http://www.winterbergbus.nl/l_winterbergbus_busreis_busreizen_winterberg_winterbergen_voorwaarden.html">Algemene voorwaarden</a></label>



            <div id="kassabutton" class="inactief">Plaats uw boeking</div>



          </div>

        </div>



        <div style="clear:both; height:30px;"></div>



      </div>



    <?php $this->endWidget(); ?>



  </div>



  <script type="text/javascript">

    $(document).ready(function()

    {

      // Point 3

      $("#referrer").attr("disabled", "disabled");

      $("input[name=radiox]").click(function()

      {

        if ($(this).val() != '6') $("#referrer").attr("disabled", "disabled");

        else                      $("#referrer").removeAttr("disabled");

      });



      // Terms selected ?

      $("#akkoord").change(function()

      {

        $("#kassabutton").toggleClass('inactief');

      });



      // Submit button

      $("#kassabutton").on( 'click', function()

      {

        // Payment selected ?

        if( $("#method").val() != '' )

        {

          // Terms selected ?

          if( $("#akkoord").prop('checked') == true )

          {

            // iDeal or Other payment method ?

            switch( $("#method").val( ))

            {

              // Hema coupon payment (1 per person)

              case 'hema' : $.ajax(

                            {

                              url      : "<?= $this->createUrl( '/ajax/checkhema' ); ?>",

                              data     : { codes : $("#codes textarea").val() },

                              type     : 'post',

                              dataType : "json",

                              success  : function( data )

                              {

                                if( data.status == 'failed' )

                                {

                                  $("#codes textarea").removeClass( 'success' );

                                  $("#codes textarea").addClass( 'error' );

                                }

                                else

                                {

                                  $("#codes textarea").removeClass( 'error' );

                                  $("#codes textarea").addClass( 'success' );



                                  // Submit the form

                                  $("#step2Form").submit();

                                }

                              }

                            });



                            break;



              // Sisow iDeal payment

              case 'ideal' : $("#step2Form").submit();

                             break;



              // Sisow MisterCash payment

              case 'mistercash' : $("#step2Form").submit();

                             break;



              // Paypal iDeal payment

              case 'paypal' : $("#step2Form").submit();

                              break;



              // Vakantieveiling coupon (2 people only)

              case 'vakantieveilingen' : $.ajax(

                                         {

                                           url      : "<?= $this->createUrl( '/ajax/checkvoucher' ); ?>",

                                           data     : { voucher : $("#coupon input").val() },

                                           type     : 'post',

                                           dataType : "json",

                                           success  : function( data )

                                           {

                                             if( data.status == 'failed' )

                                             {

                                               $("#coupon input").val( '' );

                                               $("#coupon input").removeClass( 'success' );

                                               $("#coupon input").addClass( 'error' );

                                             }

                                             else

                                             {

                                               $("#coupon input").removeClass( 'error' );

                                               $("#coupon input").addClass( 'success' );



                                               // Submit the form

                                               $("#step2Form").submit();

                                             }

                                           }

                                         });



                                         break;

            }

          }

        }

        else

        {

          // No payment is selected at the moment. Display error

          alert('Er is nog geen betaal optie geslecteerd.');

        }

      });



      // Payment button clicked ?

      $("[id^=payment_]").on('click', null, function()

      {

        if( !$(this).hasClass('selected'))

        {

          // Remove the selected class from the current selected payment

          $("[id^=payment_]").removeClass('selected');



          // Set payment to data attribute

          $("#method").val($(this).attr('data'));



          // Show or hide the coupon field

          switch( $("#method").val( ))

          {

            // Hema coupon payment (1 per person)

            case 'hema' : $("#codes textarea").val( '' );

                          $('#coupon').hide();

                          $("#codes").show();

                          break;



            // Sisow iDeal payment

            case 'ideal' : $("#coupon").hide();

                           $('#codes').hide();

                           break;



            // Sisow Paypal payment

            case 'paypal' : $("#coupon").hide();

                            $('#codes').hide();

                            break;



            // Sisow MisterCash payment

            case 'mistercash' : $("#coupon").hide();

                            $('#codes').hide();

                            break;

                            

            // Vakantieveiling coupon (2 people only)

            case 'vakantieveilingen' : $("#coupon input").val( '' );

                                       $('#codes').hide();

                                       $("#coupon").show();

                                       break;

          }



          // Add selected class

          $(this).addClass('selected');

        }

      });



      // Vakantieveilingen voucher Check button

      $("#vv-check").on('click', null, function()

      {

        $.ajax(

        {

          url      : "<?= $this->createUrl( '/ajax/checkvoucher' ); ?>",

          data     : { voucher : $("#coupon input").val() },

          type     : 'post',

          dataType : "json",

          success  : function( data )

                     {

                       if( data.status == 'failed' )

                       {

                         $("#coupon input").val( '' );

                         $("#coupon input").removeClass( 'success' );

                         $("#coupon input").addClass( 'error' );

                       }

                       else

                       {

                         $("#coupon input").removeClass( 'error' );

                         $("#coupon input").addClass( 'success' );

                       }

                     }

        });

      });



      // Hema code check

      $("#hema-check").on('click', null, function()

      {

        $.ajax(

        {

          url      : "<?= $this->createUrl( '/ajax/checkhema' ); ?>",

          data     : { codes : $("#codes textarea").val() },

          type     : 'post',

          dataType : "json",

          success  : function( data )

                     {

                       if( data.status == 'failed' )

                       {

                         $("#codes textarea").removeClass( 'success' );

                         $("#codes textarea").addClass( 'error' );

                       }

                       else

                       {

                         $("#codes textarea").removeClass( 'error' );

                         $("#codes textarea").addClass( 'success' );

                       }

                     }

        });

      });

    });

  </script>

