<div id="boek-banner">
  <img src="/themes/default/images/boek_busreis_winterberg.png" width="1002" height="246" />
</div>

<div id="boek-title">Boek busticket nu</div>

<div id="boek-content">
  <div id="boek-content-inner">

    <div class="leftSide">

      <form id="bookingForm" method="post" action="http://bus.winterbergbus.nl/default/step2">

        <input type="hidden" name="step1[type]" id="type" value="retour"/>
        <input type="hidden" name="step1[depart]" id="depart" value="" />

        <div class="formRow">
          <label>Soort</label>
          <div id="retour" class="radio selected">Retour</div>
           <!-- <div id="oneway" class="radio">Enkele reis</div> -->
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Van</label>
          <select id="departure" name="step1[vertrek]" class="groot dropdown"></select>
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Naar</label>
          <select id="destination" name="step1[bestemming]" class="groot dropdown"></select>
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Vertrekdatum</label>
          <input type="text" name="step1[datum]" id="calendar" size="10" />
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Aantal dagen</label>
          <select id="days" name="step1[verblijfsduur]" class="dropdown"></select>
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Aantal personen</label>
          <div class="klein">
            <select id="passengers" name="step1[personen]" class="dropdown"></select>
          </div>
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <input type="submit" id="bookingbutton" value="VIND BUSREIS" />
        </div>

      </form>

    </div>

    <div class="rightSide">
      <div class="youtube">
        <iframe width="459" height="291" src="//www.youtube.com/embed/S-ivNAYxHno?rel=0" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>

    <div style="clear: both"></div>

  </div>
</div>


<script type="text/javascript">
  function initDeparture( filter )
  {
    filter =filter || '';

    // Disable fields
    $('#days').attr('disabled', 'disabled').addClass('disabled').empty();
    $('#calendar').val( '' ).attr('disabled', 'disabled').addClass('disabled');
    $('#passengers').attr('disabled', 'disabled').addClass('disabled').empty();
    $('#bookingbutton').attr('disabled', 'disabled').addClass('disabled');

    // Get departure data
    $.ajax({
      url      : 'http://bus.winterbergbus.nl/ajax/citylist/type/depart/filter/' + filter,
      cache    : false,
      dataType : "json",
      error : function( data )
              {
                console.log( 'error: initDeparture' );
              },
      success : function( data )
                {
                  // Display data
                  if( data.status == 'success' )
                  {
                    var departure =$('#departure');

                    // Place
                    departure.empty( );
                    departure.append( '<option value="">Selecteer vertrekplaats</option>' );
                    departure.append( data.data );

                    // Enable departure
                    departure.removeAttr('disabled').removeClass('disabled');
                  }
                }
    });
  }

  function initDestination( filter )
  {
    filter =filter || '';

    // Get destination data
    $.ajax({
      url      : 'http://bus.winterbergbus.nl/ajax/citylist/type/dest/filter/' + filter,
      cache    : false,
      dataType : "json",
      error : function( data )
              {
                console.log( 'error: initDestination' );
              },
      success : function( data )
                {
                  // Display data
                  if( data.status == 'success' )
                  {
                    var dest =$('#destination');

                    // Place
                    dest.empty( );
                    dest.append( '<option value="">Winterberg (centrum skigebied)</option>' );
                    dest.append( data.data );

                    // Enable destination
                    dest.removeAttr('disabled').removeClass('disabled');
                  }
                }
    });
  }

  jQuery(document).ready(function()
  {
    // global variable to store retrieved data
    var dates=[];

    // Init the form
    initDeparture( 'nl' );
    initDestination( 'de' );

    // Select retour
    $('#retour').on('click', null, function()
    {
      $(this).addClass('selected');
      $('#type').val('retour');
      $('#oneway').removeClass('selected');

      // Init form
      initDeparture( 'nl' );
      initDestination( 'de' );
    });

    // Select oneway
    $('#oneway').on('click', null, function()
    {
      $(this).addClass('selected');
      $('#type').val('oneway');
      $('#retour').removeClass('selected');

      // Init form
      initDeparture();

      // Disable departure
      $('#destination').empty().attr('disabled', 'disabled').addClass('disabled');
    });

    // When a departure place has been selected
    $('#departure').on('change', null, function()
    {
      // Retour or single ?
      if( $('#oneway').hasClass('selected') )
      {
        var city =$(this).find('option:selected').html().toLowerCase();

        if( city.indexOf('winter') != -1 ) initDestination('nl');
        else                               initDestination('de');

        $.ajax({
          url      : 'http://bus.winterbergbus.nl/data/departinfo/id/' + $(this).val( ),
          cache    : false,
          dataType : 'JSONP',
          error : function( data )
          {
            console.log( 'error' );
          },
          success : function( data )
          {
            dates =data;

            if( data.length == 0 )
            {
              alert( 'Ik heb helaas geen vertrek data gevonden');

              initDeparture( 'nl' );
            }
            else
            {
              $('#calendar').datepicker(
                {
                  dateFormat: 'yy-mm-dd',
                  beforeShowDay: function( date )
                  {
                    // Get a date to compare with
                    day =$.datepicker.formatDate( 'yy-mm-dd', date );

                    // Loop through all the data to see if we got a match
                    for( i=0;i<dates.length;i++ )
                    {
                      if( dates[i].date == day ) return [true, 'highlight', ''];
//                        if( dates[i].date == day ) return [true, 'highlight', dates[i].left + ' plaatsen vrij'];
                    }

                    return [false, "", ""];
                  }
                });

              // Enable calendar
              $('#calendar').removeAttr('disabled').removeClass('disabled');
            }
          }
        });
      }
      else
      {
        if( $(this).val() == '' ) initDeparture( 'nl' );
        else
        {
          $.ajax({
            url      : 'http://bus.winterbergbus.nl/data/departinfo/id/' + $(this).val( ),
            cache    : false,
            dataType : 'JSONP',
            error : function( data )
            {
              console.log( 'error' );
            },
            success : function( data )
            {
              dates =data;

              if( data.length == 0 )
              {
                alert( 'Ik heb helaas geen vertrek data gevonden');

                initDeparture( 'nl' );
              }
              else
              {
                $('#calendar').datepicker(
                {
                  dateFormat: 'yy-mm-dd',
                  beforeShowDay: function( date )
                  {
                    // Get a date to compare with
                    day =$.datepicker.formatDate( 'yy-mm-dd', date );

                    // Loop through all the data to see if we got a match
                    for( i=0;i<dates.length;i++ )
                    {
                      if( dates[i].left > 0 )
                      {
                        if( dates[i].date == day ) return [true, 'highlight', ''];
//                        if( dates[i].date == day ) return [true, 'highlight', dates[i].left + ' plaatsen vrij'];
                      }
                    }

                    return [false, "", ""];
                  }
                });

                // Enable calendar
                $('#calendar').removeAttr('disabled').removeClass('disabled');
              }
            }
          });
        }
      }
    });

    // When a date has been selected
    $('#calendar').on('change', null, function()
    {
      // Get a date to compare with
      var day =$(this).val( );
      var type =$('#type');

      // lookup the selected date
      for( i=0;i<dates.length;i++ )
      {
        if( dates[i].date == day )
        {
          // Store the selected depart id
          $('#depart').val( dates[i].id );

          // Fill the days dropdown
          if( type.val() != 'retour' ) $('#days').empty();
          else
          {
            days =$("#days");
            data =dates[i].days
            for( j=0;j<data.length;j++ )
            {
              days.append( '<option value="' + data[j].id + '">' + data[j].name + '</option>' );
            }
          }

          // Fill the free places dropdown
          passengers =$("#passengers");
          passengers.empty( );
          for( j=1;j<=20&&j<=dates[i].left;j++ )
          {
            if( j == 1 ) passengers.append( '<option value="' + j + '">' + j + ' persoon</option>' );
            else         passengers.append( '<option value="' + j + '">' + j + ' personen</option>' );
          }

          // Enable days and passengers
          if( type.val() == 'retour' ) days.removeAttr('disabled').removeClass('disabled');
          passengers.removeAttr('disabled').removeClass('disabled');
          $('#bookingbutton').removeAttr('disabled').removeClass('disabled');
        }
      }
    });
  });
</script>
