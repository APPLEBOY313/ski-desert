<div id="kassablok">
  <div id="kassawrap">

    <form id="booking-step4" method="post" action="">

      <div class="kassakolom">
        <div class="blok">
          <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_5.png" class="nummer" />
          <h3>Maak uw reis compleet</h3>
        </div>
      </div>

      <div style="clear: both"></div>

      <div class="product left">
        <div class="title">stoelen bij elkaar</div>
        <div class="image">
          <img src="/themes/default/images/winterbergbus_stoelen.png" width="151" height="180" />
        </div>
        <div class="info top">
          <div class="price">€ 3,50 <span class="price-small" style="background: none;">per boeking</span></div>
          <ul>
            <li>stoelen gegarandeerd bij elkaar</li>
            <li>op naam gereserveerd</li>
          </ul>
        </div>
        <div class="choice">
          <label>Stoelen reserveren?</label>
          <select id="sidebyside" name="Step4[sidebyside]">
            <option value="nee" selected="selected">nee</option>
            <option value="ja">ja</option>
          </select>
        </div>
      </div>

      <div class="product right">
        <div class="title">Skihelm</div>
        <div class="image">
          <img src="/themes/default/images/skihelm_huren_winterberg.png" width="183" height="165" />
        </div>
        <div class="info top">
          <div class="price">€ 3,50 <span class="linethr">lokaal € 4,00</span></div>
          <ul>
            <li>korting via Winterbergbus</li>
            <li>veiligheid boven alles</li>
          </ul>
        </div>
        <div class="choice">
          <label>Aantal helmen</label>
          <select id="helmets" name="Step4[helmets]">
            <option value="geen" selected="selected">Geen</option>
            <?php for( $i=1;$i<=Yii::app()->session['people'];$i++ ) echo "<option value='$i'>$i " . ( $i == 1 ? 'Skihelm' : 'Skihelmen') . "</option>"; ?>
          </select>
        </div>
      </div>

      <div style="clear: both"></div>

      <div class="product left">
        <div class="title">Ski's (set)</div>
        <div class="image">
          <img src="/themes/default/images/ski_schoenen_huur.jpg" width="458" height="109" />
        </div>
        <div class="choice">
          <label>Selecteer aantal sets:</label>
          <select id="skis" name="Step4[skis]">
            <option value="geen" selected="selected">Geen</option>
            <?php for( $i=1;$i<=Yii::app()->session['people'];$i++ ) echo "<option value='$i'>$i " . ( $i == 1 ? 'Set' : 'Sets') . "</option>"; ?>
          </select>
        </div>
        <div class="info top">
          <div class="price">€ 14,50 <span class="linethr">lokaal € 16,00</span></div>
          <ul>
            <li>korting via Winterbergbus</li>
            <li>huur direct aan de piste</li>
            <li>verzekerd van beschikbaarheid</li>
          </ul>
        </div>
      </div>

      <div class="product right">
        <div class="title">Snowboard (set)</div>
        <div class="image">
          <img src="/themes/default/images/snowboard_schoenen_huur.png" width="450" height="109" />
        </div>
        <div class="choice">
          <label>Selecteer aantal sets:</label>
          <select id="snowboards" name="Step4[snowboards]">
            <option value="geen" selected="selected">Geen</option>
            <?php for( $i=1;$i<=Yii::app()->session['people'];$i++ ) echo "<option value='$i'>$i " . ( $i == 1 ? 'Set' : 'Sets') . "</option>"; ?>
          </select>
        </div>
        <div class="info top">
          <div class="price">€ 17,50 <span class="linethr">lokaal € 19,00</div>
          <ul>
            <li>korting via Winterbergbus</li>
            <li>huur direct aan de piste</li>
            <li>verzekerd van beschikbaarheid</li>
          </ul>
        </div>
      </div>

      <div style="clear: both"></div>

      <div class="product left">
        <div class="title">LanglaufSki's (set)</div>
        <div class="image">
          <img src="/themes/default/images/nordic_touring_langlaufski_huren_winterberg.jpg" width="458" height="109" />
        </div>
        <div class="choice">
          <label>Selecteer aantal sets:</label>
          <select id="langlauf" name="Step4[langlauf]">
            <option value="geen" selected="selected">Geen</option>
            <?php for( $i=1;$i<=Yii::app()->session['people'];$i++ ) echo "<option value='$i'>$i " . ( $i == 1 ? 'Set' : 'Sets') . "</option>"; ?>
          </select>
        </div>
        <div class="info top">
          <div class="price">€ 9,00 <span class="linethr">lokaal € 10,00</span></div>
          <ul>
            <li>korting via Winterbergbus</li>
            <li>huur direct aan de piste</li>
            <li>verzekerd van beschikbaarheid</li>
          </ul>
        </div>
      </div>

      <div class="product right">
        <div class="title">Slee huren</div>
        <div class="image">
          <img src="/themes/default/images/slee_rodel_huren_winterberg.png" width="212" height="141" />
        </div>
        <div class="info top">
          <div class="price">€ 6,00 <span class="linethr">lokaal € 7,50</div>
          <ul>
            <li>korting via Winterbergbus</li>
            <li>sleeën in de bossen en op de pistes</li>
            <li>voor jong en oud</li>
          </ul>
        </div>
        <div class="choice">
          <label>Aantal sleeën</label>
          <select id="slee" name="Step4[slee]">
            <option value="geen" selected="selected">Geen</option>
            <?php for( $i=1;$i<=Yii::app()->session['people'];$i++ ) echo "<option value='$i'>$i " . ( $i == 1 ? 'Slee' : 'Sleeën') . "</option>"; ?>
          </select>
        </div>
      </div>

      <div style="clear: both"></div>

      <div class="product left">
        <div class="title">Winterwandeltocht</div>
        <div class="image">
          <img src="/themes/default/images/wandelen.jpg" width="185" height="165" />
        </div>
        <div class="info top">
          <div class="price">€ 7,50 <span class="price-small" style="background: none;">incl. drankje</span></div>
          <ul>
            <li>begeleide wandeling met gids</li>
            <li>natuur en cultuur</li>
            <li>koffie/thee in het bos</li>
          </ul>
        </div>
        <div class="choice">
          <label>Wandeling reserveren</label>
          <select id="walking" name="Step4[walking]">
            <option value="geen" selected="selected">Geen</option>
            <?php for( $i=1;$i<=Yii::app()->session['people'];$i++ ) echo "<option value='$i'>$i " . ( $i == 1 ? 'Persoon' : 'Personen') . "</option>"; ?>
          </select>
        </div>
      </div>

      <div class="product right">
        <div class="title">Duitse LUNCH</div>
        <div class="image">
          <img src="/themes/default/images/braadworst.png" width="185" height="165" />
        </div>
        <div class="info top">
          <div class="price">€ 4,50 <span class="price-small" style="background: none;">excl. drank</div>
          <ul>
            <li>bord patat met echte Duitse Bratwurst</li>
            <li>gezellig bij Der Brabander</li>
            <li>direct aan de piste</li>
          </ul>
        </div>
        <div class="choice">
          <label>Aantal personen</label>
          <select id="food" name="Step4[food]">
            <option value="geen" selected="selected">Geen</option>
            <?php for( $i=1;$i<=Yii::app()->session['people'];$i++ ) echo "<option value='$i'>$i " . ( $i == 1 ? 'Persoon' : 'Personen') . "</option>"; ?>
          </select>
        </div>
      </div>

      <div style="clear: both"></div>

      <button id="kassabutton">Volgende stap</button>

      <div style="clear: both"></div>

    </form>

  </div>
</div>
