<div id="boek-banner">
  <img src="/themes/default/images/boek_busreis_winterberg.png" width="1002" height="246" />
</div>

<div id="boek-title">Boek busticket</div>

<div id="boek-content">
  <div id="boek-content-inner">

    <div class="leftSide">
      <form id="bookingForm" method="post" action="http://bus.winterbergbus.nl/bestellen/stap2">

        <input type="hidden" name="Step1[type]" id="type" value="retour"/>
        <input type="hidden" name="Step1[depart]" id="depart" value="" />

        <div class="formRow">
          <label>Soort</label>
          <div id="retour" class="radio selected">Retour</div>
          <!-- <div id="oneway" class="radio">Enkele reis</div> -->
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Van</label>
          <select id="departure" name="Step1[departure]" class="groot dropdown"></select>
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Naar</label>
          <select id="destination" name="Step1[destination]" class="groot dropdown"></select>
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Vertrekdatum</label>
          <input type="text" name="Step1[date]" id="calendar" size="10" />
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Aantal dagen</label>
          <select id="days" name="Step1[duration]" class="dropdown"></select>
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <label>Aantal personen</label>
          <div class="klein">
            <select id="passengers" name="Step1[people]" class="dropdown"></select>
          </div>
          <div style="clear: both"></div>
        </div>

        <div class="formRow">
          <input type="submit" id="bookingbutton" value="VIND BUSREIS" />
        </div>

      </form>
    </div>

    <div class="rightSide">
      <div class="youtube">
        <iframe width="459" height="291" src="//www.youtube.com/embed/S-ivNAYxHno?rel=0" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>

    <div style="clear: both"></div>

  </div>
</div>
