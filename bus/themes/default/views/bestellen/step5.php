  <div id="kassablok">
    <div id="kassawrap">

      <form id="booking-step5" method="post" action="">

        <div class="kassakolom col_2_3">
          <div class="blok">
            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_6.png" class="nummer" />

            <h3>controleer uw boeking</h3>

            <table>

              <tr>
                <?php
                  if( $booking->betaald_via == 'hema' || $booking->betaald_via == 'groupon' || $booking->betaald_via == 'vakantieveilingen' )
                  {
                    $payed =true;
                    ?>
                      <th class="left">Reis kosten (betaald via coupon van <?= $booking->betaald_via; ?>)</th>
                    <?php
                  }
                  else
                  {
                    $payed =false;
                    ?>
                      <th class="left">Reis kosten</th>
                    <?php
                  }
                ?>
                <th width="70" class="center">Aantal pers.</th>
                <th width="70" class="center">Subtotaal</th>
              </tr>
              <tr>
                <td class="departure">Vertrek vanaf <?= $departure->name; ?> om <?= date( 'H:i', strtotime( $booking->event->depart )); ?> naar <?= $destination->name; ?></td>
                <td class="center"><?php echo $passengers; ?></td>
                <td class="right">&euro; <?= number_format( Yii::app()->session['reis_sub'], 2 ); ?></td>
              </tr>
              <tr>
                <td colspan="2" class="right bold">Subtotaal</td>
                <td class="right">&euro; <?php echo number_format( Yii::app()->session['reis_sub'], 2 ); ?></td>
              </tr>

              <tr>
                <td colspan="3">&nbsp;</td>
              </tr>

              <?php
                // Display the order from the shop if any
                if( count( $order->lines ) > 0 )
                {
                  $dummy =0;

                  ?>
                    <tr>
                      <th class="left">Bestellingen vanuit de winkel</th>
                      <th width="70" class="center">Aantal</th>
                      <th width="70" class="center">Subtotaal</th>
                    </tr>
                  <?php

                  foreach( $order->lines as $line )
                  {
                    $dummy =doubleval( $line->price * $line->amount );
                    ?>
                      <tr>
                        <td><?= $line->title; ?></td>
                        <td class="center"><?= $line->amount; ?></td>
                        <td class="right">&euro; <?= number_format( $dummy, 2 ); ?></td>
                      </tr>
                    <?php
                  }

                  ?>
                    <tr>
                      <td colspan="2" class="right bold">Subtotaal</td>
                      <td class="right">&euro; <?php echo number_format( $order->total, 2 ); ?></td>
                    </tr>
                    <tr>
                      <td colspan="3">&nbsp;</td>
                    </tr>
                  <?php
                }
              ?>

              <tr>
                <th class="left">Totalen</th>
                <th width="70" class="center">&nbsp;</th>
                <th width="70" class="center">&nbsp;</th>
              </tr>
              <tr>
                <td>Kosten van de reis</td>
                <td>&nbsp;</td>
                <td class="right">&euro; <?= number_format( Yii::app()->session['reis_sub'], 2 ); ?></td>
              </tr>

              <?php
                if( count( $order->lines ) > 0 )
                {
                  ?>
                    <tr>
                      <td>Bestelling uit winkel</td>
                      <td>&nbsp;</td>
                      <td class="right">&euro; <?= number_format( $order->total, 2 ); ?></td>
                    </tr>
                  <?php
                }
              ?>
              <tr>
                <td colspan="2" class="right bold">Boekingskosten</td>
                <td class="right">&euro; <?php echo number_format( $admin, 2 ); ?></td>
              </tr>
              <?php
                $dummy =array( 'hema', 'groupon', 'vakantieveilingen' );

                if( in_array( $booking->betaald_via, $dummy ))
                {
                  ?>
                    <tr>
                      <td colspan="2" class="right bold">Reeds betaald via <?= $booking->betaald_via; ?></td>
                      <td class="right">&euro; <?php echo number_format( $booking->total, 2 ); ?></td>
                    </tr>
                  <?php
                }
              ?>
              <?php if( $payed ) { ?>
                <tr>
                  <td colspan="2" class="right bold">Nog te betalen</td>
                  <td class="right">&euro; <?php echo number_format( $order->total + $admin, 2 ); ?></td>
                </tr>
              <?php } else { ?>
                <tr>
                  <td colspan="2" class="right bold">Nog te betalen</td>
                  <td class="right">&euro; <?php echo number_format( $booking->total + $order->total + $admin, 2 ); ?></td>
                </tr>
              <?php } ?>
              <tr>
                <td colspan="3">&nbsp;</td>
              </tr>
            </table>
          </div>
        </div>

        <div class="kassakolom col_1_3">
          <?php
            $dummy =array( 'hema', 'groupon', 'vakantieveilingen' );

            if( !in_array( $booking->betaald_via, $dummy ) || ( count( $order->lines ) > 0 && $order->total > 0 ))
            {
              ?>
                <div class="blok">
                  <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_7.png" class="nummer" />
                  <h3>Kies betaalmethode</h3>
                  <input id="method" type="hidden" value="" name="Step5[payment]" >

                  <div class="paymentGroup">
                    <div id="payment_ideal" data="ideal" class="payment"></div>
                    <div id="payment_paypal" data="paypal" class="payment"></div>
                  </div>
                </div>
              <?php
            }
          ?>
        </div>

        <div class="buttonblock">
          <input type="checkbox" id="akkoord" name="Step5[agree]" value="1" /><label for="akkoord">Ik accepteer de <a target="_blank" href="http://www.winterbergbus.nl/l_winterbergbus_busreis_busreizen_winterberg_winterbergen_voorwaarden.html">Algemene voorwaarden</a></label>
          <button id="kassabutton">Plaats uw boeking</button>
        </div>

        <div style="clear:both;"></div>

      </form>

    </div>
  </div>
