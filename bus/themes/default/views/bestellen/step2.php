  <div id="kassablok">

    <?php

      $form =$this->beginWidget( 'CActiveForm', array( 'id'                     => 'step2Form',

                                                       'enableAjaxValidation'   => false,

                                                       'enableClientValidation' => true,

                                                       'clientOptions'          => array( 'validateOnSubmit' => true,

                                                                                          'validateOnChange' => true,

                                                                                          'validateOnType'   => false,

                                                                                          'hideErrorMessage' => true,

                                                       ),

      ));

    ?>



      <div id="kassawrap">

        <div class="kassakolom">

          <div class="blok">

            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_1.png" class="nummer" />



            <h3>hoofdboeker</h3>



            <div class="halve">

              <?php

                echo $form->labelEx( $model, 'voornaam' );

                echo $form->textfield( $model, 'voornaam' );

                echo $form->error( $model, 'voornaam' );

              ?>

            </div>

            <div class="halve">

              <?php

                echo $form->labelEx( $model, 'achternaam' );

                echo $form->textfield( $model, 'achternaam' );

                echo $form->error( $model, 'achternaam' );

              ?>

            </div>

            <div class="halve">

              <?php

                echo $form->labelEx( $model, 'email' );

                echo $form->textfield( $model, 'email' );

                echo $form->error( $model, 'email' );

              ?>

            </div>

            <div class="halve">

              <?php

                echo $form->labelEx( $model, 'email2' );

                echo $form->textfield( $model, 'email2' );

                echo $form->error( $model, 'email2' );

              ?>

            </div>

            <div class="full">

              <?php

                echo $form->labelEx( $model, 'telefoon1' );

                echo $form->textfield( $model, 'telefoon1' );

                echo $form->error( $model, 'telefoon1' );

              ?>

            </div>

            <div class="full">

              <?php

                echo $form->labelEx( $model, 'telefoon2' );

                echo $form->textfield( $model, 'telefoon2' );

                echo $form->error( $model, 'telefoon2' );

              ?>

            </div>

            <div class="full">

              <?php

                echo $form->labelEx( $model, 'plaats' );

                echo $form->textfield( $model, 'plaats' );

                echo $form->error( $model, 'plaats' );

              ?>

            </div>

          </div>

        </div>



        <div class="kassakolom">

          <div class="blok">

            <img class="bus" src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/winterbergbus.jpg" />

          </div>

        </div>



        <div class="kassakolom">

          <div class="blok">

            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_3.png" class="nummer" />



            <h3>hoe kent u ons??</h3>

            <input type="radio" id="radiox1" name="radiox" value="1" /><label for="radiox1" class="ref">Zoekmachine</label><br />

            <input type="radio" id="radiox2" name="radiox" value="2" /><label for="radiox2" class="ref">Social media (Facebook etc.)</label><br />

            <input type="radio" id="radiox3" name="radiox" value="3" /><label for="radiox3" class="ref">Reclame/advertentie</label><br />

            <input type="radio" id="radiox5" name="radiox" value="5" /><label for="radiox5" class="ref">Via vrienden</label><br />

            <input type="radio" id="radiox6" name="radiox" value="6" /><label for="radiox6" class="ref">Anders, namelijk</label><input type="text" name="other" id="referrer" />

          </div>

        </div>



        <div class="kassakolom col_3_3">

          <div class="blok">

            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_2.png" class="nummer" />



            <h3>Reizigers</h3>



            <?php

              for( $i=1;$i<=Yii::app()->session['people'];$i++ )

              {

                ?>

                  <fieldset>

                    <legend>Reiziger <?= $i; ?></legend>

                    <div class="halve">

                      <?php

                        echo $form->textfield( $model, 'voornaam' . $i, array( 'placeholder' => 'Voornaam' ));

                        echo $form->error( $model, 'voornaam' . $i );

                      ?>

                    </div>

                    <div class="halve">

                      <?php

                        echo $form->textfield( $model, 'achternaam' . $i, array( 'placeholder' => 'Achternaam' ));

                        echo $form->error( $model, 'achternaam' . $i );

                      ?>

                    </div>

                  </fieldset>

                <?php

              }

            ?>

          </div>

        </div>



        <div class="kassakolom col_3_3">

          <button id="kassabutton">Volgende stap</button>

        </div>



        <div style="clear:both;"></div>



      </div>



    <?php $this->endWidget(); ?>



  </div>

