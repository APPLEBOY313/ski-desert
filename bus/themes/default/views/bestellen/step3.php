  <div id="kassablok">

    <form id="booking-step3" method="post" action="">

      <div id="kassawrap">

        <div class="kassakolom">
          <div class="blok">
            <img src="<?php echo Yii::app()->baseUrl; ?>/themes/default/images/kassa_4.png" class="nummer" />

            <h3>Heeft u een coupon?</h3>

            <input id="method" type="hidden" value="" name="Step3[method]" >

            <div class="paymentGroup">

              <div id="payment_none" data="none" class="payment"></div>

              <?php
                if( Yii::app()->session['people'] == 2 && Yii::app()->session['type'] == 'retour' )
                {
                  ?>
                    <div id="payment_vakantieveilingen" data="vakantieveilingen" class="payment"></div>
                  <?php
                }
              ?>

              <?php if( Yii::app()->session['type'] == 'retour' ) { ?>
                <div id="payment_hema" data="hema" class="payment"></div>
                <div id="payment_groupon" data="groupon" class="payment"></div>
              <?php } ?>

            </div>
          </div>
        </div>

        <?php // Vakantieveilingen
          if( Yii::app()->session['people'] == 2 && Yii::app()->session['type'] == 'retour' )
          {
            ?>
              <div id="vakantieveilingen" class="kassakolom" style="display:none; width: 608px; ">
                <h4>Vakantieveilingen</h4>
                <p>Vul hieronder je vakantieveilingen coupon code in en druk daarna op de knop om deze code te controleren.</p>
                <input type="text" value="" name="Step3[vv-code]" />
                <div id="vv-check">Check</div>
              </div>
            <?php
          }
        ?>

        <?php if( Yii::app()->session['type'] == 'retour' ) { ?>
          <div id="hema" class="kassakolom" style="display:none; width: 608px; ">
            <h4>Hema</h4>
            <p>Vul hieronder de voucher codes in die je bij de Hema hebt gekocht.<br /><br />Let op! Vul ze in zonder een comma er tussen.</p>
            <textarea name="Step3[hema-code]" placeholder="Bijvoorbeeld: s43res3e t2retred etc"></textarea>
            <div id="hema-check">Check</div>
          </div>
          <div id="groupon" class="kassakolom" style="display:none; width: 608px; ">
            <h4>Groupon</h4>
            <p>
              Vul hieronder de voucher je groupon codes in samen met de controle code.<br />
              <br />
              Als je inclusief materiaal hebt gekocht dan wordt daar op de afreken pagina rekening mee gehouden door een set ski's, langlauf latten of een snowboard op 0 euro te zetten.
            </p>
            <?php
              for( $i=1;$i<=Yii::app()->session['people'];$i++ )
              {
                ?>
                  <div class="set">
                    <label>Passagier <?= $i; ?></label>
                    <input type="text" class="code" name="Step3[groupon-code][code][]" value="" placeholder="Grouponcode <?= $i; ?>" />
                    <input type="text" class="control" name="Step3[groupon-code][control][]" value="" placeholder="Controlecode <?= $i; ?>" />
                  </div>
                <?php
              }
            ?>
            <div id="groupon-check">Check</div>
          </div>
        <?php } ?>

        <div id="none" class="kassakolom" style="display:none;">
          <h4>Geen coupon</h4>
          <p>Gebruik deze optie als u geen coupon code heeft.</p>
        </div>

        <div class="kassakolom col_3_3">
          <div id="kassabutton">Volgende stap</div>
        </div>

        <div style="clear:both;"></div>

      </div>

    </form>
  </div>
