<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php echo $this->pageTitle; ?></title>
    <meta name="description" content="">
    <meta name="author" content="CentrioSoft | www.CentrioSoft.com">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php $cs = Yii::app()->getClientScript();?>

    <?php $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/plugins/jquery.visualize.css');?>
    <?php $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/plugins/jquery.jgrowl.css'); ?>
    <?php $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/huraga-red.css'); ?>
    <?php $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/style.css'); ?>

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icons/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icons/apple-touch-icon-57-precomposed.png">

    <!-- JS Libs -->
    <?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/libs/modernizr.js'); ?>
    <?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/libs/selectivizr.js'); ?>
    <?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.printElement.min.js'); ?>
</head>
<body>

<!-- Main page header -->
<header class="container">
    <h1><a href="<?php echo $this->createUrl('/admin'); ?>" class="brand">CentrioSoft</a></h1>
    <p>CentrioSoft: The art of programming</p>
    <nav>
        <ul>
            <li><a href="<?php echo $this->CreateUrl('/admin/auth/logout');?>">Logout</a></li>
        </ul>
    </nav>
    <!-- /Alternative navigation -->

</header>
<!-- /Main page header -->

<!-- Main page container -->
<section class="container" role="main">

<!-- Left (navigation) side -->
<div class="navigation-block">
    <!-- Sample side note -->
    <section class="side-note">
        <div class="side-note-container">
            <h2>Sample Side Note</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis erat dui, quis purus.</p>
        </div>
        <div class="side-note-bottom"></div>
    </section>
    <!-- /Sample side note -->

    <!-- Main navigation -->
    <nav class="main-navigation" role="navigation">
        <ul>
            <li <?php if (Yii::app()->request->requestUri == $this->createUrl('/admin')): ?>class="current"<?php endif; ?>><a href="<?php echo $this->createUrl('/admin'); ?>" class="no-submenu"><span class="awe-home"></span>Dashboard</a></li>
            <li <?php if (Yii::app()->request->requestUri == $this->createUrl('/admin/days')): ?>class="current"<?php endif; ?>><a href="<?php echo $this->createUrl('/admin/days'); ?>" class="no-submenu"><span class="awe-calendar"></span>Dagen</a></li>
            <li <?php if (Yii::app()->request->requestUri == $this->createUrl('/admin/events')): ?>class="current"<?php endif; ?>><a href="<?php echo $this->createUrl('/admin/events'); ?>" class="no-submenu"><span class="awe-globe"></span>Reizen</a></li>
            <li <?php if (Yii::app()->request->requestUri == $this->createUrl('/admin/bookings')): ?>class="current"<?php endif; ?>><a href="<?php echo $this->createUrl('/admin/bookings'); ?>" class="no-submenu"><span class="awe-tasks"></span>Boekingen</a></li>
            <li <?php if (Yii::app()->request->requestUri == $this->createUrl('/admin/cities')): ?>class="current"<?php endif; ?>><a href="<?php echo $this->createUrl('/admin/cities'); ?>" class="no-submenu"><span class="awe-signal"></span>Opstap Plaatsen</a></li>
            <li <?php if (Yii::app()->request->requestUri == $this->createUrl('/admin/settings')): ?>class="current"<?php endif; ?>><a href="<?php echo $this->createUrl('/admin/settings');?>" class="no-submenu"><span class="awe-table"></span>Settings</a></li>
        </ul>
    </nav>
    <!-- /Main navigation -->

</div>
<!-- Left (navigation) side -->

<!-- Right (content) side -->
<div class="content-block" role="main">
    <div class="row">
        <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array('links'=>$this->getBreadCrumbs())); ?>
    </div>
<?php echo $content; ?>
</div>
<!-- /Right (content) side -->

</section>
<!-- /Main page container -->

<!-- Main page footer -->
<footer class="container">
    <p>&copy; Copyright 2003-<?php echo date('Y'); ?> <a href="http://www.centriosoft.com">CentrioSoft</a></p>
    <a href="#top" class="btn btn-primary btn-flat pull-right">Top &uarr;</a>
</footer>
<!-- /Main page footer -->

<!-- Scripts -->
<?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/navigation.js'); ?>
<?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/bootstrap/bootstrap-affix.js'); ?>
<?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/bootstrap/bootstrap-tooltip.js'); ?>
<?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/bootstrap/bootstrap-popover.js'); ?>
<?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/bootstrap/bootstrap-collapse.js'); ?>
<?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/bootstrap/bootstrap-dropdown.js'); ?>
<?php $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/bootstrap/bootstrap-transition.js'); ?>

</body>
</html>
