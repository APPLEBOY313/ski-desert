<article class="page-header">
    <h1>Dashboard</h1>
    <p>Great attention has been paid to the typography elements. This preview page will show you basic elements and popular user interface components available. Many styles are combinable, so don't be affraid to experiment. Modifying is super easy, you can edit final CSS or, if you are familiar with LESS, typographic components are controled with few LESS variables.</p>
</article>
<div class="hero-unit">
    <h1>Showcase heading</h1>
    <p>When you really need to draw the attention. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis metus sit amet lorem viverra suscipit. Integer lobortis, sem vitae bibendum vulputate, neque orci vestibulum quam, vel volutpat nibh felis ac ante.</p>
    <p>
</div>
