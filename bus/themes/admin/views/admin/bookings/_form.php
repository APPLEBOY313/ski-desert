<?php
  $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
      'id'=>'horizontalForm',
      'enableClientValidation'=>true,
      'clientOptions'=>array('validateOnSubmit'=>true),
      'type'=>'horizontal',
    )
  );
?>
  <fieldset>
    <legend>Vertrek datum en locatie</legend>
    <?php echo $form->dropDownListRow($model,'event_depart_id',$this->getEventDepartList()); ?>
    <?php
      $dummy =City::model()->findByPk( $model->destination );
      if( $dummy !== null )
      {
        ?>
          <div class="control-group ">
            <label class="control-label">Bestemming: </label>
            <div class="controls">
              <span style="display: block;padding: 5px 5px 5px 0;"><?= $dummy->name; ?></span>
            </div>
          </div>
        <?php
      }
    ?>
  </fieldset>

  <fieldset>
    <legend>Gegevens</legend>
    <?php echo $form->textFieldRow($model, 'voornaam'); ?>
    <?php echo $form->textFieldRow($model, 'achternaam'); ?>
    <?php echo $form->textFieldRow($model, 'straat'); ?>
    <?php echo $form->textFieldRow($model, 'postcode'); ?>
    <?php echo $form->textFieldRow($model, 'plaats'); ?>
    <?php echo $form->datepickerRow($model, 'geboorte',array( 'options' => array( 'format' => 'yyyy-mm-dd' ), 'prepend' => '<i class="icon-calendar"></i>', 'style'   => 'width:179px;' )); ?>
    <?php echo $form->textFieldRow($model, 'email'); ?>
    <?php echo $form->textFieldRow($model, 'telefoon1'); ?>
    <?php echo $form->textFieldRow($model, 'telefoon2'); ?>
  </fieldset>

  <fieldset>
    <legend>Retour of enkele reis</legend>
    <div class="control-group ">
      <label class="control-label">Enkele reis ?</label>
      <div class="controls">
        <span style="display: block;padding: 5px 5px 5px 0;"><?= ( $model->single ? 'Ja' : 'Nee' ); ?></span>
      </div>
    </div>
  </fieldset>

  <fieldset>
    <legend>Financieel</legend>
    <?php echo $form->dropDownListRow( $model, 'betaald', array( '1' => 'Ja', '0' => 'Nee' )); ?>

    <div class="control-group ">
      <label class="control-label">Totaal bedrag</label>
      <div class="controls">
        <?php
          $total =$model->total;
          if( $model->order != '' )
          {
            $order =json_decode( $model->order );
            $total +=$order->total;
          }
        ?>
        <span style="display: block;padding: 5px 5px 5px 0;">&euro; <?php echo number_format( $total, 2 ); ?></span>
      </div>
    </div>

    <div class="control-group ">
      <label class="control-label">Betaal methode</label>
      <div class="controls">
        <span style="display: block;padding: 5px 5px 5px 0;"><?php echo $model->betaald_via; ?></span>
      </div>
    </div>

    <?php
      if( strpos( $model->betaald_via, 'hema' )              !== false ||
          strpos( $model->betaald_via, 'groupon' )           !== false ||
          strpos( $model->betaald_via, 'vakantieveilingen' ) !== false
      )
      {
        ?>
          <div class="control-group ">
            <label class="control-label">Gebruikte codes</label>
            <div class="controls">
              <span style="display: block;padding: 5px 5px 5px 0;"><?php echo $model->voucher; ?></span>
            </div>
          </div>
        <?php
      }
    ?>

    <?php
      if( $model->order != '' )
      {
        $order =json_decode( $model->order );
        if( count( $order->lines ) > 0 )
        {
          ?>
            <div class="control-group ">
              <label class="control-label">Bestelling</label>
              <div class="controls">
                <table>
                  <tr style="border-bottom: 1px solid black;">
                    <th style="text-align: left; width: 250px;">Product</th>
                    <th style="text-align: center; width: 75px;">Aantal</th>
                    <th style="text-align: right; width: 75px;">Subtotaal</th>
                  </tr>
                  <?php
                    foreach( $order->lines as $line )
                    {
                      ?>
                        <tr>
                          <td><?= $line->title; ?></td>
                          <td style="text-align: center;"><?= $line->amount; ?></td>
                          <td style="text-align: right;"><?= number_format( $line->price, 2 ); ?></td>
                        </tr>
                      <?php
                    }
                  ?>
                  <tr style="border-top: 1px solid black;">
                    <td colspan="2" style="text-align: right; font-weight: bold;">Totaal: </td>
                    <td style="text-align: right;"><?= number_format( $order->total, 2 ); ?></td>
                  </tr>
                </table>
              </div>
            </div>
          <?php
        }
      }
    ?>
  </fieldset>

  <fieldset>
    <legend>Deelnemers</legend>
    <div class="control-group ">
    <label class="control-label" for="Event_avail">Deelnemers</label>
    <div class="controls">
      <table cellpadding="0" cellspacing="0" id="items" style="width:500px">
        <tr>
          <td>Voornaam</td>
          <td>Achternaam</td>
          <td></td>
          <td></td>
        </tr>

        <tr id="add-item-template">
          <td class="item">
            <input type="hidden" name="Booking[BookingPerson][id][]" value="">
            <input type="text" name="Booking[BookingPerson][voornaam][]" style="width:206px;">
          </td>
          <td>
            <input type="text" name="Booking[BookingPerson][achternaam][]" style="width:206px;">
          </td>
          <td>
            <span class="add-item icon-plus"></span>
          </td>
          <td>
            <span class="remove-item icon-minus"></span>
          </td>
        </tr>

        <?php if ($model->persons): ?>
        <?php foreach($model->persons as $person): ?>
          <tr>
            <td class="item">
              <input type="hidden" name="Booking[BookingPerson][id][]" value="<?php echo $person->id; ?>">
              <input type="text" name="Booking[BookingPerson][voornaam][]" style="width:206px;" value="<?php echo $person->voornaam; ?>">
            </td>
            <td>
              <input type="text" name="Booking[BookingPerson][achternaam][]" style="width:206px;" value="<?php echo $person->achternaam; ?>">
            </td>
            <td>
              <span class="add-item icon-plus"></span>
            </td>
            <td>
              <span class="remove-item icon-minus"></span>
            </td>
          </tr>
          <?php endforeach; ?>
        <?php else: ?>
        <tr>
          <td class="item">
            <input type="hidden" name="Booking[BookingPerson][id][]" value="">
            <input type="text" name="Booking[BookingPerson][voornaam][]" style="width:206px;">
          </td>
          <td>
            <input type="text" name="Booking[BookingPerson][achternaam][]" style="width:206px;">
          </td>
          <td>
            <span class="add-item icon-plus"></span>
          </td>
          <td>
            <span class="remove-item icon-minus"></span>
          </td>
        </tr>
        <?php endif; ?>
      </table>
      <?php Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' ); ?>
      <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/dynoTable.js"></script>
      <script type="text/javascript">
        jQuery(document).ready(function()
        {
          jQuery('#items').dynoTable(
          {
            removeClass: '.remove-item',            //Custom remover class name in beers table
            cloneClass: '.clone-item',              //Custom cloner class name in beers table
            addRowTemplateId: '#add-item-template', //Custom id for beer row template
            addRowButtonId: '.add-item',            //Click this to add a beer!
            lastRowRemovable: false,                //Don't let the table be empty. Never run out of beer!
            orderable: false,                       //beers can be rearranged
            dragHandleClass: ".drag-item"           //class for the click and draggable drag handle
          });
        });
      </script>
    </div>
  </fieldset>

<?php /*
  <fieldset>
    <legend>Uw bagage</legend>
    <?php echo $form->dropDownListRow($model, 'nr_ski',array('Selecteer ...', '1', '2', '3', '4', '5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20')); ?>
    <?php echo $form->dropDownListRow($model, 'nr_snowboard',array('Selecteer ...', '1', '2', '3', '4', '5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20')); ?>
    <?php echo $form->dropDownListRow($model, 'nr_langlauf',array('Selecteer ...', '1', '2', '3', '4', '5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20')); ?>
  </fieldset>
*/ ?>

  <div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Verzenden')); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'label'=>'Annuleren', 'url'=>$this->createUrl('/admin/bookings'))); ?>
  </div>

<?php $this->endWidget(); ?>
