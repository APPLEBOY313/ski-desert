<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'enableClientValidation'=>true,
    'clientOptions'=>array('validateOnSubmit'=>true),
    'type'=>'horizontal',
)); ?>

    <?php $model->days =unserialize( $model->days ); ?>

    <?php echo $form->textFieldRow( $model, 'name' ); ?>
    <?php echo $form->datepickerRow( $model, 'depart', array('options'=>array('format'=>'yyyy-mm-dd'),'prepend'=>'<i class="icon-calendar"></i>')); ?>
    <?php echo $form->checkBoxListRow( $model, 'days', Day::getItemsAsArray( ), array( 'prompt' => 'Maak een keuze' )); ?>
    <?php echo $form->textFieldRow( $model, 'avail' ); ?>
    <div class="control-group ">
        <label class="control-label" for="Event_avail">Opstapplaatsen</label>
        <div class="controls">
            <table cellpadding="0" cellspacing="0" id="items" style="width:420px">
                <tr>
                    <td>Plaats</td>
                    <td>Tijd</td>
                    <td>Prijs</td>
                    <td></td>
                </tr>

                <tr id="add-item-template">
                  <td>
                    <input type="hidden" name="Event[EventDepart][id][]" value="">
                    <select name="Event[EventDepart][city_id][]">
                      <?php
                        $items = City::model()->findAll();
                        foreach($items as $item)
                        {
                          echo "<option value='".$item->id."'>".$item->name." - ".$item->description."</option>";
                        }
                      ?>
                    </select>
                  </td>
                  <td style="width:75px;">
                    <input type="text" name="Event[EventDepart][time][]" style="width:50px;">
                  </td>
                  <td style="width:75px;">
                    <input type="text" name="Event[EventDepart][price][]" style="width:50px;">
                  </td>
                  <td style="width:20px;">
                    <span class="add-item icon-plus"></span>
                  </td>
                  <td style="width:20px;">
                    <span class="remove-item icon-minus"></span>
                  </td>
                </tr>

                <?php if ($model->departs): ?>
                <?php foreach($model->departs as $city): ?>
                <tr>
                    <td>
                        <input type="hidden" name="Event[EventDepart][id][]" value="<?php echo $city->id; ?>">
                        <select name="Event[EventDepart][city_id][]">
                            <?php
                                $items = City::model()->findAll();
                                foreach($items as $item)
                                {
                                    $selected = '';
                                    if ($item->id == $city->city_id)
                                    {
                                        $selected = "selected='selected' ";
                                    }
                                    echo "<option ".$selected."value='".$item->id."'>".$item->name." - ".$item->description."</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <td style="width:75px;">
                        <input type="text" name="Event[EventDepart][time][]" style="width:50px;" value="<?php echo date('H:i',strtotime($city->depart)); ?>">
                    </td>
                    <td style="width:75px;">
                        <input type="text" name="Event[EventDepart][price][]" style="width:50px;" value="<?php echo $city->price; ?>">
                    </td>
                    <td style="width:20px;">
                      <span class="add-item icon-plus"></span>
                    </td>
                    <td style="width:20px;">
                        <span class="remove-item icon-minus"></span>
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td>
                        <input type="hidden" name="Event[EventDepart][id][]" value="">
                        <select name="Event[EventDepart][city_id][]">
                            <?php
                              $items = City::model()->findAll();
                              foreach($items as $item)
                              {
                                echo "<option value='".$item->id."'>".$item->name." - ".$item->description."</option>";
                              }
                            ?>
                        </select>
                    </td>
                    <td style="width:75px;">
                        <input type="text" name="Event[EventDepart][time][]" style="width:50px;">
                    </td>
                    <td style="width:75px;">
                        <input type="text" name="Event[EventDepart][price][]" style="width:50px;">
                    </td>
                    <td style="width:20px;">
                        <span class="add-item icon-plus"></span>
                    </td>
                    <td style="width:20px;">
                        <span class="remove-item icon-minus"></span>
                    </td>
                </tr>
                <?php endif; ?>
            </table>
            <?php Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' ); ?>
            <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/dynoTable.js"></script>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#items').dynoTable({
                        removeClass: '.remove-item',
                        cloneClass: '.clone-item',
                        addRowTemplateId: '#add-item-template',
                        addRowButtonId: '.add-item',
                        lastRowRemovable: false,
                        orderable: false,
                        dragHandleClass: ".drag-item"
                    });
                });
            </script>
        </div>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Opslaan')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'label'=>'Annuleren', 'url'=>$this->createUrl('/admin/events'))); ?>
    </div>
<?php $this->endWidget(); ?>
