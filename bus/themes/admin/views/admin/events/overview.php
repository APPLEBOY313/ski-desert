<?php
  foreach( $event->departs as $depart )
  {
    // Any bookings ?
    if( count( $depart->bookings ) <= 0 ) continue;

    // Print a table with information
    $city =$depart->city;

    $id =uniqid( );
    ?>
      <div class="print">

        <div class="button">Afdrukken</div>

        <table width="100%" border="1" cellspacing="0">
          <caption>Vertrek in <?php echo $depart->city->name; ?> om <?php echo $depart->depart; ?></caption>
          <tr>
            <th>Voornaam</th>
            <th>Achternaam</th>
            <th>Telefoon</th>
            <th>Reis tel</th>
            <th>Betaald</th>
            <th>Aantal</th>
            <th>Type</th>
            <th align="left">Personen</th>
          </tr>

          <?php
            foreach( $depart->bookings as $booking )
            {
//              if( !$booking->betaald ) continue;

              $list ='';
              $members =$booking->members;
              if( $members <= 0 ) echo '-';
              else
              {
                $counter =1;
                foreach( $booking->persons as $person )
                {
                  $list .=$person->voornaam . ' ' . $person->achternaam;

                  if( $counter++ < $members ) $list .='<br />';
                }
              }
              ?>
                <tr>
                  <td valign="top"><?php echo $booking->voornaam; ?></td>
                  <td valign="top"><?php echo $booking->achternaam; ?></td>
                  <td valign="top" align="center" class="center"><?= $booking->telefoon1; ?></td>
                  <td valign="top" align="center" class="center"><?= $booking->telefoon2; ?></td>
                  <td valign="top" align="center" class="center"><?= $booking->betaald ? 'Ja' : "Nee"; ?></td>
                  <td valign="top" align="center" class="center"><?= $booking->members; ?></td>
                  <td valign="top" align="center" class="center"><?= ( $booking->single ? 'Enkel' : 'Retour' ); ?></td>
                  <td valign="top" align="left" class="left" width="300"><?php echo $list; ?></td>
                </tr>
              <?php
            }
          ?>

        </table>
      </div>
    <?php
  }
?>
  <script type="text/javascript">
    $('.button').live("click", function()
    {
      $(this).siblings('table').printElement(
      {
        overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl . '/css/style.css'; ?>']
      });
    });
  </script>
