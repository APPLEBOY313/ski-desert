<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Login | CentrioSoft</title>
    <meta name="description" content="">
    <meta name="author" content="CentrioSoft | www.CentrioSoft.com">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS styles -->
    <link rel='stylesheet' type='text/css' href='<?php echo Yii::app()->theme->baseUrl;?>/css/huraga-red.css'>

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icons/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icons/apple-touch-icon-57-precomposed.png">

    <!-- JS Libs -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/libs/jquery.js"><\/script>')</script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/libs/modernizr.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/libs/selectivizr.js"></script>
</head>
<body>

<!-- Main page container -->
<section class="container login" role="main">

    <h1><a href="<?php echo $this->createUrl('/admin');?>" class="brand">Huraga</a></h1>
    <div class="data-block">
        <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableAjaxValidation'=>true,
    )); ?>
            <fieldset>
                <div class="control-group">
                    <?php echo $form->error($model,'password'); ?>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx($model,'username'); ?>
                    <div class="controls">
                        <?php echo $form->textField($model,'username',array('placeholder'=>'Your username')); ?>
                        <?php echo $form->error($model,'username'); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx($model,'password'); ?>
                    <div class="controls">
                        <?php echo $form->passwordField($model,'password',array('placeholder'=>'Password')); ?>
                        <label class="checkbox">
                            <?php echo $form->checkBox($model,'rememberMe'); ?>
                            <?php echo $form->label($model,'rememberMe'); ?>
                            <?php echo $form->error($model,'rememberMe'); ?>
                        </label>
                    </div>
                </div>
                <div class="form-actions">
                    <button class="btn btn-large btn-inverse btn-alt" type="submit"><span class="awe-signin"></span> Log in</button>
                </div>
            </fieldset>
        <?php $this->endWidget(); ?>
    </div>
    <p><a href="#" class="pull-right"><small>Password reset</small></a></p>

</section>
<!-- /Main page container -->

<!-- Scripts -->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap/bootstrap-tooltip.js"></script>

</body>
</html>
