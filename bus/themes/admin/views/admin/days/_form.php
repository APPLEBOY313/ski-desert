<?php
  $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array('id'                      => 'horizontalForm',
                                                                      'type'                   => 'horizontal',
                                                                      'clientOptions'          => array('validateOnSubmit'=>true),
                                                                      'enableClientValidation' => true,
  )); ?>

  <?php echo $form->textFieldRow( $model, 'name' ); ?>
  <?php echo $form->textFieldRow( $model, 'number' ); ?>

  <div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Opslaan')); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'label'=>'Annuleren', 'url'=>$this->createUrl('/admin/cities'))); ?>
  </div>

  <?php $this->endWidget(); ?>
