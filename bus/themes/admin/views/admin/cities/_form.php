<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'enableClientValidation'=>true,
    'clientOptions'=>array('validateOnSubmit'=>true),
    'type'=>'horizontal',
)); ?>

<?php echo $form->textFieldRow($model, 'name',array('hint'=>'In addition to freeform text, any HTML5 text-based input appears like so.')); ?>
<?php echo $form->textAreaRow($model, 'description',array('hint'=>'In addition to freeform text, any HTML5 text-based input appears like so.')); ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Opslaan')); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'label'=>'Annuleren', 'url'=>$this->createUrl('/admin/cities'))); ?>
</div>
<?php $this->endWidget(); ?>
