<!-- Grid row -->
<div class="row">
    <!-- Data block -->
    <article class="span12 data-block">
        <div class="data-container">
            <header>
                <h2>Settings</h2>
                <ul class="data-header-actions">
                    <li class="demoTabs active"><a href="#basic" class="btn">Basic</a></li>
                    <li class="demoTabs"><a href="#advanced" class="btn">Advanced</a></li>
                </ul>
            </header>
            <section class="tab-content">

                <!-- Tab #basic -->
                <div class="tab-pane active" id="basic">

                    <!-- Example vertical forms -->
                    <div class="row-fluid">
                        <div class="span4">
                            <h3>Vertical form</h3>
                            <p>Lorem ipsum dolor sit amet, adipiscing elit. Phasellus vitae  ac ligula eleifend commodo id quis risus. Donec consequat lorem et metus vitae dapibus felis fermentum.</p>
                            <p>Aenean imperdiet nibh vitae sem condimentum pharetra. Mauris molestie molestie faucibus. Aenean sit amet sem orci, at adipiscing dolor. Fusce feugiat odio vitae erat commodo sed accumsan ante sollicitudin. Integer mollis, tellus vel feugiat varius, risus quam lacinia erat, eu ultricies urna dolor ac metus.</p>
                        </div>
                        <div class="span8">
                            <?php
                            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                                'id'=>'horizontalForm',
                                'type'=>'horizontal',
                            )); ?>
                            <form class="form-horizontal">
                                <fieldset>
                                    <?php echo $form->textFieldRow($model, 'username'); ?>
                                    <?php echo $form->textFieldRow($model, 'password'); ?>
                                    <?php echo $form->textAreaRow($model,'bedankt_msg'); ?>
                                    <?php echo $form->textFieldRow($model,'booking_cost'); ?>
                                    <div class="form-actions">
                                        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Opslaan')); ?>
                                    </div>
                                </fieldset>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </article>
</div>
<!-- /Grid row -->