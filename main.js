$(document).ready(function(){
    $("#materiaal").change(function(){
    	if (this.value=="slee") {
    		$("#erva").prop('disabled','disabled');
    		$("#scho_left").prop('disabled','disabled');
    		$("#leng_left").prop('disabled','disabled');
    		$("#leng_right").prop('disabled','disabled');

    		$("#erva").prop('style', 'background-color:#EAEAEA');
    		$("#scho_left").prop('style', 'background-color:#EAEAEA');
    		$("#scho_right").prop('style', 'background-color:#EAEAEA'); 
    		$("#scho_right").children().css( "opacity", 0.5 );
    		$("#leng_left").prop('style', 'background-color:#EAEAEA');
    		$("#leng_right").prop('style', 'background-color:#EAEAEA');
    	} else{
    		$("#erva").prop('disabled',false);
    		$("#scho_left").prop('disabled',false);
    		$("#leng_left").prop('disabled',false);
    		$("#leng_right").prop('disabled',false);

    		$("#erva").prop('style', 'background-color:none');
    		$("#scho_left").prop('style', 'background-color:none');
    		$("#scho_right").prop('style', 'background-color:none');    		
    		$("#scho_right").children().css( "opacity", 1 );
    		$("#leng_left").prop('style', 'background-color:none');
    		$("#leng_right").prop('style', 'background-color:none');
    	}
	});
	$("#scho_left").change(function(){
		if (this.value=="EU") {
			$("#scho_right").empty();
			$("#scho_right").append("<p>EU 40</p>");
		}
		if (this.value=="UK") {
			$("#scho_right").empty();
			$("#scho_right").append("<p>UK 7</p>");
		}
		if (this.value=="Mondopoint") {
			$("#scho_right").empty();
			$("#scho_right").append("<p>MP 22.5</p>");
		}
		if (this.value=="Men") {
			$("#scho_right").empty();
			$("#scho_right").append("<p>US M 12½</p>");
		}
		if (this.value=="Women") {
			$("#scho_right").empty();
			$("#scho_right").append("<p>US W 10½</p>");
		}
		if (this.value=="Kids") {
			$("#scho_right").empty();
			$("#scho_right").append("<p>US K 9½</p>");
		}
	});
	$("#leng_left").change(function(){
		if (this.value=="Centimeter") {
			$("#leng_right").empty();
			var myoption = "";
			for(var i=125; i<130; i++){
				myoption = myoption + "<option>" + i + " Cm</option>"
			}
			$("#leng_right").append(myoption);
		}
		if (this.value=="Foot") {
			$("#leng_right").empty();
			$("#leng_right").append(`<option>3'4" ft</option><option>3'5" ft</option>`);
		}
	});
	$("#helm_left").change(function(){
		if (this.value=="Centimeter") {			
			$("#helm_right").empty();
			$("#helm_right").append("<option>53 Cm</option>");
		}
		if (this.value=="Inch") {			
			$("#helm_right").empty();
			$("#helm_right").append("<option>23½ in</option>");
		}
	});
	$("#check_box").change(function(){
		if($('#check_box').is(":checked")){			
    		$("#scho_left").prop('disabled','disabled');    		
    		$("#scho_left").prop('style', 'background-color:#EAEAEA');
    		$("#scho_right").prop('style', 'background-color:#EAEAEA'); 
    		$("#scho_right").children().css( "opacity", 0.5 );
		} else{
			$("#scho_left").prop('disabled',false);    		
    		$("#scho_left").prop('style', 'background-color:none');
    		$("#scho_right").prop('style', 'background-color:none'); 
    		$("#scho_right").children().css( "opacity", 1 );
		}
	});
});